#ifdef BEAMFORMER_CUH_

#include <cuda.h>
#include <cuComplex.h>
#include <cuda_fp16.h>
#include <mma.h>
#include "beamforming/details/utils.cu"

namespace cryopaf{
namespace beamforming{

template<typename T>
__device__ void warp_reduce(T data[WARP_SIZE], const int thrd_idx, T *p = nullptr)
{
__syncwarp();
  if(thrd_idx < 16)
  {
    data[thrd_idx] += data[thrd_idx + 16];
  }
__syncwarp();
  if(thrd_idx < 8)
  {
    data[thrd_idx] += data[thrd_idx + 8];
  }
__syncwarp();
  if(thrd_idx < 4)
  {
    data[thrd_idx] += data[thrd_idx + 4];
  }
__syncwarp();
  if(thrd_idx < 2)
  {
    data[thrd_idx] += data[thrd_idx + 2];
  }
__syncwarp();
  if(thrd_idx < 1)
  {
    data[thrd_idx] += data[thrd_idx + 1];
    data[thrd_idx] /= WARP_SIZE;
    if(p != nullptr)
    {
      p[0] = data[thrd_idx];
    }
  }
}


// Input: F-P-E-T
// Weights: F-P-B-E
// Out vol: B-F-P-T
// Out stokes I: B-F-T
// Out full stokes: B-F-T-S
__global__ void beamform(
  const char2* idata,
  const char2* wdata,
  const float2* twiddle,
  int2* odata_vol,
  float* odata_sti,
  float4* odata_stf,
  const unsigned n_time,
  const unsigned n_elem,
  const unsigned n_beam,
  const unsigned integrate,
  const unsigned left,
  const unsigned right)
{
#if __CUDA_ARCH__ >= 750
    using namespace nvcuda; // used namespace for wmma API - use of Nvidia's Tensor cores

    // Variables ending with '_id' are used for clear indexing of memory pointers
    const int thrd_id = threadIdx.x; // Thread ID of this block
    const int warp_id = threadIdx.y; // Warp ID of this block
    const int time_id_offset = (warp_id % INTEGRATE_ITER) * WARP_SIZE + thrd_id;   // Internal time offset within this block
    // const int time_id = blockIdx.y * SAMPLES_ITER + time_id_offset; // Global time index (e.g. 128 samples per block)
    const int time_id = blockIdx.y * SAMPLES_ITER * integrate / INTEGRATE_ITER; // Global time index (e.g. 128 samples per block)
    const int beam_id = blockIdx.x * TC_M;                            // Global beam index (e.g. 16 beams per block)
    const int freq_id = blockIdx.z;                                   // Global frequency index (e.g. each z-block works on a single coarse channel)
    const int n_freq = gridDim.z;                                     // Total number of channels

    // Variables ending with offset are commonly used numbers
    const int wg_offset = n_beam * n_elem;                            // Offset for beam weights
    const int in_offset = n_elem * n_time;                            // Offset for raw input data
    const int ot_offset = n_time * N_POL * n_freq;                    // Offset for output data (voltage)
    const int n_downsample = right - left - 1;                        // Number of samples which are NOT discarded
    const int n_dropsample = n_time/(FFT32_POINTS*integrate) * (FFT32_POINTS - n_downsample); // Number of total discarded samples

    // Local register variables
    int2 voltage[N_POL][N_WARPS]; // Array stored in local register, holding 2x2x4 (Polarisation, Beam, Time) beamformed voltage data . Last dimension holding 2 different beams, each 4 samples.
    char2 tmp; // Local register variable for splitting imag and real part

    // wmma::fragments below are used for Tensor core operations
    wmma::fragment<wmma::accumulator, TC_M, TC_N, TC_K, int32_t> out_frag_real[N_WARPS];
    wmma::fragment<wmma::accumulator, TC_M, TC_N, TC_K, int32_t> out_frag_imag[N_WARPS];
    wmma::fragment<wmma::matrix_a, TC_M, TC_N, TC_K, int8_t, wmma::row_major> wg_frag_real[(WARP_SIZE/TC_K)];
    wmma::fragment<wmma::matrix_a, TC_M, TC_N, TC_K, int8_t, wmma::row_major> wg_frag_imag[(WARP_SIZE/TC_K)];
    wmma::fragment<wmma::matrix_b, TC_M, TC_N, TC_K, int8_t, wmma::row_major> in_frag_real[N_WARPS];
    wmma::fragment<wmma::matrix_b, TC_M, TC_N, TC_K, int8_t, wmma::row_major> in_frag_imag[N_WARPS];

    // shared memory for cGEMM (Size: 9536 Bytes)
    __shared__ int8_t smem_wdata_real[TC_M * WARP_SIZE]; // shared memory for beam weight data for real part
    __shared__ int8_t smem_wdata_imag[TC_M * WARP_SIZE]; // shared memory for beam weight data for imagainary part
    __shared__ int8_t smem_idata_real[TC_N * WARP_SIZE * N_WARPS]; // shared memory for raw voltage data for real part
    __shared__ int8_t smem_idata_imag[TC_N * WARP_SIZE * N_WARPS]; // shared memory for raw voltage data for imag part
    __shared__ int    smem_odata_real[N_WARPS * TC_N * TC_M]; // Contains the real result of the cGEMM (8 x 16 x 16)
    __shared__ int    smem_odata_imag[N_WARPS * TC_N * TC_M]; // Contains the imag result of the cGEMM (8 x 16 x 16)
    // shared memory for FFT (Size: 5376)
    __shared__ float2 smem_fft_x[N_WARPS][WARP_SIZE]; // FFT for X-polarisation
    __shared__ float2 smem_fft_y[N_WARPS][WARP_SIZE]; // FFT for Y-polarisation
    __shared__ float2 smem_tw[FFT32_STAGES][WARP_SIZE]; // Twiddle factors for FFT-DIT algorithm

    // Shared memory for output data prodcuts (Size: 6400)
    // e.g. Stokes I high temporal resolution and full Stokes high frequency resolution
    __shared__ float4  smem_stokes[N_WARPS][WARP_SIZE]; // Contains beamformed full Stokes spectra (temporary - not integrated in time) -> high frequency resolution
    __shared__ float4  smem_stokes_integrated[N_BEAMS_BLOCK][WARP_SIZE]; // Contains integrated and beamformed full Stokes spectra -> high frequency resolution
    __shared__ float   smem_stokes_i[N_WARPS][WARP_SIZE]; // Contains beamformed Stokes I spectra (temporary - not integrated in time) -> high time resolution
    __shared__ float   smem_stokes_i_o[N_WARPS*N_WARPS];  // Contains integrated and beamformed Stokes I spectra -> high time resolution

    smem_stokes_integrated[warp_id*2][thrd_id] = {0,0,0,0};
    smem_stokes_integrated[warp_id*2+1][thrd_id] = {0,0,0,0};
    // Total shared memory size: 21312
    /**
    * Description below summarizes the pprocedure for the cGEMM (beamformin)
    *   complex general matrix multiplication (cGEMM)
    *     1. initialize output fragement / accumulator with zeros
    *     2. Load beam weights from global to shared memory and seperate imag and real compoenent
    *     3. Load input data from global to shared memory and seperate imag and real compoenent
    *     4. Apply cGEMM with Tensor cores
    *       4.1 Load data from shared memory to tensor core
    *       4.2 Execute 4 MMAs (complex MMA)
    *       4.3 Offload result from tensor core to shared memory
    *     5. Store output of cGEMM in thread register
    *     6. Offload N_MAX_RBEAMS to global memory
    **/
    for(int t = 0; t < integrate; t+=N_WARPS / 2)
    {
      // Iterate over polarisations
      for(int p = 0; p < N_POL; p++)
      {
        wmma::fill_fragment(out_frag_real[warp_id], 0);
        wmma::fill_fragment(out_frag_imag[warp_id], 0);
        // Iterate over all elements
        for(int i = 0; i < n_elem; i+=WARP_SIZE)
        {
          /**
          * Loading beam weights
          *   16 beams and 32 elements per iteration of "i" (batch size of shared mem = 16 x 32)
          **/
          for(int ii = 0; ii < TC_M; ii += N_WARPS)
          {
            // Load data to local register variable
            tmp = wdata[freq_id * wg_offset * N_POL   // Frequency axis
              + p * wg_offset                         // Polarisation axis
              + (beam_id + ii + warp_id) * n_elem     // Beam axis
              + i + thrd_id                           // Element axis
            ];
            // Split real and imaginary part
            smem_wdata_real[(warp_id + ii) * WARP_SIZE + thrd_id] = tmp.x;
            smem_wdata_imag[(warp_id + ii) * WARP_SIZE + thrd_id] = tmp.y;
          }
          /**
          * Loading raw input data
          *   8 tiles of size 16 x 32 (shared mem = 8 x 16 x 32)
          **/
          for(int ii = 0; ii < TC_N; ii++)
          {
            // Lower warps (0-3) operate on odd rows, upper warps (4-7) operate on even rows
            int offset = 0;
            if( warp_id > 3 )
              offset = TC_K;
            // Load data to local register variable
            if((i + ii + offset) >= n_elem)
            {
              tmp = {0,0};
            }
            else{
              tmp = idata[freq_id * N_POL * in_offset // Frequency axis
              + in_offset * p                         // Polarisation axis
              + (i + ii + offset) * n_time            // Element axis
              + time_id + (t+warp_id % 4) * WARP_SIZE + thrd_id                // Time axis
              ];
            }
            // Split real and imaginary part
            smem_idata_real[(ii + offset) * N_WARPS * TC_N + time_id_offset] = tmp.x;
            smem_idata_imag[(ii + offset) * N_WARPS * TC_N + time_id_offset] = tmp.y;
          }
  __syncthreads(); // After loading data from global to shared memory, block-wide sychronisation is needed
          int cnt = 0; // Just used for indexing the wmma::fragments
          /**
          * Perform complex matrix multiplication
          **/
          for(int ii = 0; ii < WARP_SIZE; ii+=TC_K)
          {
            // Load batches to tensor cores
            wmma::load_matrix_sync(wg_frag_real[cnt], &smem_wdata_real[ii], WARP_SIZE);
            wmma::load_matrix_sync(wg_frag_imag[cnt], &smem_wdata_imag[ii], WARP_SIZE);
            wmma::load_matrix_sync(in_frag_real[warp_id], &smem_idata_real[warp_id * TC_N + ii * TC_N * N_WARPS], TC_N * N_WARPS);
            wmma::load_matrix_sync(in_frag_imag[warp_id], &smem_idata_imag[warp_id * TC_N + ii * TC_N * N_WARPS], TC_N * N_WARPS);
            // smem_wdata_real * smem_idata_real -> real component (MMA 16x16 * 16x16 = 16x16)
            wmma::mma_sync(out_frag_real[warp_id], wg_frag_real[cnt], in_frag_real[warp_id], out_frag_real[warp_id]);
            // smem_wdata_real * smem_idata_imag -> imaginary component (MMA 16x16 * 16x16 = 16x16)
            wmma::mma_sync(out_frag_imag[warp_id], wg_frag_real[cnt], in_frag_imag[warp_id], out_frag_imag[warp_id]);
            // smem_wdata_imag * smem_idata_real -> imagainary component (MMA 16x16 * 16x16 = 16x16)
            wmma::mma_sync(out_frag_imag[warp_id], wg_frag_imag[cnt], in_frag_real[warp_id], out_frag_imag[warp_id]);
            // -1 * smem_wdata_imag * smem_idata_imag -> real component (MMA 16x16 * 16x16 = 16x16)
  #pragma unroll
            for(int k = 0; k < in_frag_imag[warp_id].num_elements; k++)
            {
              in_frag_imag[warp_id].x[k] *= -1;
            }
__syncthreads();
            wmma::mma_sync(out_frag_real[warp_id], wg_frag_imag[cnt], in_frag_imag[warp_id], out_frag_real[warp_id]);
            cnt ++;
          }
        }

        wmma::store_matrix_sync(&smem_odata_real[warp_id * TC_N], out_frag_real[warp_id], TC_N * N_WARPS, wmma::mem_row_major);
        wmma::store_matrix_sync(&smem_odata_imag[warp_id * TC_N], out_frag_imag[warp_id], TC_N * N_WARPS, wmma::mem_row_major);
__syncthreads();

        // Store the outcome of the cGEMM in local register array (voltage[p][i])
        // This is necessary, since only one polarisation is computed at a time and
        // must be stored before the next polarisation is computed.
        // To reduce the shared memory space, we can make use of the local registers.
        // Each thread stores 2x2 beams (p=0=X and p=1=Y) each with 4 samples
        // Two consecutive samples in the local array have a "true" distance of 32 samples.
        // Samples of the first and second beam are stored in the lower and upper 4 positions, respectivly.
        for(int i = 0; i < N_WARPS / 2; i++)
        {
          voltage[p][i].x = smem_odata_real[((warp_id*2) * (N_WARPS / 2 * WARP_SIZE)) + i * WARP_SIZE + thrd_id] / SCALE_FACTOR;
          voltage[p][i].y = smem_odata_imag[((warp_id*2) * (N_WARPS / 2 * WARP_SIZE)) + i * WARP_SIZE + thrd_id] / SCALE_FACTOR;
          voltage[p][N_WARPS/2+i].x = smem_odata_real[((warp_id*2+1) * (N_WARPS / 2 * WARP_SIZE)) + i * WARP_SIZE + thrd_id] / SCALE_FACTOR;
          voltage[p][N_WARPS/2+i].y = smem_odata_imag[((warp_id*2+1) * (N_WARPS / 2 * WARP_SIZE)) + i * WARP_SIZE + thrd_id] / SCALE_FACTOR;
__syncthreads();
          // Offload the channelized voltage beams (XY), we will just offload N_MAX_RBEAMS beams (data rate reduction).
          if(beam_id < N_MAX_RBEAMS)
          {
            const int ovol_idx = (beam_id + warp_id * 2) * ot_offset   // Beam axis
              + freq_id * N_POL * n_time                               // Frequency axis
              + p * n_time                                             // Polarisation axis
              + time_id + (i+t) * WARP_SIZE + thrd_id;             // Time axis
            odata_vol[ovol_idx] = voltage[p][i];
            odata_vol[ovol_idx + ot_offset] = voltage[p][N_WARPS / 2+i];
          }
        }
      }
      /**
      *  --- End of cGEMM ---
      **/

      /**
      * --- 32-point FFT ---
      *   1. Compute the bit reverse position
      *   2. Load twiddle factors (only a subset (=stages) of warps is required)
      *   3. Load beamformed data from register to shared memory
      *   4. Perform FFT-DIT algorithm - Note: Lower 16 threads operate on X-Pol, upper 16 threads on Y-Pol
      **/
      // Determine index position with the bitrev()-function / new ordering
      uint8_t pos = bitrev((uint8_t)thrd_id, 8-FFT32_STAGES);
      // Load twiddle factors to shared memory
      if(warp_id < FFT32_STAGES)
      {
        smem_tw[warp_id][thrd_id] = twiddle[warp_id * FFT32_POINTS + thrd_id];
      }

      for(int i = 0; i < N_WARPS; i++)
      {
        // Load beamformed raw voltage data from register into shared memory
        // We apply 2x 32-point FFTs per warp
        // And a single dual pol beam per iteration
        smem_fft_x[warp_id][pos].x = (float)voltage[0][i].x;  // 1st FFT-batch per warp / polarisation x
        smem_fft_x[warp_id][pos].y = (float)voltage[0][i].y;  // 1st FFT-batch per warp / polarisation x
        smem_fft_y[warp_id][pos].x = (float)voltage[1][i].x;  // 2nd FFT-batch per warp / polarisation y
        smem_fft_y[warp_id][pos].y = (float)voltage[1][i].y;  // 2nd FFT-batch per warp / polarisation y

        int op, k;
        float2 tw;

__syncthreads();
        // Apply FFT-DIT algorithm (32-point FFT)
        for(int ii = 0; ii < FFT32_STAGES; ii++)
        {
            op = 0x01 << ii;
            // If thread is one of the lower 16, apply FFT for x-polarisation
            if( thrd_id < WARP_SIZE / 2)
            {
              k = op*((int)thrd_id/op + 1) + thrd_id;
              tw = cmul(smem_fft_x[warp_id][k], smem_tw[ii][thrd_id]);
              smem_fft_x[warp_id][k]      = csub(smem_fft_x[warp_id][k - op], tw);
              smem_fft_x[warp_id][k - op] = cadd(smem_fft_x[warp_id][k - op], tw);
            }
            // Else thread is one of the upper 16, apply FFT for y-polarisation
            else
            {
              k = op*((int)(thrd_id - WARP_SIZE/2) / op + 1) + (thrd_id - WARP_SIZE/2);
              tw = cmul(smem_fft_y[warp_id][k], smem_tw[ii][thrd_id]);
              smem_fft_y[warp_id][k]      = csub(smem_fft_y[warp_id][k - op], tw);
              smem_fft_y[warp_id][k - op] = cadd(smem_fft_y[warp_id][k - op], tw);
            }
__syncwarp();
        }
        /**
        * --- End of FFT ---
        **/

        /**
        * Compute high time resolution Stokes I and high frequency resolution full Stokes
        *   1. Discard outer sample according to the oversample ration (e.g. 32/27 -> 2 samples left; 3 samples right
        *   2. Detect Stokes paramter (stokes_iquv())
        *   3. Integrate the spectras for high frequency full Stokes (iterative -> variable integrate)
        *   4. Sum up "fine" channels of one spectra (only Stokes I)
        *   5. Offload high time res. Stokes I data
        *   6. If all spectras are integrated, offload high frequency full Stokes data
        **/
        // Downsample by discarding edge bins of the FFT (left to right)
        if( thrd_id > left && thrd_id < right )
        {
          smem_stokes[warp_id][thrd_id] = stokes_iquv(smem_fft_x[warp_id][thrd_id], smem_fft_y[warp_id][thrd_id]);
        }
        else
        {
          smem_stokes[warp_id][thrd_id] = {0,0,0,0};
        }
        smem_stokes_i[warp_id][thrd_id] = smem_stokes[warp_id][thrd_id].x;

__syncwarp();
        // Integrate 2nd FFT spectras (Full Stokes beams) / add_iquv
        // Integrate frequency bins 2nd FFT for high time resolution Stokes I beams / warp_reduce
        if(i <  N_WARPS / 2){
          smem_stokes_integrated[warp_id*2][thrd_id] = add_iquv(smem_stokes_integrated[warp_id*2][thrd_id], smem_stokes[warp_id][thrd_id]);
          warp_reduce(smem_stokes_i[warp_id], thrd_id, &smem_stokes_i_o[(warp_id*2) * N_WARPS/2 + i]);
        }else{
          smem_stokes_integrated[warp_id*2+1][thrd_id] = add_iquv(smem_stokes_integrated[warp_id*2+1][thrd_id], smem_stokes[warp_id][thrd_id]);
          warp_reduce(smem_stokes_i[warp_id], thrd_id, &smem_stokes_i_o[(warp_id*2+1) * N_WARPS/2 + i - INTEGRATE_ITER]);
        }
      }
__syncthreads();
      // Offload stokes I high temporal resolution data to global memory
      if(warp_id < 2)
      {
        const int oidx = (beam_id + thrd_id / INTEGRATE_ITER + warp_id * N_WARPS) * n_freq * n_time/FFT32_POINTS
          + freq_id * n_time/FFT32_POINTS
          + blockIdx.y * integrate + (thrd_id % INTEGRATE_ITER) + t;
        odata_sti[oidx] = smem_stokes_i_o[warp_id * WARP_SIZE + thrd_id];
      }
    }
    // Offload full Stokes high freq res. spectra to global memory
    const int oidx_even = (beam_id + 2*warp_id) * n_freq * (n_time/integrate-n_dropsample)
      + freq_id * (n_time/integrate-n_dropsample)
      + blockIdx.y * n_downsample + thrd_id - left - 1;
    const int oidx_odd = (beam_id + 2*warp_id+1) * n_freq * (n_time/integrate-n_dropsample)
      + freq_id * (n_time/integrate-n_dropsample)
      + blockIdx.y * n_downsample + thrd_id - left - 1;
    if(thrd_id > left && thrd_id < right)
    {
      odata_stf[oidx_even] = smem_stokes_integrated[warp_id*2][thrd_id];
      odata_stf[oidx_odd] = smem_stokes_integrated[warp_id*2+1][thrd_id];
    }
#endif
}


} //namespace beamforming
} //namespace cryopaf


#endif
