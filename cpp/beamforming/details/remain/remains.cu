



// Input: F-P-E-T
// Weights: F-P-B-E
// Out vol: B-F-T-P
// Out stokes I: B-F-T
// Out full stokes: B-F-T-S
// __global__ void beamform(
//   const int16_t* idata,
//   const __half2* wdata,
//   const __half2* twiddle,
//   __half2* odata_vol,
//   __half* odata_sti,
//   __half4* odata_stf,
//   const unsigned n_time,
//   const unsigned n_elem,
//   const unsigned n_beam,
//   const unsigned nfft,
//   const unsigned stages,
//   const unsigned left,
//   const unsigned right)
// {
//     __half2 voltage_x = {0,0};
//     __half2 voltage_y = {0,0};
//     __half2 tw;
//     __half4 s_param;
//     int op, k, idx, iter=1, integration=1;
//
//     const int n_freq = gridDim.z;
//     const int thrd_idx = threadIdx.x;
//     const int thrd_idy = threadIdx.y;
//
//     const int beam_id = blockDim.x * blockIdx.x + thrd_idx;  // Beam index
//     const int time_id = blockDim.y * blockIdx.y + thrd_idy;  // Time index
//     const int freq_idx = blockIdx.z;  // Frequency index
//
//     const int size_idata = n_elem * n_time; // Size of input data used in a single thread block
//     const int size_wdata = n_elem * n_beam; // Size of weight data used in a single thread block
//     const int size_odata = n_time * n_freq; // Size of output data used in a single thread block
//
//     const int idata_offset = freq_idx * 2 * size_idata; // Offset index for input data of a thread block
//     const int wdata_offset = freq_idx * 2 * size_wdata; // Offset index for output data of a thread block
//     const int odata_offset = beam_id * size_odata; // Offset index for weight data of a thread block
//
//     const int ovol_idx = odata_offset * 2 + freq_idx * n_time * 2 + time_id * 2; // Index for raw voltage beams
//     const int osti_idx = odata_offset     + freq_idx * n_time + time_id; // Index for Stokes I beams
//     const int ostf_idx = odata_offset / integration  + freq_idx * n_time / integration + time_id; // Index for full Stokes beams
//
//     // shared memory for cGEMM
//     __shared__ __half2 s_idata_x[WARP_SIZE][WARP_SIZE]; // shared memory for raw voltage data of polarization x
//     __shared__ __half2 s_idata_y[WARP_SIZE][WARP_SIZE]; // shared memory for raw voltage data of polarization y
//     __shared__ __half2 s_wdata_x[WARP_SIZE][WARP_SIZE]; // shared memory for beam weight data of polarization x
//     __shared__ __half2 s_wdata_y[WARP_SIZE][WARP_SIZE]; // shared memory for beam weight data of polarization y
//
//     // shared memory for 32-point FFT
//     __shared__ __half2 s_fft_x[WARP_SIZE][WARP_SIZE]; // shared memory for 32-point FFT of polarization x
//     __shared__ __half2 s_fft_y[WARP_SIZE][WARP_SIZE]; // shared memory for 32-point FFT of polarization y
//     __shared__ __half2 s_tw[5][WARP_SIZE]; // Twarp_iddle factors for 32-point FFT
//
//     // shared memory for output data
//     __shared__ __half s_stokes_i[WARP_SIZE][WARP_SIZE];
//     __shared__ __half s_stokes_i_o[WARP_SIZE][WARP_SIZE];
//     __shared__ __half4 s_stokes_f[WARP_SIZE][WARP_SIZE];
//
//     // Load the twiddle factors for the FFT into shared memory / We only need 5 warps for loading (5 stages each with 32 twiddle factors)
//     if(thrd_idy < stages)
//     {
//       s_tw[thrd_idy][thrd_idx] = twiddle[thrd_idy * nfft + thrd_idx];
//     }
//     /*
//       First we start with the actual beamforming by applying a
//       complex General Matrix Multiplication (cGEMM) between raw voltages and beam weights.
//       We need to iterate over each PAF element.
//     */
//     for( int k = 0; k < n_elem; k+=WARP_SIZE )
//     {
//       // Load raw voltage data from global to shared memory
//       if(time_id < n_time && thrd_idx + k < n_elem )
//       {
//         // s_idata_x[thrd_idy][thrd_idx] = int16_t2half2(idata[idata_offset + time_id * n_elem + thrd_idx + k]);
//         // s_idata_y[thrd_idy][thrd_idx] = int16_t2half2(idata[idata_offset + size_idata + time_id * n_elem + thrd_idx + k]);
//         s_idata_x[thrd_idy][thrd_idx] = int16_t2half2(idata[idata_offset + (thrd_idx + k) * n_time + time_id]);
//         s_idata_y[thrd_idy][thrd_idx] = int16_t2half2(idata[idata_offset + size_idata + (thrd_idx + k) * n_time + time_id]);
//       }
//       // Else it is outside the time and element index, so we need to zero pad.
//       else
//       {
//         s_idata_x[thrd_idy][thrd_idx] = {0,0};
//         s_idata_y[thrd_idy][thrd_idx] = {0,0};
//       }
//       // Load beam weights from global to shared memory
//       if(thrd_idy == 0 && time_id == 0)
//       {
//
//       }
//       if(beam_id < n_beam && thrd_idy + k < n_elem )
//       {
//         // s_wdata_x[thrd_idy][thrd_idx] = wdata[wdata_offset + (thrd_idy + k) * n_beam + beam_id];
//         // s_wdata_y[thrd_idy][thrd_idx] = wdata[wdata_offset + size_wdata + (thrd_idy + k) * n_beam + beam_id];
//           s_wdata_x[thrd_idy][thrd_idx] = wdata[wdata_offset + beam_id * n_elem + thrd_idy + k];
//           s_wdata_y[thrd_idy][thrd_idx] = wdata[wdata_offset + size_wdata + beam_id * n_elem + thrd_idy + k];
//           // printf("GPU[%d][0][%d][%d]=%f+i*%f\n",freq_idx, beam_id, thrd_idy + k, (float)s_wdata_x[thrd_idy][thrd_idx].x, (float)s_wdata_x[thrd_idy][thrd_idx].y);
//           // printf("GPU[%d][1][%d][%d]=%f+i*%f\n",freq_idx, beam_id, thrd_idy + k, (float)s_wdata_y[thrd_idy][thrd_idx].x, (float)s_wdata_y[thrd_idy][thrd_idx].y);
//       }
//       else
//       {
//         s_wdata_x[thrd_idy][thrd_idx] = {0,0};
//         s_wdata_y[thrd_idy][thrd_idx] = {0,0};
//       }
//       // Before applying cGEMM ensure that threads in a block are synced
// __syncthreads();
//       // apply cGEMM
//       for( int j = 0; j < WARP_SIZE; j++)
//       {
//         if(j + k == n_elem)
//           break;
//         voltage_x = cmadd(s_idata_x[thrd_idy][j], s_wdata_x[j][thrd_idx], voltage_x); // strided on s_idata
//         voltage_y = cmadd(s_idata_y[thrd_idy][j], s_wdata_y[j][thrd_idx], voltage_y); // strided on s_idata
//       }
//     }
//     // After applying the cGEMM, ensure all threads in a block are synced
// __syncthreads();
//     // Transfer the raw voltage beams back to the global memory (no downsampling takes place, but we limit the number of beams to N_MAX_RBEAMS)
//     if(beam_id < N_MAX_RBEAMS && beam_id < n_beam && time_id < n_time)
//     {
//       odata_vol[ovol_idx] = voltage_x;
//       odata_vol[ovol_idx + 1] = voltage_y;
//     }
//
//     // Determine index position with the bitrev()-function / new ordering
//     uint8_t pos = bitrev((uint8_t)thrd_idx, 8-stages);
//     // Load beamformed raw voltage data from register into shared memory
//     // We apply 2 times 32-point FFTs per warp
//     s_fft_x[thrd_idy][pos] = voltage_x;  // 1st FFT-batch per warp / polarisation x
//     s_fft_y[thrd_idy][pos] = voltage_y;  // 2nd FFT-batch per warp / polarisation y
//
// __syncthreads();
//
//     if( thrd_idx > WARP_SIZE / 2)
//     {
//       idx = thrd_idx - 16;
//     }
//
//     // Apply FFT-DIT algorithm (32-point FFT)
//     for(int i = 0; i < stages; i++)
//     {
//         op = 0x01 << i;
//         // If thread is one of the lower 16, apply FFT for x-polarisation
//         if( thrd_idx < WARP_SIZE / 2)
//         {
//           k = op*((int)thrd_idx/op + 1) + thrd_idx;
//           tw = __hmul2(s_idata_x[thrd_idy][k], s_tw[i][thrd_idx]);
//           s_fft_x[thrd_idy][k]      = __hsub2(s_fft_x[thrd_idy][k - op], tw);
//           s_fft_x[thrd_idy][k - op] = __hadd2(s_fft_x[thrd_idy][k - op], tw);
//         }
//         // Else thread is one of the upper 16, apply FFT for y-polarisation
//         else
//         {
//           k = op*((int)idx/op + 1) + idx;
//           tw = __hmul2(s_idata_y[thrd_idy][k], s_tw[i][thrd_idx]);
//           s_fft_y[thrd_idy][k]      = __hsub2(s_fft_y[thrd_idy][k - op], tw);
//           s_fft_y[thrd_idy][k - op] = __hadd2(s_fft_y[thrd_idy][k - op], tw);
//         }
// __syncwarp();
//     }
//     // Downsample by discarding left and right FFT-bins and Stokes I detection
//     s_param.i = 0;
//     s_param.q = 0;
//     s_param.u = 0;
//     s_param.v = 0;
//     if( thrd_idx > left && thrd_idx < right)
//     {
//       s_param = stokes_iquv(s_fft_x[thrd_idy][thrd_idx], s_fft_y[thrd_idy][thrd_idx]);
//       // Integrate in time for high frequency resolution full Stokes beams
//       s_stokes_f[thrd_idy][thrd_idx-left] = (s_stokes_f[thrd_idy][thrd_idx-left] + s_param);
//     }
//     s_stokes_i[thrd_idy][thrd_idx] = s_param.i;
//     // Integrate frequency bins for high time resolution Stokes I beams
//     warp_reduce(s_stokes_i[thrd_idy], thrd_idx, &s_stokes_i_o[thrd_idy][iter]);
//
//     // Copy stokes I high temporal resolution data back to global memory
//     if( thrd_idx < iter )
//     {
//       odata_sti[osti_idx] = s_stokes_i_o[thrd_idy][thrd_idx];
//     }
//     // Copy full Stokes high frequency resolution data back to global memory
//     if( thrd_idx < 27 ) // Assumed oversampling ratio 32/27 -> TODO: dont hardcode here
//     {
//       odata_stf[ostf_idx].i = s_stokes_f[thrd_idy][thrd_idx].i;
//       odata_stf[ostf_idx].q = s_stokes_f[thrd_idy][thrd_idx].q;
//       odata_stf[ostf_idx].u = s_stokes_f[thrd_idy][thrd_idx].u;
//       odata_stf[ostf_idx].v = s_stokes_f[thrd_idy][thrd_idx].v;
//     }
// }
