#ifndef UTILS_CU
#define UTILS_CU

/** UTILS **/

#define PI            2*acos(0.0)


namespace cryopaf {
namespace beamforming {

struct __half4
{
  __half i;
  __half q;
  __half u;
  __half v;
  __device__ __half4 operator + (const __half4& val)
  {
    i = __hadd(i,val.i);
    q = __hadd(q,val.q);
    u = __hadd(u,val.u);
    v = __hadd(v,val.v);
    return *this;
  }
};


__device__ int bitrev(uint8_t idx, unsigned shift)
{
    idx = (idx & 0xF0) >> 4 | (idx & ~0xF0) << 4;
    idx = (idx & 0xCC) >> 2 | (idx & ~0xCC) << 2;
    idx = (idx & 0xAA) >> 1 | (idx & ~0xAA) << 1;
    return idx >> shift;
}

__device__ __half4 stokes_iquv(const __half2 x, const __half2 y)
{
  __half4 val;
  val.i = __habs(x.x*x.x + x.y*x.y) + __habs(y.x*y.x + y.y*y.y);
  val.q = __habs(x.x*x.x + x.y*x.y) - __habs(y.x*y.x + y.y*y.y);
  val.u = __hmul(2, x.x*y.x + x.y*y.y);
  val.v = __hmul(- 2, x.y*y.x - x.x*y.y);
  return val;
}

__device__ __host__ float4 stokes_iquv(const float2 x, const float2 y)
{
  float4 val;
  val.x = fabsf(x.x*x.x + x.y*x.y) + fabsf(y.x*y.x + y.y*y.y); // I
  val.y = fabsf(x.x*x.x + x.y*x.y) - fabsf(y.x*y.x + y.y*y.y); // Q
  val.z = 2 *  (x.x*y.x + x.y*y.y); // U
  val.w = -2 * (x.y*y.x - x.x*y.y); // V
  return val;
}

__device__ __host__ float4 add_iquv(const float4 x, const float4 y)
{
  float4 val;
  val.x = x.x + y.x;
  val.y = x.y + y.y;
  val.z = x.z + y.z;
  val.w = x.w + y.w;
  return val;
}

__host__ __device__ float stokes_i(const float2 x, const float2 y)
{
  return (x.x*x.x + x.y*x.y) + (y.x*y.x + y.y*y.y); // I
}

__device__ __half2 __hCmul2(__half2 a, __half2 b)
{
		const __half r = a.x * b.x - a.y * b.y;
		const __half i = a.x * b.y + a.y * b.x;

		__half2 val; val.x = r; val.y = i;
		return val;
}

template<typename T, typename U>
__host__ __device__ T cmadd(U a, U b, T c)
{
	T val;
	val.x = a.x * b.x - a.y * b.y + c.x;
	val.y = a.x * b.y + a.y * b.x + c.y;
	return val;
}

template<typename T>
__host__ __device__ T cadd(T a, T b)
{
	T val;
	val.x = a.x + b.x;
	val.y = a.y + b.y;
	return val;
}

template<typename T>
__host__ __device__ T csub(T a, T b)
{
	T val;
	val.x = a.x - b.x;
	val.y = a.y - b.y;
	return val;
}

template<typename T>
__host__ __device__ double cabs(T a)
{
	return (double)sqrt((double)(a.x * a.x + a.y * a.y));
}

template<typename T>
__host__ __device__ T cmul(T a, T b)
{
	return {(a.x * b.x - a.y * b.y), (a.x * b.y + a.y * b.x)};
}

__host__ void dft(int2* input, float2* output, int nfft)
{
  float2 w;
  float2 a;
  for(int i = 0; i < nfft; i++)
  {
    for(int ii = 0; ii < nfft; ii++)
    {
      w.x = cos(-2 * PI * ii * i / nfft);
      w.y = sin(-2 * PI * ii * i / nfft);
      a.x = (float)input[ii].x;
      a.y = (float)input[ii].y;
      output[i] = cmadd(w, a, output[i]);
    }
  }
}

__device__ __half2 int16_t2half2(int16_t in)
{
  __half2 out;
  out.x = __int2half_rz((int8_t)((in & 0xFF00) >> 8));
  out.y = __int2half_rz((int8_t)(in & 0x00FF));
  return out;
}

__host__ __device__ float2 int16_t2float2(int16_t in)
{
  float2 out;
  out.x = (float)((int8_t)((in & 0xFF00) >> 8));
  out.y = (float)((int8_t)(in & 0x00FF));
  return out;
}

} //namespace beamforming
} //namespace cryopaf

#endif
