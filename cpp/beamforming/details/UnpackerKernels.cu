#ifdef UNPACKER_CUH

namespace cryopaf{
namespace beamforming {


__device__ __forceinline__ uint64_t swap64(uint64_t x)
{
    uint64_t result;
    uint2 t;
    asm("mov.b64 {%0,%1},%2; \n\t"
        : "=r"(t.x), "=r"(t.y) : "l"(x));
    t.x = __byte_perm(t.x, 0, 0x0123);
    t.y = __byte_perm(t.y, 0, 0x0123);
    asm("mov.b64 %0,{%1,%2}; \n\t"
        : "=l"(result) : "r"(t.y), "r"(t.x));
    return result;
}

__global__
void unpack_spead_tfpet_to_fpet(__restrict__ const char2* idata, __restrict__ char2* odata)
{
    int time_idx = threadIdx.x;   // inner time axis
    int elem_idx = blockIdx.y;    // element axis
    int chan_idx = blockIdx.z;    // frequency axis
    int heap_idx = blockIdx.x;    // outer time axis
    int n_heap =  gridDim.x;
    int n_elem =  gridDim.y;
    int n_chan =  gridDim.z;

    int in_idx_x = heap_idx * n_chan * NPOL_SAMP * n_elem * NSAMP_PER_HEAP
      + chan_idx * NPOL_SAMP * n_elem * NSAMP_PER_HEAP
      + elem_idx * NSAMP_PER_HEAP
      + time_idx;

    int in_idx_y = heap_idx * n_chan * NPOL_SAMP * n_elem * NSAMP_PER_HEAP
      + chan_idx * NPOL_SAMP * n_elem * NSAMP_PER_HEAP
      + n_elem * NSAMP_PER_HEAP
      + elem_idx * NSAMP_PER_HEAP
      + time_idx;

    int out_idx_x = chan_idx * NPOL_SAMP * n_elem * n_heap * NSAMP_PER_HEAP
      + elem_idx * n_heap * NSAMP_PER_HEAP
      + heap_idx * NSAMP_PER_HEAP
      + time_idx;

    int out_idx_y = chan_idx * NPOL_SAMP * n_elem * n_heap * NSAMP_PER_HEAP
      + n_elem * n_heap * NSAMP_PER_HEAP
      + elem_idx * n_heap * NSAMP_PER_HEAP
      + heap_idx * NSAMP_PER_HEAP
      + time_idx;

    for(int i = 0; i < NSAMP_PER_HEAP; i+=NTHREADS)
    {
      odata[out_idx_x] = idata[in_idx_x];
      odata[out_idx_y] = idata[in_idx_y];
      in_idx_x  += NTHREADS;
      in_idx_y  += NTHREADS;
      out_idx_y += NTHREADS;
      out_idx_x += NTHREADS;
    }
}


} //namespace beamforming
} //namespace cryopaf


#endif
