/*
* Beamformer.cuh
* Author: Niclas Esser <nesser@mpifr-bonn.mpg.de>
* Description:
*  This file consists of a single class (Beamformer<ComputeType>). An object of Beamformer
*  can be used o either perform a Stokes I detection or raw voltage beamforming
*  on a GPU.
*  Both beamforming kernels expect the same dataproduct (linear aligned in device memory)
*    Input:  F-P-T-E
*    Weight: F-P-B-E
*    Output: F-T-B-P (voltage beams)
*    Output: F-T-B   (Stokes I beams)
*/

#ifndef BEAMFORMER_CUH_
#define BEAMFORMER_CUH_

#include <cuda.h>
#include <cuda_fp16.h>
#include <thrust/device_vector.h>
#include <cmath>

#include "psrdada_cpp/cuda_utils.hpp"
#include "psrdada_cpp/multilog.hpp"
#include "details/utils.cu"

namespace cryopaf{
namespace beamforming{

// Constants for beamform kernels
#define N_WARPS       8
#define WARP_SIZE     32
#define N_THREAD      (N_WARPS * WARP_SIZE)
#define N_MAX_RBEAMS  16
#define TC_K          16
#define TC_M          16
#define TC_N          16
#define N_POL         2
#define SCALE_FACTOR  1
#define N_BEAMS_BLOCK 16
#define FFT32_STAGES  5
#define FFT32_POINTS  32
#define FFT128_POINTS 128
#define INTEGRATE_ITER 4
#define SAMPLES_ITER  128

#define CONFIG_ERROR  3
/**
* @brief 	GPU kernel to perform raw voltage beamforming
*
* @detail Template type T has to be etiher T=float2 or T=__half2
*
* @param	T* idata       pointer to input memory (format: F-P-T-E)
* @param	T* wdata       pointer to beam weight memory (format: F-P-B-E)
* @param	T* odata	     pointer to output memory (format: F-T-B)
* @param	int time	     Width of time dimension (T)
* @param	int elem       Number of elements (E)
* @param	int beam       Number of beams (B)
*/
__global__ void beamform(
  const char2* idata,
  const char2* wdata,
  const float2* twiddle,
  int2* odata_vol,
  float* odata_sti,
  float4* odata_stf,
  const unsigned n_time,
  const unsigned n_elem,
  const unsigned n_beam,
  const unsigned integrate,
  const unsigned left,
  const unsigned right);


template<class ComputeType>
class Beamformer{

// Internal typedefintions
private:
  typedef decltype(ComputeType::x) real; // Just necessary for twiddle factors (Datatype of FFT)
  typedef decltype(ComputeType::y) imag; // Just necessary for twiddle factors (Datatype of FFT)

// Public functions
public:
  /**
  * @brief  constructs an object of Beamformer<ComputeType> (ComputeType=float2 or ComputeType=__half2)
  *
  * @param	cudaStream_t& stream      Object of cudaStream_t to allow parallel copy + processing (has to be created and destroyed elsewhere)
  * @param  std::size_t sample        Number of samples to process in on kernel launch (at least 128)
  * @param  std::size_t channel       Number of channels to process in on kernel launch (no restrictions)
  * @param  std::size_t element       Number of elements to process in on kernel launch (no restrictions)
  * @param  std::size_t beam          Number of beams to process in on kernel launch (at least 16)
  * @param  std::size_t integration   Samples to be integrated, has to be power of 2 and smaller 32
  */
  Beamformer(
    cudaStream_t& stream,
    std::size_t sample,
    std::size_t channel,
    std::size_t element,
    std::size_t beam,
    std::size_t nfft,
    std::size_t num,
    std::size_t dnum,
    std::size_t integration);

  /**
  * @brief  deconstructs an object of Beamformer<ComputeType> (ComputeType=float2 or ComputeType=__half2)
  */
  ~Beamformer();

  /**
  * @brief 	Launches voltage beamforming GPU kernel
  *
  * @param	ComputeType* input       pointer to input memory (format: F-P-T-E)
  * @param	ComputeType* weights     pointer to beam weight memory (format: F-P-B-E)
  * @param	ComputeType* output	     pointer to output memory (format: F-T-B-P)
  */
  void process(
      const char2* input,
      const char2* weights,
      int2* ovoltage,
      float* ostokesi,
      float4* ostokesf);

    /**
  	* @brief	Prints the block and grid layout of a kernel (used for debugging purposes)
  	*/
  	void print_layout();

    /**
  	* @brief	Prints the block and grid layout of a kernel (used for debugging purposes)
  	*/
    int check_config();
// Private functions
private:
    void __twid();
    ComputeType __get_tw(int i, int n);
// Private attributes
private:
  thrust::device_vector<float2> _twiddle;
  cudaStream_t& _stream;
  dim3 grid;
  dim3 block;
  std::size_t _sample;
  std::size_t _channel;
  std::size_t _element;
  std::size_t _beam;
  std::size_t _nfft;
  std::size_t _stages;
  std::size_t _left;
  std::size_t _right;
  std::size_t _integration;
};


} // namespace beamforming
} // namespace cryopaf

#include "beamforming/details/BeamformerKernels.cu"
#include "beamforming/src/Beamformer.cu"

#endif /* BEAMFORMER_CUH_ */
