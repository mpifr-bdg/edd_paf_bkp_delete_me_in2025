#include <thrust/device_vector.h>
#include <thrust/copy.h>
#include <thrust/complex.h>
#include <cuda.h>
#include <random>
#include <cmath>
#include <fstream>
#include <chrono>
#include <unordered_map>


#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

#include <psrdada_cpp/cli_utils.hpp>
#include <psrdada_cpp/dada_input_stream.hpp>
#include <psrdada_cpp/dada_output_stream.hpp>
#include <psrdada_cpp/multilog.hpp>

#include "beamforming/Pipeline.cuh"


const size_t SUCCESS = 0;
const size_t ERROR_IN_COMMAND_LINE = 1;
const size_t ERROR_UNHANDLED_EXCEPTION = 2;

using namespace psrdada_cpp;
using namespace cryopaf::beamforming;

template<typename T>
void launch(PipelineConfig& conf)
{
    MultiLog log(conf.logname);
    DadaOutputStream ovol(conf.ovol_key, log);
    DadaOutputStream osti(conf.osti_key, log);
    DadaOutputStream ostf(conf.ostf_key, log);
    Pipeline<DadaOutputStream, T> pipeline(conf, log, ovol, osti, ostf);
    DadaInputStream<decltype(pipeline)> input(conf.in_key, log, pipeline);
    input.start();
}

int main(int argc, char** argv)
{
    try
    {
        // Variables to store command line options
        PipelineConfig conf;

        // Parse command line
        namespace po = boost::program_options;
        po::options_description desc("Options");
        desc.add_options()
        ("help,h", "Print help messages")
        ("in_key", po::value<std::string>()->required()
          ->notifier([&conf](std::string key){conf.in_key = string_to_key(key);}), "Input dada key")
        ("out_vol_key", po::value<std::string>()->required()
          ->notifier([&conf](std::string key){conf.ovol_key = string_to_key(key);}), "Output dada key for channelized voltage beams")
        ("out_sti_key", po::value<std::string>()->required()
          ->notifier([&conf](std::string key){conf.osti_key = string_to_key(key);}), "Output dada key for high time resoltion stokes I beams")
        ("out_stf_key", po::value<std::string>()->required()
          ->notifier([&conf](std::string key){conf.ostf_key = string_to_key(key);}), "Output dada key for high frequency resoltion full stokes beams")
        ("samples", po::value<std::size_t>(&conf.n_samples)->default_value(262144), "Number of samples within one dada block")
        ("channels", po::value<std::size_t>(&conf.n_channel)->default_value(7), "Number of channels")
        ("elements", po::value<std::size_t>(&conf.n_elements)->default_value(36), "Number of elments")
        ("beams", po::value<std::size_t>(&conf.n_beam)->default_value(36), "Number of beams")
        ("nfft", po::value<std::size_t>(&conf.nfft)->default_value(32), "Number of beams")
        ("integration", po::value<std::size_t>(&conf.integration)->default_value(1), "Integration interval; must be multiple 2^n and smaller 32")
        ("device", po::value<int>(&conf.device_id)->default_value(0), "ID of GPU device")
        ("protocol", po::value<std::string>(&conf.protocol)->default_value("spead"), "Protocol of input data; supported 'spead' and 'dummy_input'.")
        ("log", po::value<std::string>(&conf.logname)->default_value("cryo_beamform.log"), "Directory of logfile");

        po::variables_map vm;
        try
        {
            po::store(po::parse_command_line(argc, argv, desc), vm);
            if ( vm.count("help")  )
            {
                std::cout << "Cryopaf -- The CryoPAF Controller implementations" << std::endl
                << desc << std::endl;
                return SUCCESS;
            }
            po::notify(vm);
        }
        catch(po::error& e)
        {
            std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
            std::cerr << desc << std::endl;
            return ERROR_IN_COMMAND_LINE;
        }
        launch<float2>(conf);
    }
    catch(std::exception& e)
    {
        std::cerr << "Unhandled Exception reached the top of main: "
            << e.what() << ", application will now exit" << std::endl;
        return ERROR_UNHANDLED_EXCEPTION;
    }
    return SUCCESS;
}
