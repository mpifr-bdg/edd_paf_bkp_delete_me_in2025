#ifdef WEIGHTMANAGER_HPP

namespace cryopaf {
namespace beamforming {

namespace bip = boost::interprocess;

template<class ComputeType, class InputType>
WeightManager<ComputeType, InputType>::WeightManager(WeightManagerConfig& config, psr::MultiLog& logger, red::Subscriber& subscriber)
  :
  conf(config),
  log(logger),
  sub(subscriber)
{
  // assign redis subscriber
  std::size_t size = conf.n_channel
    * conf.n_beam
    * conf.n_elements
    * conf.n_pol;
  expected_bytes = size * sizeof(InputType);
  vect_weights.resize(size);

  sub.on_message(boost::function<void (std::string, std::string)>
    (boost::bind(&WeightManager::parse_weights, this, _1, _2)));
  sub.subscribe({conf.redis_channel});

  // Prepare IPC POSSIX shared memory
  try
  {
    smem = bip::shared_memory_object(bip::open_only, "SharedMemoryWeights", bip::read_write);
  }
  catch(bip::interprocess_exception &ex)
  {
     BOOST_LOG_TRIVIAL(error) << "Error when trying to connect to shared memory: " << ex.what();
     exit(1);
  }
  try
  {
    //Map the whole shared memory in this process
    region = bip::mapped_region(smem, bip::read_write);
    //Get the address of the mapped region
    smem_addr = region.get_address();
  }
  catch(bip::interprocess_exception &ex)
  {
     BOOST_LOG_TRIVIAL(error) << "Error when trying to create mapped memory region: " << ex.what();
     exit(1);
  }
  qheader = new (smem_addr) QueueHeader();
  smem_weights = &(static_cast<ComputeType*>(smem_addr)[sizeof(QueueHeader)]);
  vect_weights = std::vector<ComputeType>(size);
}

template<class ComputeType, class InputType>
WeightManager<ComputeType, InputType>::~WeightManager()
{

}

template<class ComputeType, class InputType>
void WeightManager<ComputeType, InputType>::parse_weights(std::string channel, std::string msg)
{
  BOOST_LOG_TRIVIAL(debug) << "Received '" << msg << "' as message on redis channel: " << channel;
  std::ifstream file(msg, std::ios_base::in | std::ios_base::binary);
  if(!file)
  {
    BOOST_LOG_TRIVIAL(error) << "Could not open file " << msg << ". Weights not updated!";
  }
  else if((std::size_t)file.tellg() != expected_bytes)
  {
    BOOST_LOG_TRIVIAL(error) << "Size of file " << msg << " is " << file.tellg() << ", but expected "
      << expected_bytes << " bytes. Weights not updated!";
  }
  else
  {
    file.read(raw_weights.data(), expected_bytes);
    this->convert();
    BOOST_LOG_TRIVIAL(info) << "Updated beam weights!";
  }
}

template<class ComputeType, class InputType>
void WeightManager<ComputeType, InputType>::convert()
{
  InputType* p = reinterpret_cast<InputType*>(raw_weights.data());
  for(std::size_t i = 0; i < expected_bytes/sizeof(InputType); i++)
  {
    vect_weights[i] = (ComputeType)p[i];
  }
}


template<class ComputeType, class InputType>
void WeightManager<ComputeType, InputType>::start()
{
  std::cout << "WeightManager is running" << std::endl;
  qheader->stop = false;
  while(!qheader->stop)
  {
    try
    {
      sub.consume();
      updated = true;
    }
    catch (const red::Error &e)
    {
      std::cout << e.what() << std::endl;
    }
    if(updated)
    {
      bip::scoped_lock<bip::interprocess_mutex> lock(qheader->mutex);
      if(qheader->data_in)
      {
         std::cout << "Waiting for reading weights from shared memory" << std::endl;
         qheader->ready_to_write.wait(lock);
      }
      std::cout << "Writing new weights to shared memory " << std::endl;
      memcpy(smem_weights, (void*)&vect_weights[0], vect_weights.size()*sizeof(ComputeType));
      qheader->data_in = true;
      qheader->ready_to_read.notify_all();
      update_cnt++;
      updated = false;
    }
  }
}

} //namespace beamforming
} //namespace cryopaf

#endif
