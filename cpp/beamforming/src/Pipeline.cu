#ifdef PIPELINE_CUH_

namespace cryopaf{
namespace beamforming{

template<class HandlerType, class ComputeType>
Pipeline<HandlerType, ComputeType>::Pipeline(
		PipelineConfig& conf,
    psrdada_cpp::MultiLog &log,
    HandlerType &handler_vol,
    HandlerType &handler_sti,
    HandlerType &handler_stf
):
		_conf(conf),
		_log(log),
		_handler_vol(handler_vol),
		_handler_sti(handler_sti),
		_handler_stf(handler_stf)
{
	// Check if passed template types are valid
	if(!(std::is_same<ComputeType, float2>::value))
	// && !(std::is_same<ComputeType,__half2>::value))
	{
		BOOST_LOG_TRIVIAL(error) << "PipelineError: Template type not supported";
		exit(1);
	}
#ifdef DEBUG
	// Cuda events for profiling
  CUDA_ERROR_CHECK( cudaEventCreate(&start) );
	CUDA_ERROR_CHECK( cudaEventCreate(&stop) );
#endif
	// Create streams
	CUDA_ERROR_CHECK(cudaStreamCreate(&_h2d_stream));
	CUDA_ERROR_CHECK(cudaStreamCreate(&_prc_stream));
	CUDA_ERROR_CHECK(cudaStreamCreate(&_d2h_stream));

	conf.print();
	// Instantiate processor objects
	_unpacker = new Unpacker<char2>(_prc_stream,
		_conf.n_samples,
		_conf.n_channel,
		_conf.n_elements,
		_conf.protocol
	);

	_beamformer = new Beamformer<ComputeType>(_prc_stream,
		_conf.n_samples,
		_conf.n_channel,
		_conf.n_elements,
		_conf.n_beam,
		_conf.nfft,
		_conf.os_numerator,
		_conf.os_denominator,
		_conf.integration
	);

	if(_beamformer->check_config()){exit(CONFIG_ERROR);}
	int raw_beams;
	(conf.n_beam > N_MAX_RBEAMS) ? raw_beams = N_MAX_RBEAMS: raw_beams = conf.n_beam;

	_packer_voltage = new Packer<VoltageBeam::type>(
		_d2h_stream,
		_conf.n_samples,
		_conf.n_channel,
		raw_beams,
		"voltage"
	);

	_packer_stokesi = new Packer<StokesIBeam::type>(
		_d2h_stream,
		_conf.n_samples,
		_conf.n_channel,
		_conf.n_beam,
		"stokesi"
	);

	_packer_stokesf = new Packer<StokesFBeam::type>(
		_d2h_stream,
		_conf.n_samples,
		_conf.n_channel,
		_conf.n_beam,
		"stokesf"
	);

	// Calculate buffer sizes
  std::size_t raw_input_size = _conf.n_samples
		* _conf.n_channel
		* _conf.n_elements
		* _conf.n_pol;
  std::size_t transpose_size = _conf.n_samples
		* _conf.n_channel
		* _conf.n_elements
		* _conf.n_pol;
  std::size_t weight_size = _conf.n_beam
		* _conf.n_channel
		* _conf.n_elements
		* _conf.n_pol;
  std::size_t ovoltage_size = raw_beams
		* _conf.n_channel
		* _conf.n_samples
		* _conf.n_pol;
  std::size_t ostokesi_size = _conf.n_beam
		* _conf.n_channel
		* _conf.n_samples
		/ _conf.nfft;
	std::size_t ostokesf_size = conf.n_beam
    * conf.n_samples * (1 - ((float)1/conf.os_numerator) * (conf.os_numerator - conf.os_denominator))
    * conf.n_channel
    / conf.integration;

	std::size_t total_size = (raw_input_size * sizeof(UnpackedInputType::type)
		+ transpose_size * sizeof(InputType::type)
		+ weight_size 	* sizeof(WeightType::type)
		+ ovoltage_size * sizeof(VoltageBeam::type)
		+ ostokesi_size * sizeof(StokesIBeam::type)
		+ ostokesf_size * sizeof(StokesFBeam::type)) * 2; // Multiplied by two, since each buffer is double buffer
  BOOST_LOG_TRIVIAL(debug) << "Required memory for raw input:\t 2x" << std::to_string(raw_input_size * sizeof(UnpackedInputType::type) / (1024)) << "\tkiB";
	BOOST_LOG_TRIVIAL(debug) << "Required memory for unpacked:\t 2x" << std::to_string(transpose_size * sizeof(InputType::type) / (1024)) << "\tkiB";
  BOOST_LOG_TRIVIAL(debug) << "Required memory for weights:\t 2x" << std::to_string(weight_size * sizeof(WeightType::type) / (1024)) << "\tkiB";
  BOOST_LOG_TRIVIAL(debug) << "Required memory for ovoltage:\t 2x" << std::to_string(ovoltage_size * sizeof(VoltageBeam::type) / (1024)) << "\tkiB";
  BOOST_LOG_TRIVIAL(debug) << "Required memory for ostokesi:\t 2x" << std::to_string(ostokesi_size * sizeof(StokesIBeam::type) / (1024)) << "\tkiB";
  BOOST_LOG_TRIVIAL(debug) << "Required memory for ostokesf:\t 2x" << std::to_string(ostokesf_size * sizeof(StokesFBeam::type) / (1024)) << "\tkiB";
	BOOST_LOG_TRIVIAL(info) << "Required total device memory: " << total_size;

	// Instantiate buffers
	_raw_input_buffer = new UnpackedInputType(raw_input_size);//, true);
	_input_buffer = new InputType(transpose_size);
	_weight_buffer = new WeightType(weight_size);
	_ovoltage_buffer = new VoltageBeam(ovoltage_size, true);
	_ostokesi_buffer = new StokesIBeam(ostokesi_size, true);
	_ostokesf_buffer = new StokesFBeam(ostokesf_size, true);

}


template<class HandlerType, class ComputeType>
Pipeline<HandlerType, ComputeType>::~Pipeline()
{

#ifdef DEBUG
	  CUDA_ERROR_CHECK( cudaEventDestroy(start) );
	  CUDA_ERROR_CHECK( cudaEventDestroy(stop) );
#endif
		if(_h2d_stream)
		{
			CUDA_ERROR_CHECK(cudaStreamDestroy(_h2d_stream));
		}
		if(_prc_stream)
		{
			CUDA_ERROR_CHECK(cudaStreamDestroy(_prc_stream));
		}
		if(_d2h_stream)
		{
			CUDA_ERROR_CHECK(cudaStreamDestroy(_d2h_stream));
		}

		if(_raw_input_buffer)
		{
			delete _raw_input_buffer;
		}
		if(_input_buffer)
		{
			delete _input_buffer;
		}
		if(_weight_buffer)
		{
			delete _weight_buffer;
		}
		if(_ovoltage_buffer)
		{
			delete _ovoltage_buffer;
		}
		if(_ostokesi_buffer)
		{
			delete _ostokesi_buffer;
		}
		if(_ostokesf_buffer)
		{
			delete _ostokesf_buffer;
		}
}



template<class HandlerType, class ComputeType>
void Pipeline<HandlerType, ComputeType>::init(psrdada_cpp::RawBytes &header_block)
{
		std::size_t bytes = header_block.total_bytes();
		_handler_vol.init(header_block);
		_handler_sti.init(header_block);
		_handler_stf.init(header_block);
}

template<class HandlerType, class ComputeType>
bool Pipeline<HandlerType, ComputeType>::operator()(psrdada_cpp::RawBytes &dada_input)
{
	_call_cnt += 1;
	BOOST_LOG_TRIVIAL(debug) << "Processing "<< _call_cnt << " dada block..";
	if(dada_input.used_bytes() > _raw_input_buffer->total_bytes())
	{
		BOOST_LOG_TRIVIAL(error) << "Unexpected Buffer Size - Got "
       << dada_input.used_bytes() << " byte, expected "
       << _input_buffer->total_bytes() << " byte)";
		CUDA_ERROR_CHECK(cudaDeviceSynchronize());
		return true;
	}

#ifdef DEBUG
	CUDA_ERROR_CHECK( cudaEventRecord(start,0) );
#endif

	// CUDA_ERROR_CHECK(cudaMemcpy((void*)_raw_input_buffer->host.a_ptr(), (const void*) dada_input.ptr(), dada_input.used_bytes(), cudaMemcpyHostToHost));
	// _raw_input_buffer->async_copy(_h2d_stream, cudaMemcpyHostToDevice);
	CUDA_ERROR_CHECK(cudaMemcpyAsync((void*)_raw_input_buffer->b_ptr(),
		(const void*)dada_input.ptr(),
		dada_input.used_bytes(),
		cudaMemcpyHostToDevice,
		_h2d_stream));
	BOOST_LOG_TRIVIAL(debug) << "DADA size " << dada_input.used_bytes();

	BOOST_LOG_TRIVIAL(debug) << "Copied dada block from host to device";
	if(_call_cnt == 1)
	{
		_raw_input_buffer->swap();
		return false;
	}

	_unpacker->unpack(_raw_input_buffer->a_ptr(), _input_buffer->b_ptr());
	_raw_input_buffer->swap();
	BOOST_LOG_TRIVIAL(debug) << "Unpacked data";

	if(_call_cnt == 2)
	{
		_input_buffer->swap();
		return false;
	}

	_beamformer->process(
		_input_buffer->a_ptr(),
		_weight_buffer->a_ptr(),
		_ovoltage_buffer->b_ptr(),
		_ostokesi_buffer->b_ptr(),
		_ostokesf_buffer->b_ptr());

	_input_buffer->swap();

	BOOST_LOG_TRIVIAL(debug) << "Beamformed data";

	if(_call_cnt == 3)
	{
		_ovoltage_buffer->swap();
		_ostokesi_buffer->swap();
		_ostokesf_buffer->swap();
		return false;
	}
	// _packer_voltage->pack(_ovoltage_buffer->a_ptr(), _ovoltage_buffer->host.a_ptr());
	// _packer_stokesi->pack(_ostokesi_buffer->a_ptr(), _ostokesi_buffer->host.a_ptr());
	// _packer_stokesf->pack(_ostokesf_buffer->a_ptr(), _ostokesf_buffer->host.a_ptr());
	_ovoltage_buffer->async_copy(_d2h_stream);
	_ostokesi_buffer->async_copy(_d2h_stream);
	_ostokesf_buffer->async_copy(_d2h_stream);
	BOOST_LOG_TRIVIAL(debug) << "Copied processed dada block from device to host";

	// Wrap in a RawBytes object here;
	psrdada_cpp::RawBytes dada_ovoltage(static_cast<char*>(
		(void*)_ovoltage_buffer->host.a_ptr()),
		_ovoltage_buffer->total_bytes(),
		_ovoltage_buffer->total_bytes());
	psrdada_cpp::RawBytes dada_ostokesi(static_cast<char*>(
		(void*)_ostokesi_buffer->host.a_ptr()),
		_ostokesi_buffer->total_bytes(),
		_ostokesi_buffer->total_bytes());
	psrdada_cpp::RawBytes dada_ostokesf(static_cast<char*>(
		(void*)_ostokesf_buffer->host.a_ptr()),
		_ostokesf_buffer->total_bytes(),
		_ostokesf_buffer->total_bytes());

	BOOST_LOG_TRIVIAL(debug) << "Swapping double buffers";
	_ovoltage_buffer->swap();
	_ovoltage_buffer->host.swap();
	_ostokesi_buffer->swap();
	_ostokesi_buffer->host.swap();
	_ostokesf_buffer->swap();
	_ostokesf_buffer->host.swap();

	BOOST_LOG_TRIVIAL(debug) << "Writing to output streams";
	_handler_vol(dada_ovoltage);
	_handler_sti(dada_ostokesi);
	_handler_stf(dada_ostokesf);
	BOOST_LOG_TRIVIAL(debug) << "Processed "<< _call_cnt << " dada block..";

#ifdef DEBUG
	CUDA_ERROR_CHECK(cudaEventRecord(stop, 0));
	CUDA_ERROR_CHECK(cudaEventSynchronize(stop));
	CUDA_ERROR_CHECK(cudaEventElapsedTime(&ms, start, stop));
	BOOST_LOG_TRIVIAL(info) << "Took " << ms << " ms to process stream";
#endif
	return false;
}

} // namespace beamforming
} // namespace cryopaf

#endif
