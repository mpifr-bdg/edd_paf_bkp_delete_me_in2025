#ifdef UNPACKER_CUH

namespace cryopaf {
namespace beamforming {

template<typename T>
Unpacker<T>::Unpacker(cudaStream_t& stream,
  std::size_t nsamples,
  std::size_t nchannels,
  std::size_t nelements,
  std::string protocol)
    : _stream(stream), _protocol(protocol)
{
  if(_protocol == "spead")
  {
    grid.x = nsamples / NSAMP_PER_HEAP;
    grid.y = nelements;
    grid.z = nchannels;
    block.x = NTHREADS;
  }
  else if(_protocol == "dummy_input")
  {
    BOOST_LOG_TRIVIAL(warning) << "UnpackerWarning: Skipping unpacking process";
  }
  else
  {
    BOOST_LOG_TRIVIAL(error) << "UnpackerError: Protocol " << _protocol << " not implemented";
    exit(1);
  }
}


template<typename T>
Unpacker<T>::~Unpacker()
{
  BOOST_LOG_TRIVIAL(debug) << "Destroy Unpacker object";
}


template<typename T>
void Unpacker<T>::unpack(const char2* input, char2* output)
{
  BOOST_LOG_TRIVIAL(debug) << "Unpack SPEAD2 data";
  unpack_spead_tfpet_to_fpet<<< grid, block, 0, _stream>>>(input, output);
}



template<typename T>
void Unpacker<T>::print_layout()
{
  std::cout << "Grid.x " << grid.x << std::endl;
  std::cout << "Grid.y " << grid.y << std::endl;
  std::cout << "Grid.z " << grid.z << std::endl;
  std::cout << "block.x " << block.x << std::endl;
  std::cout << "block.z " << block.z << std::endl;
}

} //namespace beamforming
} //namespace cryopaf

#endif
