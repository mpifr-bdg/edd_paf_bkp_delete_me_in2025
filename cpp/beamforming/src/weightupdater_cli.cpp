#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

#include <psrdada_cpp/multilog.hpp>
#include <sw/redis++/redis++.h>
#include <cuda_fp16.h>

#include "beamforming/WeightManager.hpp"

namespace psr = psrdada_cpp;
namespace red = sw::redis;
namespace cbf  = cryopaf::beamforming;

const size_t ERROR_IN_COMMAND_LINE = 1;
const size_t SUCCESS = 0;
// const size_t ERROR_UNHANDLED_EXCEPTION = 2;


template<typename T, typename U>
void launch(cbf::WeightManagerConfig& conf)
{
    psr::MultiLog log("weightupdater");
    try
    {
      std::string url = "tcp://" + conf.host + ":" + std::to_string(conf.port);
      BOOST_LOG_TRIVIAL(info) << "Connecting to " << url;
      red::Redis client(url);
      red::Subscriber subscriber = client.subscriber();
      cbf::WeightManager<T, U> manager(conf, log, subscriber);
      manager.start();
    }
    catch (const red::Error &ex)
    {
      std::cout << "Failed to start WeightManager" << ex.what() << std::endl;
    }
}

int main(int argc, char** argv)
{
    // Variables to store command line options
    cbf::WeightManagerConfig conf;

    // Parse command line
    namespace po = boost::program_options;
    po::options_description desc("Options");
    desc.add_options()
    ("help,h", "Print help messages")
    ("redis_channel", po::value<std::string>(&conf.redis_channel)->default_value("BeamformNode1"), "Subscribe redis channel")
    ("channels", po::value<std::size_t>(&conf.n_channel)->default_value(7), "Number of channels")
    ("elements", po::value<std::size_t>(&conf.n_elements)->default_value(32), "Number of antennas")
    ("beams", po::value<std::size_t>(&conf.n_beam)->default_value(32), "Number of beams")
    ("host", po::value<std::string>(&conf.host)->default_value("pacifix6"), "RedisDB host, only used when interface is set to 'redis'")
    ("port", po::value<std::size_t>(&conf.port)->default_value(6379), "RedisDB port, only used when interface is set to 'redis'")
    ("scale", po::value<int>(&conf.scale)->default_value(1), "If set all weights are scaled down by the provided divider");

    po::variables_map vm;
    try
    {
        po::store(po::parse_command_line(argc, argv, desc), vm);
        if ( vm.count("help")  )
        {
            std::cout << "Cryopaf -- The CryoPAF Controller implementations" << std::endl
            << desc << std::endl;
            return SUCCESS;
        }
        po::notify(vm);
    }
    catch(po::error& e)
    {
        std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
        std::cerr << desc << std::endl;
        return ERROR_IN_COMMAND_LINE;
    }
    launch<char, char>(conf);
    return SUCCESS;
}
