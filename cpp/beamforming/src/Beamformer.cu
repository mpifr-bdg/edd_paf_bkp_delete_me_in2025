#ifdef BEAMFORMER_CUH_

namespace cryopaf{
namespace beamforming{

template<class ComputeType>
Beamformer<ComputeType>::Beamformer(
  cudaStream_t& stream,
  std::size_t sample,
  std::size_t channel,
  std::size_t element,
  std::size_t beam,
  std::size_t nfft,
  std::size_t num,
  std::size_t dnum,
  std::size_t integration)
  : _stream(stream),
    _sample(sample),
    _channel(channel),
    _element(element),
    _beam(beam),
    _nfft(nfft),
    _integration(integration)
{

  block.x = WARP_SIZE;
  block.y = N_WARPS;
	grid.x = ceil(_beam   / (float)TC_M);
	grid.y = ceil(_sample / (float)(N_WARPS * TC_N * _integration /(N_WARPS/2)));
	grid.z = _channel;
  _left = (num - dnum) / 2;
  _right = num - (num - dnum) / 2;
  _stages = log2f(nfft);
  __twid();
  BOOST_LOG_TRIVIAL(debug) << "Instantiated Beamformer object";
}


template<class ComputeType>
Beamformer<ComputeType>::~Beamformer()
{
  BOOST_LOG_TRIVIAL(debug) << "Destroy Beamformer object";
}

template<class ComputeType>
void Beamformer<ComputeType>::process(
    const char2* input,
    const char2* weights,
    int2* ovoltage,
    float* ostokesi,
    float4* ostokesf)
{
  BOOST_LOG_TRIVIAL(debug) << "Kernel call to beamform()";
  beamform<<<grid, block, 0, _stream>>>(
    input, weights, thrust::raw_pointer_cast(_twiddle.data()),
    ovoltage, ostokesi, ostokesf,
    _sample, _element,_beam,
    _integration, _left, _right);
}

template<class ComputeType>
void Beamformer<ComputeType>::print_layout()
{
  std::cout << " Kernel layout: " << std::endl
    << " g.x = " << std::to_string(grid.x) << std::endl
    << " g.y = " << std::to_string(grid.y) << std::endl
    << " g.z = " << std::to_string(grid.z) << std::endl
    << " b.x = " << std::to_string(block.x)<< std::endl
    << " b.y = " << std::to_string(block.y)<< std::endl
    << " b.z = " << std::to_string(block.z)<< std::endl;
}


template<class ComputeType>
int Beamformer<ComputeType>::check_config()
{
  if(_sample%SAMPLES_ITER){
      BOOST_LOG_TRIVIAL(error) << "ERROR: Number of samples must be multiple of 128"; return CONFIG_ERROR;}
  if(_integration%INTEGRATE_ITER){
      BOOST_LOG_TRIVIAL(error) << "ERROR: Integration value must be multiple of 4"; return CONFIG_ERROR;}
  if(_nfft != FFT32_POINTS && _nfft != FFT128_POINTS){
      BOOST_LOG_TRIVIAL(error) << "ERROR: Only 32- and 128-point FFTs are implemented"; return CONFIG_ERROR;}
  if(_integration*_nfft > _sample){
      BOOST_LOG_TRIVIAL(error) << "ERROR: Not enough samples ("<< _sample << ") in batch to integrate "<< _integration<< "x"<< _nfft << " spectra."; return CONFIG_ERROR;}
  // if(_nfft != FFT32_POINTS || _nfft != FFT128_POINTS){
  //     BOOST_LOG_TRIVIAL(error) << "ERROR: Only 32- and 128-point FFTs are implemented"; return CONFIG_ERROR;}
  return 0;
}


template<typename ComputeType>
ComputeType Beamformer<ComputeType>::__get_tw(int i, int n)
{
    return {(real)(cos(2*PI*i/n)), (imag)(sin(-2*PI*i/n))};
}


template<class ComputeType>
void Beamformer<ComputeType>::__twid()
{
  std::size_t op;
  thrust::host_vector<ComputeType> twiddle_h(_stages*WARP_SIZE);
  _twiddle.resize(WARP_SIZE * _stages);
  for(std::size_t i = 0; i < _stages; i++)
  {
    op = 0x01 << i;
    for(std::size_t l = 0; l < WARP_SIZE; l+=op)
    {
      for(std::size_t k = 0; k < op; k++)
      {
          twiddle_h[i * WARP_SIZE + l + k] = __get_tw(k, op*2);
      }
    }
  }
  BOOST_LOG_TRIVIAL(debug) << "1";
  _twiddle = twiddle_h;
}

} // namespace beamforming
} // namespace cryopaf

#endif
