#ifdef PACKER_CUH

namespace cryopaf {
namespace beamforming {

template<typename T>
Packer<T>::Packer(cudaStream_t& stream,
  std::size_t nsamples,
  std::size_t nchannels,
  std::size_t nbeams,
  std::string type)
    : _stream(stream),
    _nsamples(nsamples),
    _nchannels(nchannels),
    _nbeams(nbeams),
    _type(type)
{
  if(_type == "voltage"){
    _batch_size = NSAMP_HEAP_VOLTAGE * sizeof(T);
    _func = &Packer::pack_voltage_beams;
    _heap_group_size = _nchannels * NPOL_SAMP * NSAMP_HEAP_VOLTAGE * sizeof(T);
  }else if(_type == "stokesi"){
    _batch_size = NSAMP_HEAP_STOKESI * sizeof(T);
    _func = &Packer::pack_stokesi_beams;
  }else if(_type == "stokesf"){
    _batch_size = NSAMP_HEAP_STOKESF * sizeof(T);
    _func = &Packer::pack_stokesf_beams;
  }

}


template<typename T>
Packer<T>::~Packer()
{
  BOOST_LOG_TRIVIAL(debug) << "Destroy Packer object";
}


template<typename T>
void Packer<T>::pack(const T* input, T* output)
{
  (*this.*_func)(input, output);
}

template<typename T>
void Packer<T>::pack_voltage_beams(const T* input, T* output)
{
  std::size_t in_idx;
  std::size_t out_idx;
  for(std::size_t b = 0; b < _nbeams; b++)
  {
    for(std::size_t f = 0; f < _nchannels; f++)
    {
      for(std::size_t p = 0; f < NPOL_SAMP; p++)
      {
        for(std::size_t i = 0; i < _nsamples/NSAMP_HEAP_VOLTAGE; i++)
        {
            in_idx = b * _nchannels * NPOL_SAMP * _nsamples + f * NPOL_SAMP * _nsamples + p * _nsamples + i*NSAMP_HEAP_VOLTAGE;
            out_idx = i * _nbeams * _heap_group_size + b * _heap_group_size + f * NPOL_SAMP * NSAMP_HEAP_VOLTAGE + p * NSAMP_HEAP_VOLTAGE;
            CUDA_ERROR_CHECK(cudaMemcpyAsync((void*)&output[out_idx], (const void*)&input[in_idx], _batch_size, cudaMemcpyDeviceToHost, _stream));
        }
      }
    }
  }
}

template<typename T>
void Packer<T>::pack_stokesi_beams(const T* input, T* output)
{
}

template<typename T>
void Packer<T>::pack_stokesf_beams(const T* input, T* output)
{
}

} //namespace beamforming
} //namespace cryopaf

#endif
