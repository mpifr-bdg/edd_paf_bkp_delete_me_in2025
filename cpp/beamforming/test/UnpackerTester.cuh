#ifndef UNPACKERTESTER_CUH
#define UNPACKERTESTER_CUH

#include <gtest/gtest.h>
#include <vector>
#include <thrust/host_vector.h>
#include <random>
#include <cmath>

#include "psrdada_cpp/common.hpp"
#include "psrdada_cpp/cuda_utils.hpp"

#include "beamforming/Unpacker.cuh"

namespace cryopaf {
namespace beamforming {
namespace test {

struct UnpackerTestConfig{
   int device_id;
   std::size_t n_samples;
   std::size_t n_channel;
   std::size_t n_elements;
   std::string protocol;
   const std::size_t n_pol = 2;
   void print()
   {
     std::cout << "Test configuration" << std::endl;
     std::cout << "device_id: " << device_id << std::endl;
     std::cout << "n_samples: " << n_samples << std::endl;
     std::cout << "n_channel: " << n_channel << std::endl;
     std::cout << "n_elements: " << n_elements << std::endl;
     std::cout << "n_pol: " << n_pol << std::endl;
     std::cout << "protocol: " << protocol << std::endl;
   }
};


class UnpackerTester: public ::testing::TestWithParam<UnpackerTestConfig>
{
public:
    UnpackerTester();
    ~UnpackerTester();

    void test();

protected:
    void SetUp() override;
    void TearDown() override;

    void compare(
      thrust::host_vector<char2>& cpu,
      thrust::host_vector<char2>& gpu);

    void cpu_process(
      thrust::host_vector<char2>& input,
      thrust::host_vector<char2>& output);

    void gpu_process(
      thrust::host_vector<char2>& input,
      thrust::host_vector<char2>& output);

private:
  UnpackerTestConfig conf;
  thrust::host_vector<char2> input;
  thrust::host_vector<char2> host_output;
  thrust::host_vector<char2> dev_output;

};

} //namespace test
} //namespace beamforming
} //namespace cryopaf


#endif
