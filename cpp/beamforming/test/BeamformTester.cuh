#ifndef BEAMFORMER_TESTER_H_
#define BEAMFORMER_TESTER_H_

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <cuda_fp16.h>
#include <random>
#include <cmath>
#include <gtest/gtest.h>
#include <fstream>
#include <iostream>

#include "psrdada_cpp/common.hpp"
#include "psrdada_cpp/cuda_utils.hpp"

#include "beamforming/Beamformer.cuh"

namespace cryopaf{
namespace beamforming{
namespace test{

struct BeamformTestConfig{
   int device_id;
   std::size_t n_samples;
   std::size_t n_channel;
   std::size_t n_elements;
   std::size_t n_beam;
   std::size_t nfft;
   std::size_t integration;
   const std::size_t n_pol = N_POL;
   const std::size_t num = 32;
   const std::size_t dnum = 27;
   void print()
   {
     std::cout << "Test configuration" << std::endl;
     std::cout << "device_id: " << device_id << std::endl;
     std::cout << "n_samples: " << n_samples << std::endl;
     std::cout << "n_channel: " << n_channel << std::endl;
     std::cout << "n_elements: " << n_elements << std::endl;
     std::cout << "n_pol: " << n_pol << std::endl;
     std::cout << "n_beam: " << n_beam << std::endl;
     std::cout << "integration: " << integration << std::endl;
   }
};

class BeamformTester : public ::testing::TestWithParam<BeamformTestConfig> {

protected:
  void SetUp() override;
  void TearDown() override;
public:
  BeamformTester();
  ~BeamformTester();


  /**
  * @brief
  *
  * @param
  */
  void reference_test(float tol=0.0001);

  /**
  * @brief
  *
  * @param
  */
  void analytical_test();

  /**
  * @brief
  *
  * @param
  */
  void cpu_beamform(
    thrust::host_vector<char2>& in,
    thrust::host_vector<char2>& weights,
    thrust::host_vector<int2>& ovoltage,
    thrust::host_vector<float>& ostokesi,
    thrust::host_vector<float4>& ostokesf);

  /**
  * @brief
  *
  * @param
  */
  void gpu_beamform(
    thrust::host_vector<char2>& in,
    thrust::host_vector<char2>& weights,
    thrust::host_vector<int2>& ovoltage,
    thrust::host_vector<float>& ostokesi,
    thrust::host_vector<float4>& ostokesf);

  /**
	* @brief
	*
	* @param
	*/
  void compare_voltage(
    const thrust::host_vector<int2> cpu,
    const thrust::host_vector<int2> gpu
  );

  /**
	* @brief
	*
	* @param
	*/
  void compare_stokesi(
    const thrust::host_vector<float> cpu,
    const thrust::host_vector<float> gpu,
    const float tol);

  /**
	* @brief
	*
	* @param
	*/
  void compare_stokesf(
    const thrust::host_vector<float4> cpu,
    const thrust::host_vector<float4> gpu,
    const float tol);


  template<typename T>
  void write_vector2disk(thrust::host_vector<T> vec, std::string fname);


  // thrust::host_vector<float2> convert_vec(thrust::host_vector<__half2> in);
  // thrust::host_vector<float2> convert_vec(thrust::host_vector<int16_t> in);


private:
  BeamformTestConfig conf;
  cudaStream_t _stream;
  std::size_t _left;
  std::size_t _right;
  bool _expected_err = 0;
  thrust::host_vector<char2> input;
  thrust::host_vector<char2> weights;
  thrust::host_vector<int2> cpu_ovol;
  thrust::host_vector<int2> gpu_ovol;
  thrust::host_vector<float4> cpu_ostf;
  thrust::host_vector<float4> gpu_ostf;
  thrust::host_vector<float> cpu_osti;
  thrust::host_vector<float> gpu_osti;

};

} // namespace beamforming
} // namespace cryopaf
} // namespace test


#endif //POWERBEAMFORMER_TESTER_H_
