#include "beamforming/test/UnpackerTester.cuh"

namespace cryopaf {
namespace beamforming {
namespace test {

UnpackerTester::UnpackerTester()
    : ::testing::TestWithParam<UnpackerTestConfig>(),
    conf(GetParam())
{
  CUDA_ERROR_CHECK(cudaSetDevice(conf.device_id));
}
UnpackerTester::~UnpackerTester()
{

}
void UnpackerTester::SetUp()
{
  BOOST_LOG_TRIVIAL(debug) << "UnpackerTester::SetUp()" << std::endl;
  std::size_t n = conf.n_samples * conf.n_channel * conf.n_elements * conf.n_pol;
  std::default_random_engine generator;
  std::uniform_int_distribution<uint64_t> distribution(0,65535);

  input.resize(n);
  host_output.resize(n);
  dev_output.resize(n);
  BOOST_LOG_TRIVIAL(debug) << "Required device memory: " << (n * (sizeof(char2))) / (1024*1024) << "MiB";
  BOOST_LOG_TRIVIAL(debug) << "Required host memory: "   << (n * (2 * sizeof(char2))) / (1024*1024) << "MiB";

  // Generate test samples
  BOOST_LOG_TRIVIAL(debug) << "Generating " << n << " test samples...";
  for (std::size_t i = 0; i < n; i++)
  {
      input[i].x = static_cast<int8_t>(rand() % 0x7f);
      input[i].y = static_cast<int8_t>(rand() % 0x7f);
  }
}
void UnpackerTester::TearDown()
{
}

void UnpackerTester::test()
{
    cpu_process(input, host_output);

    gpu_process(input, dev_output);

    compare(host_output, dev_output);
}

void UnpackerTester::cpu_process(
  thrust::host_vector<char2>& input,
  thrust::host_vector<char2>& output)
{
  BOOST_LOG_TRIVIAL(debug) << "Computing CPU results...";
  int i_et = conf.n_elements * NSAMP_PER_HEAP;
  int i_pet = conf.n_pol * i_et;
  int i_fpet = conf.n_channel * i_pet;
  int o_et = conf.n_elements * conf.n_samples;
  int o_pet = conf.n_pol * o_et;
  int in_idx, out_idx;
  for(std::size_t t1 = 0; t1 < conf.n_samples/NSAMP_PER_HEAP; t1++)
  {
    for (std::size_t f = 0; f < conf.n_channel; f++)
    {
      for(std::size_t p = 0; p < conf.n_pol; p++)
      {
        for(std::size_t a = 0; a < conf.n_elements; a++)
        {
          for(std::size_t t2 = 0; t2 < NSAMP_PER_HEAP; t2++)
          {
            in_idx = t1 * i_fpet + f * i_pet + p * i_et + a * NSAMP_PER_HEAP + t2;
            out_idx = f * o_pet + p * o_et + a * conf.n_samples + t1 * NSAMP_PER_HEAP + t2;
            output[out_idx] = input[in_idx];
          }
        }
      }
    }
  }
}


void UnpackerTester::gpu_process(
  thrust::host_vector<char2>& input,
  thrust::host_vector<char2>& output)
{
    cudaStream_t stream;
    CUDA_ERROR_CHECK(cudaStreamCreate(&stream));
    // Copy host 2 device (input)
    thrust::device_vector<char2> dev_input = input;
    thrust::device_vector<char2> dev_output(output.size());

    BOOST_LOG_TRIVIAL(debug) << "Computing GPU results...";

    Unpacker<char2> unpacker(stream, conf.n_samples, conf.n_channel, conf.n_elements, conf.protocol);
    unpacker.unpack(
        (char2*)thrust::raw_pointer_cast(dev_input.data()),
        (char2*)thrust::raw_pointer_cast(dev_output.data()));

    CUDA_ERROR_CHECK(cudaDeviceSynchronize());
    CUDA_ERROR_CHECK(cudaStreamDestroy(stream));
    // Copy device 2 host (output)
    output = dev_output;
}

void UnpackerTester::compare(
  thrust::host_vector<char2>& cpu,
  thrust::host_vector<char2>& gpu)
{
    BOOST_LOG_TRIVIAL(debug) << "Comparing results...";

    char2 gpu_element;
  	char2 cpu_element;

    ASSERT_TRUE(cpu.size() == gpu.size())
  		<< "Host and device vector size not equal" << std::endl;
    for (std::size_t i = 0; i < cpu.size(); i++)
    {
				gpu_element = gpu[i];
				cpu_element = cpu[i];
        ASSERT_TRUE(cpu_element.x == gpu_element.x && cpu_element.y == gpu_element.y)
          << "Unpacker: CPU and GPU results are unequal for element " << std::to_string(i) << std::endl
          << "  CPU result: " << std::to_string(cpu_element.x) << " + i*" << std::to_string(cpu_element.y) << std::endl
          << "  GPU result: " << std::to_string(gpu_element.x) << " + i*" << std::to_string(gpu_element.y) << std::endl;
    }
}

/**
* Testing with Google Test Framework
*/
TEST_P(UnpackerTester, UnpackerKernel){
  BOOST_LOG_TRIVIAL(info)
    << "\n------------------------------------------------------------------------" << std::endl
    << " Testing unpacker with T=char2, Input format = TFPET; output format = FPET " << std::endl
    << "------------------------------------------------------------------------\n" << std::endl;
  test();
}

INSTANTIATE_TEST_SUITE_P(UnpackerTesterInstantiation, UnpackerTester, ::testing::Values(
	// devie id | samples | channels | elements | protocol
  UnpackerTestConfig{0, NSAMP_PER_HEAP*1, 7, 36, "spead"},
  UnpackerTestConfig{0, NSAMP_PER_HEAP*2, 14, 2, "spead"},
  UnpackerTestConfig{0, NSAMP_PER_HEAP*3, 21, 32, "spead"},
  UnpackerTestConfig{0, NSAMP_PER_HEAP*4, 21, 11, "spead"},
  UnpackerTestConfig{0, NSAMP_PER_HEAP*5, 14, 28, "spead"},
  UnpackerTestConfig{0, NSAMP_PER_HEAP*1, 21, 28, "spead"},
  UnpackerTestConfig{0, NSAMP_PER_HEAP*2, 14, 128, "spead"}
));

} // namespace test
} // namespace beamforming
} // namespace cryopaf
