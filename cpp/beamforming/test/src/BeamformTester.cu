#include "beamforming/test/BeamformTester.cuh"

namespace cryopaf{
namespace beamforming{
namespace test{


BeamformTester::BeamformTester()
  : ::testing::TestWithParam<BeamformTestConfig>(),
  conf(GetParam()), // GetParam() is inherited from base TestWithParam
  _stream(0)
{
  BOOST_LOG_TRIVIAL(debug) << "Creating instance of BeamformTester";
	CUDA_ERROR_CHECK(cudaSetDevice(conf.device_id));
}


BeamformTester::~BeamformTester()
{
  BOOST_LOG_TRIVIAL(debug) << "Destroying instance of BeamformTester";
}


void BeamformTester::SetUp()
{
  BOOST_LOG_TRIVIAL(debug) << "BeamformTester::SetUp()" << std::endl;
	int raw_beams;
	(conf.n_beam > N_MAX_RBEAMS) ? raw_beams = N_MAX_RBEAMS: raw_beams = conf.n_beam;
  // Calulate memory size for input, weights and output
  std::size_t input_size = conf.n_samples
    * conf.n_elements
    * conf.n_channel
    * conf.n_pol;
  std::size_t weight_size =  conf.n_beam
    * conf.n_elements
    * conf.n_channel
    * conf.n_pol;
  std::size_t ovol_size = raw_beams
		* conf.n_channel
		* conf.n_samples
    * conf.n_pol;
  std::size_t ovol_size_intermediate = conf.n_beam // Buffer just used for CPU calculations
		* conf.n_channel
		* conf.n_samples
    * conf.n_pol;
  std::size_t osti_size = conf.n_beam
    * conf.n_samples
    * conf.n_channel
    / conf.nfft;
  std::size_t ostf_size = conf.n_beam
    * conf.n_samples * (1 - ((float)1/conf.num) * (conf.num - conf.dnum))
    * conf.n_channel
    / conf.integration;
  std::size_t required_mem_out = ovol_size * sizeof(int2)
    + osti_size * sizeof(float)
    + ostf_size * sizeof(float4);
  std::size_t required_mem = input_size * sizeof(char2)
    + weight_size * sizeof(char2)
    + required_mem_out;
  BOOST_LOG_TRIVIAL(debug) << "Required memory for input:    " << std::to_string(input_size * sizeof(char2) / (1024)) << "\tkiB";
  BOOST_LOG_TRIVIAL(debug) << "Required memory for weights:  " << std::to_string(weight_size * sizeof(char2) / (1024)) << "\tkiB";
  BOOST_LOG_TRIVIAL(debug) << "Required memory for ovoltage: " << std::to_string(ovol_size * sizeof(int2) / (1024)) << "\tkiB";
  BOOST_LOG_TRIVIAL(debug) << "Required memory for ostokesi: " << std::to_string(osti_size * sizeof(float) / (1024)) << "\tkiB";
  BOOST_LOG_TRIVIAL(debug) << "Required memory for ostokesf: " << std::to_string(ostf_size * sizeof(float4) / (1024)) << "\tkiB";
  BOOST_LOG_TRIVIAL(debug) << "Required device memory:       " << std::to_string(required_mem / (1024*1024)) << "\tMiB";
  BOOST_LOG_TRIVIAL(debug) << "Required host memory:         " << std::to_string((required_mem + required_mem_out) / (1024*1024)) << "\tMiB";

  input.resize(input_size);
  weights.resize(weight_size);
  cpu_ovol.resize(ovol_size_intermediate);
  gpu_ovol.resize(ovol_size);
  cpu_ostf.resize(ostf_size);
  gpu_ostf.resize(ostf_size);
  cpu_osti.resize(osti_size);
  gpu_osti.resize(osti_size);

  _left = (conf.num - conf.dnum) / 2;
  _right = conf.num - (conf.num - conf.dnum) / 2;
}


void BeamformTester::TearDown()
{
  BOOST_LOG_TRIVIAL(debug) << "TearDown()" << std::endl;
}

void BeamformTester::analytical_test()
{
  int et  = conf.n_samples * conf.n_elements;
  int pet = conf.n_pol * et;
  int be  = conf.n_beam * conf.n_elements;
  int pbe = conf.n_pol * be;
  int idx;
  // Generate a rectangle function for each element
  for(std::size_t f = 0; f < conf.n_channel; f++){
    for(std::size_t p = 0; p < conf.n_pol; p++){
      for(std::size_t e = 0; e < conf.n_elements; e++){
        for(std::size_t t = 0; t < conf.n_samples; t++){
          idx = f * pet + p * et + e * conf.n_samples  + t;
          (t > e) ? input[idx] = {127,0}: input[idx] = {0,0};
        }
      }
    }
  }

  // Generate test weights
  for(std::size_t f = 0; f < conf.n_channel; f++){
    for(std::size_t p = 0; p < conf.n_pol; p++){
      for(std::size_t b = 0; b < conf.n_beam; b++){
        for(std::size_t e = 0; e < conf.n_elements; e++){
          idx = f * pbe + p * be + b * conf.n_elements  + e;
          (b == e) ? weights[idx] = {1,0} : weights[idx] = {0,0};
        }
      }
    }
  }
	// launches CUDA kernel
	gpu_beamform(input, weights, gpu_ovol, gpu_osti, gpu_ostf);
  // launches cpu beamforming
  cpu_beamform(input, weights, cpu_ovol, cpu_osti, cpu_ostf);

  thrust::host_vector<int2> gpu_vol = gpu_ovol;
  thrust::host_vector<float> gpu_sti = gpu_osti;
  thrust::host_vector<char2> input_f = input;

  write_vector2disk(input_f, "input.dat");
  write_vector2disk(gpu_vol, "gpu_rawbeams.dat");
  write_vector2disk(cpu_ovol, "cpu_rawbeams.dat");
  write_vector2disk(gpu_sti, "gpu_stibeams.dat");
  write_vector2disk(cpu_osti, "cpu_stibeams.dat");
}

void BeamformTester::reference_test(float tol)
{
  // Generate test samples / normal distributed noise for input signal
	for (std::size_t i = 0; i < input.size(); i++)
  {
      input[i].x = static_cast<int8_t>(rand() % 0x7f); // max calue 127
      input[i].y = static_cast<int8_t>(rand() % 0x7f); // max calue 127
	}
	// Generate test weights
  for (std::size_t i = 0; i < weights.size(); i++)
  {
      weights[i].x = static_cast<int8_t>(rand() % 0x7f); // max calue 127
      weights[i].y = static_cast<int8_t>(rand() % 0x7f); // max calue 127
  }
	// launches CUDA kernel
	gpu_beamform(input, weights, gpu_ovol, gpu_osti, gpu_ostf);

  if(_expected_err == 0)
  {
    // launches cpu beamforming
    cpu_beamform(input, weights, cpu_ovol, cpu_osti, cpu_ostf);
    // compare results of both outputs
    compare_voltage(cpu_ovol, gpu_ovol);
    // compare results of both outputs
    compare_stokesi(cpu_osti, gpu_osti, tol);
    // compare results of both outputs
    compare_stokesf(cpu_ostf, gpu_ostf, tol);
  }
}


void BeamformTester::gpu_beamform(
  thrust::host_vector<char2>& in,
  thrust::host_vector<char2>& weights,
  thrust::host_vector<int2>& ovoltage,
  thrust::host_vector<float>& ostokesi,
  thrust::host_vector<float4>& ostokesf)
{
  BOOST_LOG_TRIVIAL(debug) << "Calculating GPU results... " << std::flush;
  thrust::device_vector<char2> dev_input = in;
  thrust::device_vector<char2> dev_weights = weights;
  thrust::device_vector<int2> dev_ovoltage(ovoltage.size());
  thrust::device_vector<float>  dev_ostokesi(ostokesi.size());
  thrust::device_vector<float4> dev_ostokesf(ostokesf.size());

  cudaStream_t stream;
  CUDA_ERROR_CHECK(cudaStreamCreate(&stream));

  Beamformer<float2> beamformer(
    stream,
    conf.n_samples,
    conf.n_channel,
    conf.n_elements,
    conf.n_beam,
    conf.nfft,
    conf.num,
    conf.dnum,
    conf.integration);
  if(beamformer.check_config()){_expected_err = 1; return;}

  beamformer.process(
    thrust::raw_pointer_cast(dev_input.data()),
    thrust::raw_pointer_cast(dev_weights.data()),
    thrust::raw_pointer_cast(dev_ovoltage.data()),
    thrust::raw_pointer_cast(dev_ostokesi.data()),
    thrust::raw_pointer_cast(dev_ostokesf.data())
  );

  CUDA_ERROR_CHECK(cudaDeviceSynchronize());
  CUDA_ERROR_CHECK(cudaStreamDestroy(stream));
  BOOST_LOG_TRIVIAL(debug) << "Copying GPU results to host... " << std::flush;
  // Copy device 2 host (output)
  ovoltage = dev_ovoltage;
  ostokesi = dev_ostokesi;
  ostokesf = dev_ostokesf;
  BOOST_LOG_TRIVIAL(debug) << "GPU processing done" << std::flush;
}


void BeamformTester::cpu_beamform(
  thrust::host_vector<char2>& in,
  thrust::host_vector<char2>& weights,
  thrust::host_vector<int2>& ovoltage,
  thrust::host_vector<float >& ostokesi,
  thrust::host_vector<float4>& ostokesf)
{
  BOOST_LOG_TRIVIAL(debug) << "Calculating CPU results... " << std::flush;
    // Input index helpers
    int et  = conf.n_samples * conf.n_elements;
    int pet = conf.n_pol * et;
    // Weight index helpers
    int be  = conf.n_beam * conf.n_elements;
    int pbe = conf.n_pol * be;
    // Output stokes I
    int ft  = conf.n_channel * conf.n_samples/conf.nfft;
    int ttt = conf.n_samples * (1 - ((float)1/conf.num) * (conf.num - conf.dnum)) / conf.integration;
    int ftt = conf.n_channel * ttt;
    // Output index helpers
    int tp  = conf.n_samples * conf.n_pol;
    int fpt = conf.n_channel * tp;
    // Actual indeices
  	int ou_idx_vol = 0, ou_idx_full = 0, ou_idx_stokesi = 0, in_idx = 0, wg_idx = 0;
    int2 voltage = {0,0};

    // Do cGEMM
    for(std::size_t b = 0; b < conf.n_beam; b++){
      for(std::size_t f = 0; f < conf.n_channel; f++){
       for(std::size_t t = 0; t < conf.n_samples; t++){
         for(std::size_t p = 0; p < conf.n_pol; p++){
            ou_idx_vol = b * fpt + f * tp + p * conf.n_samples + t;
            voltage = {0,0};
            for(std::size_t e = 0; e < conf.n_elements; e++){
							in_idx = f * pet + p * et + e * conf.n_samples  + t;
							wg_idx = f * pbe + p * be + b * conf.n_elements + e;
							voltage = cmadd(in[in_idx], weights[wg_idx], voltage);
						}
            ovoltage[ou_idx_vol].x = voltage.x / SCALE_FACTOR;
            ovoltage[ou_idx_vol].y = voltage.y / SCALE_FACTOR;
  				}
        }
  		}
  	}
    BOOST_LOG_TRIVIAL(debug) << "CPU beamforming done... " << std::flush;
    BOOST_LOG_TRIVIAL(debug) << "Channelizing, detection and integration" << std::flush;
    // Do FFT
    int ou_idx_x;
    int ou_idx_y;
    thrust::host_vector<float2> ovoltage_fft(ovoltage.size());
    thrust::host_vector<float4> full_stokes(conf.nfft);
    for(std::size_t b = 0; b < conf.n_beam; b++){
      for(std::size_t f = 0; f < conf.n_channel; f++){
        for(std::size_t p = 0; p < conf.n_pol; p++){
          for(std::size_t t = 0; t < conf.n_samples; t+=conf.nfft){
            ou_idx_vol = b * fpt + f * tp + p * conf.n_samples + t;
            dft(&ovoltage[ou_idx_vol], &ovoltage_fft[ou_idx_vol], conf.nfft);
          }
        }
        // Downsample and integrate
        int spec_cnt = 0;
        for(std::size_t t = 0; t < conf.n_samples; t+=conf.nfft){
          if( (t / conf.nfft) % conf.integration == 0){
            ou_idx_full = b * ftt + f * ttt + conf.dnum * spec_cnt;
            spec_cnt++;
          }
          ou_idx_stokesi = b * ft + f * conf.n_samples / conf.nfft + t/conf.nfft;
          for(std::size_t tt = 0; tt < conf.nfft; tt++){
            ou_idx_x = b * fpt + f * tp + t + tt;
            ou_idx_y = b * fpt + f * tp + conf.n_samples + t + tt;
            if(tt > _left && tt < _right){
              full_stokes[tt] = stokes_iquv(ovoltage_fft[ou_idx_x], ovoltage_fft[ou_idx_y]);
              ostokesi[ou_idx_stokesi] += full_stokes[tt].x/conf.nfft;
              ostokesf[ou_idx_full + tt - _left - 1] = add_iquv(ostokesf[ou_idx_full + tt - _left - 1], full_stokes[tt]);
            }
          }
        }
      }
    }

}

void BeamformTester::compare_voltage(
  const thrust::host_vector<int2> cpu,
  const thrust::host_vector<int2> gpu)
{
  BOOST_LOG_TRIVIAL(debug) << "Comparing results ..";
  // Check each element of CPU and GPU implementation
	for(std::size_t i = 0; i < gpu.size(); i++)
	{
    ASSERT_TRUE(cpu[i].x == gpu[i].x || cpu[i].y == gpu[i].y)
        << "Voltage: CPU and GPU result is unequal for element " << std::to_string(i) << std::endl
    // if(!(cpu[i].x == gpu[i].x || cpu[i].y == gpu[i].y))
        // BOOST_LOG_TRIVIAL(debug) << "Beamformer: CPU and GPU result is unequal for element " << std::to_string(i) << std::endl
        << "  CPU result: " << std::to_string(cpu[i].x) << "+ i*" << std::to_string(cpu[i].y) << std::endl
        << "  GPU result: " << std::to_string(gpu[i].x) << "+ i*" << std::to_string(gpu[i].y) << std::endl;
  }
}

void BeamformTester::compare_stokesi(
  const thrust::host_vector<float> cpu,
  const thrust::host_vector<float> gpu,
  const float tol)
{
  // Check if vectors have the same size
  ASSERT_TRUE(cpu.size() == gpu.size())
		<< "Host (" << cpu.size()/(1024*1024) << "MiB) and device ("
    << gpu.size()/(1024*1024) << "MiB) vector sizes not equal" << std::endl;
  BOOST_LOG_TRIVIAL(debug) << "Comparing results ..";
  // Check each element of CPU and GPU implementation
	for(std::size_t i = 0; i < gpu.size(); i++)
	{
    ASSERT_TRUE(cpu[i] <= gpu[i] + gpu[i] * tol && cpu[i] >= gpu[i] - gpu[i] * tol)
        << "Stokes I: CPU and GPU result is unequal for element " << std::to_string(i) << std::endl
    // if(!(cpu[i] <= gpu[i] + gpu[i] * tol && cpu[i] >= gpu[i] - gpu[i] * tol))
        // BOOST_LOG_TRIVIAL(debug) << "Beamformer: CPU and GPU result is unequal for element " << std::to_string(i) << std::endl
        << "  CPU result: " << std::to_string(cpu[i]) << std::endl
        << "  GPU result: " << std::to_string(gpu[i]) << std::endl;
  }
}


void BeamformTester::compare_stokesf(
  const thrust::host_vector<float4> cpu,
  const thrust::host_vector<float4> gpu,
  const float tol)
{
  // Check if vectors have the same size
  ASSERT_TRUE(cpu.size() == gpu.size())
		<< "Host (" << cpu.size()/(1024*1024) << "MiB) and device ("
    << gpu.size()/(1024*1024) << "MiB) vector sizes not equal" << std::endl;

  // Copy CUDA results to host
  for(std::size_t i = 0; i < gpu.size(); i++)
	{
    ASSERT_TRUE((cpu[i].x <= gpu[i].x + gpu[i].x * tol && cpu[i].x >= gpu[i].x - gpu[i].x * tol)
    // if(!((cpu[i].x <= gpu[i].x + gpu[i].x * tol && cpu[i].x >= gpu[i].x - gpu[i].x * tol)
      || (cpu[i].y <= gpu[i].y + gpu[i].y * tol && cpu[i].y >= gpu[i].y - gpu[i].y * tol)
      || (cpu[i].z <= gpu[i].z + gpu[i].z * tol && cpu[i].z >= gpu[i].z - gpu[i].z * tol)
      || (cpu[i].w <= gpu[i].w + gpu[i].w * tol && cpu[i].w >= gpu[i].w - gpu[i].w * tol))
      // BOOST_LOG_TRIVIAL(debug) << "Beamformer: CPU and GPU result is unequal for element " << std::to_string(i) << std::endl
      << "Full Stokes: CPU and GPU result is unequal for element " << std::to_string(i) << std::endl
      << "  CPU result: I=" << std::to_string(cpu[i].x) << " Q=" << std::to_string(cpu[i].y)
        << " U="<< std::to_string(cpu[i].z) << " V=" << std::to_string(cpu[i].w) << std::endl
      << "  GPU result: I=" << std::to_string(gpu[i].x) << " Q=" << std::to_string(gpu[i].y)
        << " U=" << std::to_string(gpu[i].z) << " V=" << std::to_string(gpu[i].w) << std::endl;
  }
}


template<typename T>
void BeamformTester::write_vector2disk(thrust::host_vector<T> vec, std::string fname)
{
  std::ofstream ofile(fname, std::ios::binary);
  ofile.write(
    reinterpret_cast<char*>(thrust::raw_pointer_cast(vec.data())),
    sizeof(T)*vec.size()
  );
  ofile.close();
}

// thrust::host_vector<float2> BeamformTester::convert_vec(thrust::host_vector<__half2> in)
// {
//   thrust::host_vector<float2> out(in.size());
//   for(int i = 0; i < in.size(); i++){
//     out[i] = __half22float2(in[i]);
//   }
//   return out;
// }
//
// thrust::host_vector<float2> BeamformTester::convert_vec(thrust::host_vector<int16_t> in)
// {
//   thrust::host_vector<float2> out(in.size());
//   for(int i = 0; i < in.size(); i++){
//     out[i] = int16_t2float2(in[i]);
//   }
//   return out;
// }

/**
* Testing with Google Test Framework
*/

TEST_P(BeamformTester, BeamformerReferenceTest)
{
  std::cout << std::endl
    << "-----------------------------------------------------" << std::endl
    << " Testing beamforming against reference implementation" << std::endl
    << "-----------------------------------------------------" << std::endl << std::endl;
  reference_test(0.1);
}

// TEST_P(BeamformTester, BeamformerAnalyticalTests)
// {
//   std::cout << std::endl
//     << "--------------------------------------------------" << std::endl
//     << " Testing beamforming with half precision (16bit)  " << std::endl
//     << "--------------------------------------------------" << std::endl << std::endl;
//   analytical_test();
// }

INSTANTIATE_TEST_SUITE_P(BeamformerTesterInstantiation, BeamformTester, ::testing::Values(

	// device ID | samples | channels | elements | beam | nfft | integration
  BeamformTestConfig{0, 128, 1, 128, 128, 32, 4},
  BeamformTestConfig{0, 256, 2, 128, 96, 32, 8},
  BeamformTestConfig{0, 384, 4, 128, 64, 32, 12},
  BeamformTestConfig{0, 512, 8, 128, 32, 32, 16},
  BeamformTestConfig{0, 8192, 16, 128, 128, 32, 32},
  BeamformTestConfig{0, 640, 16, 128, 32, 32, 20},
  BeamformTestConfig{0, 768, 20, 128, 32, 32, 24},
  BeamformTestConfig{0, 896, 24, 128, 32, 32, 28}
));



} // namespace test
} // namespace beamforming
} // namespace cryopaf
