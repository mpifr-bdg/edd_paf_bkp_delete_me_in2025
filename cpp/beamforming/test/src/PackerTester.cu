#include "beamforming/test/PackerTester.cuh"

namespace cryopaf {
namespace beamforming {
namespace test {

PackerTester::PackerTester()
    : ::testing::TestWithParam<PackerTestConfig>(),
    conf(GetParam())
{
  CUDA_ERROR_CHECK(cudaSetDevice(conf.device_id));
}
PackerTester::~PackerTester()
{
}

void PackerTester::SetUp()
{
  BOOST_LOG_TRIVIAL(debug) << "PackerTester::SetUp()" << std::endl;
  std::size_t n = conf.n_samples * conf.n_channel * conf.n_elements * conf.n_pol;
  std::default_random_engine generator;
  std::uniform_int_distribution<uint64_t> distribution(0,65535);

  input.resize(n);
  host_output.resize(n);
  dev_output.resize(n);

  // Generate test samples
  BOOST_LOG_TRIVIAL(debug) << "Generating " << n << " test samples...";
  for (std::size_t i = 0; i < n; i++)
  {
      input[i].x = static_cast<int>(rand() % 0x7f);
      input[i].y = static_cast<int>(rand() % 0x7f);
  }
}
void PackerTester::TearDown()
{
}

void PackerTester::test()
{
    cpu_process(input, host_output);

    gpu_process(input, dev_output);

    compare(host_output, dev_output);
}

void PackerTester::cpu_process(
  thrust::host_vector<char2>& input,
  thrust::host_vector<char2>& output)
{
  BOOST_LOG_TRIVIAL(debug) << "Computing CPU results...";
}


void PackerTester::gpu_process(
  thrust::host_vector<char2>& input,
  thrust::host_vector<char2>& output)
{
}

void PackerTester::compare(
  thrust::host_vector<char2>& cpu,
  thrust::host_vector<char2>& gpu)
{
}

/**
* Testing with Google Test Framework
*/
TEST_P(PackerTester, PackerKernel){
  BOOST_LOG_TRIVIAL(info)
    << "\n------------------------------------------------------------------------" << std::endl
    << " Testing unpacker with T=char2, Input format = TFPET; output format = FPET " << std::endl
    << "------------------------------------------------------------------------\n" << std::endl;
  test();
}

INSTANTIATE_TEST_SUITE_P(PackerTesterInstantiation, PackerTester, ::testing::Values(
	// devie id | samples | channels | elements | protocol
  PackerTestConfig{0, NSAMP_HEAP_VOLTAGE, 14, 28, "voltage"},
  PackerTestConfig{0, NSAMP_HEAP_STOKESI, 21, 28, "stokesi"},
  PackerTestConfig{0, NSAMP_HEAP_STOKESF, 14, 128, "stokesf"}
));

} //namespace test
} // cryopaf
} //namespace beamforming
