#ifdef KERNEL_STATISTICS_CUH

namespace cryopaf {
namespace beamforming {
namespace profiling {


KernelProfiler::KernelProfiler(std::string kernel_name,
  unsigned long long complexity,
  unsigned long long read_size,
  unsigned long long write_size,
  unsigned long long input_size,
  std::size_t device_id)
  : name(kernel_name),
    compute_complexity(complexity),
    reads(read_size),
    writes(write_size),
	  input_sz(input_size),
    dev_id(device_id)
{
  CUDA_ERROR_CHECK(cudaEventCreate(&start));
  CUDA_ERROR_CHECK(cudaEventCreate(&stop));
  CUDA_ERROR_CHECK(cudaGetDeviceProperties(&prop, dev_id));
  peak_mem_bandwidth = prop.memoryClockRate * (prop.memoryBusWidth / 8) / 1e6;
}

KernelProfiler::~KernelProfiler()
{
  CUDA_ERROR_CHECK(cudaEventDestroy(start));
  CUDA_ERROR_CHECK(cudaEventDestroy(stop));
}

void KernelProfiler::measure_start()
{
	CUDA_ERROR_CHECK(cudaEventRecord(start, NULL));
}
void KernelProfiler::measure_stop()
{
	CUDA_ERROR_CHECK(cudaEventRecord(stop, NULL));
	CUDA_ERROR_CHECK(cudaEventSynchronize(stop));
	CUDA_ERROR_CHECK(cudaEventElapsedTime(&ms, start, stop));
  update(ms);
}

void KernelProfiler::update(float time_ms)
{
  elapsed_time.push_back(time_ms);
  compute_tput.push_back(compute_complexity / (time_ms * 1000 * 1000));
  memory_bw.push_back((reads + writes) / (time_ms * 1000 * 1000));
  input_bw.push_back(input_sz / (time_ms * 1000 * 1000));
  output_bw.push_back(writes / (time_ms * 1000 * 1000));
  iterations = elapsed_time.size();
  std::cout << "Took " << ms << " ms to process " << name << std::endl;
  std::cout << "  Throughput: " << compute_tput.back() << " GFLOPS" << std::endl;
  std::cout << "  Bandwidth:  " << memory_bw.back() << " GB/s" << std::endl;
}

void KernelProfiler::finialize()
{
  std::cout  << "Finializing profiling results.." << std::endl;
  if(iterations > 0)
  {
    avg_time = std::accumulate(elapsed_time.begin(), elapsed_time.end(), .0) / iterations;
    max_time = *(std::max_element(elapsed_time.begin(), elapsed_time.end()));
    min_time = *(std::min_element(elapsed_time.begin(), elapsed_time.end()));
    avg_tput = std::accumulate(compute_tput.begin(), compute_tput.end(), .0) / iterations;
    min_tput = *(std::min_element(compute_tput.begin(), compute_tput.end()));
    max_tput = *(std::max_element(compute_tput.begin(), compute_tput.end()));
    avg_m_bw = std::accumulate(memory_bw.begin(), memory_bw.end(), .0) / iterations;
    min_m_bw = *(std::min_element(memory_bw.begin(), memory_bw.end()));
    max_m_bw = *(std::max_element(memory_bw.begin(), memory_bw.end()));
    avg_m_bw_perc = avg_m_bw / peak_mem_bandwidth * 100;
    min_m_bw_perc = min_m_bw / peak_mem_bandwidth * 100;
    max_m_bw_perc = max_m_bw / peak_mem_bandwidth * 100;
    avg_i_bw = std::accumulate(input_bw.begin(), input_bw.end(), .0) / iterations;
    min_i_bw = *(std::min_element(input_bw.begin(), input_bw.end()));
    max_i_bw = *(std::max_element(input_bw.begin(), input_bw.end()));
    avg_o_bw = std::accumulate(output_bw.begin(), output_bw.end(), .0) / iterations;
    min_o_bw = *(std::min_element(output_bw.begin(), output_bw.end()));
    max_o_bw = *(std::max_element(output_bw.begin(), output_bw.end()));
  }
  else
  {
    std::cerr  << "0 iterations, no kernel was profiled.."  << std::endl;
  }
}

void KernelProfiler::export_to_csv(std::string filename)
{
  std::ofstream csv;
  std::ifstream test(filename.c_str());
  std::cout  << "Exporting results to " << filename << std::endl;

  // If file does not exists we add the header line
  if(!test.good())
  {
    std::cout  << "Writing CSV header" << std::endl;
    csv.open(filename.c_str(), std::ios::out);
    csv << "devicename,"
        << "kernelname,"
        << "reads,"
        << "writes,"
        << "avg_time,"
        << "min_time,"
        << "max_time,"
        << "avg_throughput,"
        << "min_throughput,"
        << "max_throughput,"
        << "avg_bandwidth,"
        << "min_bandwidth,"
        << "max_bandwidth,"
        << "percentage_avg_bandwidth,"
        << "percentage_min_bandwidth,"
        << "percentage_max_bandwidth,"
        << "input_avg_bandwidth,"
        << "input_min_bandwidth,"
        << "input_max_bandwidth,"
        << "output_avg_bandwidth,"
        << "output_min_bandwidth,"
        << "output_max_bandwidth\n";
  }
  else
  {
    std::cout  << "Appending data to CSV" << std::endl;
    csv.open(filename.c_str(), std::ios::app);
  }
  csv << prop.name << ","
      << name << ","
      << reads << ","
      << writes << ","
      << avg_time << ","
      << min_time << ","
      << max_time << ","
      << avg_tput << ","
      << min_tput << ","
      << max_tput << ","
      << avg_m_bw << ","
      << min_m_bw << ","
      << max_m_bw << ","
      << avg_m_bw_perc << ","
      << min_m_bw_perc << ","
      << max_m_bw_perc << ","
      << avg_i_bw << ","
      << min_i_bw << ","
      << max_i_bw << ","
      << avg_o_bw << ","
      << min_o_bw << ","
      << max_o_bw << "\n";
  csv.close();
}

} //namespace profiling
} //namespace beamforming
} //namespace cryopaf

#endif
