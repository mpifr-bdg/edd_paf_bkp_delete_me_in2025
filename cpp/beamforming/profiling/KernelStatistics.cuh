#ifndef KERNEL_STATISTICS_CUH
#define KERNEL_STATISTICS_CUH

#include <fstream>
#include <cuda.h>
#include <iostream>
#include <numeric>


namespace cryopaf {
namespace beamforming {
namespace profiling {

class KernelProfiler
{
public:
  KernelProfiler(std::string kernel_name,
    unsigned long long complexity,
    unsigned long long read_size,
    unsigned long long write_size,
    unsigned long long input_size,
    std::size_t device_id=0);
  ~KernelProfiler();
  void measure_start();
  void measure_stop();
  void update(float time_ms);
  void finialize();
  void export_to_csv(std::string filename);
  cudaDeviceProp getDevProp(){return prop;}

private:
  cudaDeviceProp prop;

  std::string name;
  std::string head_line;

  cudaEvent_t start, stop;
  std::vector<float> elapsed_time;
  std::vector<float> compute_tput;
  std::vector<float> memory_bw;
  std::vector<float> input_bw;
  std::vector<float> output_bw;
  float peak_mem_bandwidth;
  float ms = 0;
  float avg_time = 0;
  float min_time = 0;
  float max_time = 0;
  float avg_tput = 0;
  float min_tput = 0;
  float max_tput = 0;
  float avg_m_bw = 0;
  float min_m_bw = 0;
  float max_m_bw = 0;
  float avg_m_bw_perc = 0;
  float min_m_bw_perc = 0;
  float max_m_bw_perc = 0;
  float avg_i_bw = 0;
  float min_i_bw = 0;
  float max_i_bw = 0;
  float avg_o_bw = 0;
  float min_o_bw = 0;
  float max_o_bw = 0;

  std::size_t iterations = 0;
  unsigned long long reads;
  unsigned long long writes;
  unsigned long long input_sz;
  unsigned long long compute_complexity;
  std::size_t dev_id;
};

} //namespace profiling
} //namespace beamforming
} //namespace cryopaf

#include "beamforming/profiling/src/KernelStatistics.cu"

#endif
