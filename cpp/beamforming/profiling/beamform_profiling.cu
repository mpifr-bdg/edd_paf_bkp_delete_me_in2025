#include <thrust/device_vector.h>
#include <thrust/copy.h>
#include <thrust/complex.h>
#include <cuda.h>
#include <random>
#include <cmath>
#include <fstream>
#include <chrono>

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

#include "psrdada_cpp/cuda_utils.hpp"
#include "psrdada_cpp/multilog.hpp"

#include "beamforming/Unpacker.cuh"
#include "beamforming/Beamformer.cuh"
#include "beamforming/profiling/KernelStatistics.cuh"

const size_t ERROR_IN_COMMAND_LINE = 1;
const size_t SUCCESS = 0;

using namespace psrdada_cpp;
using namespace cryopaf::beamforming;
using namespace cryopaf::beamforming::profiling;
using namespace std::chrono;

struct ProfileConfig{
   int device_id;
   std::size_t n_samples;
   std::size_t n_channel;
   std::size_t n_elements;
   std::size_t n_beam;
   std::size_t integration;
   std::size_t nfft;
   std::string precision;
   std::string protocol;
   std::string out_dir;
   const std::size_t num = 32;
   const std::size_t dnum = 27;
   const std::size_t n_pol = 2;

   void print()
   {
     std::cout << "n_samples  : " << n_samples << std::endl;
     std::cout << "n_channel  : " << n_channel << std::endl;
     std::cout << "n_elements : " << n_elements << std::endl;
     std::cout << "n_beam     : " << n_beam << std::endl;
     std::cout << "integration: " << integration << std::endl;
     std::cout << "nfft       : " << nfft << std::endl;
     std::cout << "num        : " << num << std::endl;
     std::cout << "dnum       : " << dnum << std::endl;
   }
 };


template<typename T>
void profile(ProfileConfig conf, std::size_t iter)
{
  // If template parameter is not of a complex dtype profiling has to be aborted
  if(!(std::is_same<T, float2>::value))
  //&& !(std::is_same<T,  float2>::value)
  {
    BOOST_LOG_TRIVIAL(error) << "ProfilingError: Template type not supported";
    exit(1);
  }
  cudaStream_t stream;
  CUDA_ERROR_CHECK(cudaStreamCreate(&stream));
  // Instantiate processor objects
  Beamformer<T> beamformer(stream,
    conf.n_samples,
    conf.n_channel,
    conf.n_elements,
    conf.n_beam,
    conf.nfft,
    conf.num,
    conf.dnum,
    conf.integration);

	int raw_beams;
	(conf.n_beam > N_MAX_RBEAMS) ? raw_beams = N_MAX_RBEAMS: raw_beams = conf.n_beam;
  // Calulate memory size for input, weights and output
  std::size_t input_size = conf.n_samples
    * conf.n_elements
    * conf.n_channel
    * conf.n_pol;
  std::size_t weight_size = conf.n_beam
    * conf.n_elements
    * conf.n_channel
    * conf.n_pol;
  std::size_t ovoltage_size = raw_beams
		* conf.n_channel
		* conf.n_samples
    * conf.n_pol;
  std::size_t ostokesi_size = conf.n_beam
		* conf.n_channel
		* conf.n_samples
		/ conf.nfft;
	std::size_t ostokesf_size = conf.n_beam
    * conf.n_samples * (1 - ((float)1/conf.num) * (conf.num - conf.dnum))
    * conf.n_channel
    / conf.integration;;
  std::size_t required_mem = input_size * sizeof(char2)
    + weight_size * sizeof(char2)
    + ovoltage_size * sizeof(int2)
    + ostokesi_size * sizeof(float)
    + ostokesf_size * sizeof(float4);
  BOOST_LOG_TRIVIAL(debug) << "Required memory for input:    " << std::to_string(input_size * sizeof(char2) / (1024)) << "\tkiB";
  BOOST_LOG_TRIVIAL(debug) << "Required memory for weights:  " << std::to_string(weight_size * sizeof(char2) / (1024)) << "\tkiB";
  BOOST_LOG_TRIVIAL(debug) << "Required memory for ovoltage: " << std::to_string(ovoltage_size * sizeof(int2) / (1024)) << "\tkiB";
  BOOST_LOG_TRIVIAL(debug) << "Required memory for ostokesi: " << std::to_string(ostokesi_size * sizeof(float) / (1024)) << "\tkiB";
  BOOST_LOG_TRIVIAL(debug) << "Required memory for ostokesf: " << std::to_string(ostokesf_size * sizeof(float4) / (1024)) << "\tkiB";
  BOOST_LOG_TRIVIAL(debug) << "Required device memory:       " << std::to_string(required_mem / (1024*1024)) << "\tMiB";

  // Allocate device memory
  thrust::device_vector<char2> input_bf(input_size);
  thrust::device_vector<char2> weights_bf(weight_size);
  thrust::device_vector<int2> output_bf_voltage(ovoltage_size);
  thrust::device_vector<float> output_bf_stokesi(ostokesi_size);
  thrust::device_vector<float4> output_bf_stokesf(ostokesf_size);

  // Calculation of compute complexity
  unsigned long long n = conf.n_beam * conf.n_pol
    * conf.n_channel
    * conf.n_elements
    * conf.n_samples * conf.n_pol ;
  unsigned long long complexity_bf = 7 * n;

  KernelProfiler beamform_profiler("Beamformer",
    complexity_bf,
    (input_bf.size() + weights_bf.size()) * sizeof(char2),
    output_bf_voltage.size() * sizeof(int2),
    // + output_bf_stokesi.size() * sizeof(float)
    // + output_bf_stokesf.size() * sizeof(float4),
    input_bf.size() * sizeof(char2));

  // conf.print();
  // Run all used kernels i-times
  for(std::size_t i = 0; i < iter; i++)
  {
    // Call to beamformer kernel
    beamform_profiler.measure_start();
  	beamformer.process(
      thrust::raw_pointer_cast(input_bf.data()),
      thrust::raw_pointer_cast(weights_bf.data()),
      thrust::raw_pointer_cast(output_bf_voltage.data()),
      thrust::raw_pointer_cast(output_bf_stokesi.data()),
      thrust::raw_pointer_cast(output_bf_stokesf.data())
    );
    beamform_profiler.measure_stop();
  }
  CUDA_ERROR_CHECK(cudaStreamDestroy(stream));

  beamform_profiler.finialize();
  beamform_profiler.export_to_csv(conf.out_dir + "beamform_kernel.csv");
}


int main(int argc, char** argv)
{
  // Variables to store command line options
  ProfileConfig conf;
  int iter;
  std::string precision;
  std::string filename;

  // Parse command line
  namespace po = boost::program_options;
  po::options_description desc("Options");
  desc.add_options()

  ("help,h", "Print help messages")
  ("samples,s", po::value<std::size_t>(&conf.n_samples)->default_value(1024), "Number of samples within one batch")
  ("channels,c", po::value<std::size_t>(&conf.n_channel)->default_value(16), "Number of channels")
  ("elements,e", po::value<std::size_t>(&conf.n_elements)->default_value(128), "Number of elements")
  ("beams,b", po::value<std::size_t>(&conf.n_beam)->default_value(128), "Number of beams")
  ("integration,i", po::value<std::size_t>(&conf.integration)->default_value(4), "Integration interval; must be multiple 2^n and smaller 32")
  ("device,d", po::value<int>(&conf.device_id)->default_value(0), "ID of GPU device")
  ("protocol", po::value<std::string>(&conf.protocol)->default_value("spead"), "Protocol of input data; supported protocol 'codif'")
  ("nfft", po::value<std::size_t>(&conf.nfft)->default_value(32), "Protocol of input data; supported protocol 'codif'")
  ("runs,r", po::value<int>(&iter)->default_value(5), "Iterations to run")
  ("precision", po::value<std::string>(&conf.precision)->default_value("half"), "Compute type of GEMM operation; supported precisions 'half' and 'single'")
  ("outdir", po::value<std::string>(&conf.out_dir)->default_value("Results/"), "Output directory to store csv files");

  po::variables_map vm;
  try
  {
      po::store(po::parse_command_line(argc, argv, desc), vm);
      if ( vm.count("help")  )
      {
          std::cout << "Beamform Profiling" << std::endl
          << desc << std::endl;
          return SUCCESS;
      }
      po::notify(vm);
  }
  catch(po::error& e)
  {
      std::cerr << "ERROR: " << e.what() << std::endl;
      std::cerr << desc << std::endl;
      return ERROR_IN_COMMAND_LINE;
  }

  profile<float2>(conf, iter);

  return 0;
}
