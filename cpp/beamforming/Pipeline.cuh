/*
* Pipeline.cuh
* Author: Niclas Esser <nesser@mpifr-bonn.mpg.de>
* Description:
*  This files consists of a single class (Pipeline<HandlerType, ComputeType, ResultType>)
*  and a configuration structure (PipelineConfig).
*  An object of Pipeline is used to access data from psrdada buffers, unpacks them,
*  performs beamforming and writes the results back to another psrdada buffer.
*  TODO:
*    - We need a packer
*    - We need a monitoring interface
*/
#ifndef PIPELINE_CUH_
#define PIPELINE_CUH_


#define DEBUG 1

#include <thrust/device_vector.h>
#include <thrust/copy.h>
#include <cuda.h>

#include <psrdada_cpp/cuda_utils.hpp>
#include <psrdada_cpp/multilog.hpp>
#include <psrdada_cpp/raw_bytes.hpp>
#include <psrdada_cpp/double_device_buffer.cuh>
#include <psrdada_cpp/double_host_buffer.cuh>


#include "beamforming/Unpacker.cuh"
#include "beamforming/Packer.cuh"
#include "beamforming/Beamformer.cuh"
#include "beamforming/BufferTypes.cuh"
#include "beamforming/details/utils.cu"

namespace cryopaf{
namespace beamforming{

struct PipelineConfig{
   key_t in_key;
   key_t ovol_key;
   key_t osti_key;
   key_t ostf_key;
   int device_id;
   std::string logname;
   std::size_t n_samples;
   std::size_t n_channel;
   std::size_t n_elements;
   std::size_t n_beam;
   std::size_t integration;
   std::size_t nfft;
   std::string protocol;
   const std::size_t os_numerator = 32;
   const std::size_t os_denominator = 27;
   const std::size_t n_pol = 2;
   void print()
   {
     BOOST_LOG_TRIVIAL(debug) << "Pipeline configuration";
     BOOST_LOG_TRIVIAL(debug) << "in_key: " << in_key;
     BOOST_LOG_TRIVIAL(debug) << "ovol_key: " << ovol_key ;
     BOOST_LOG_TRIVIAL(debug) << "osti_key: " << osti_key ;
     BOOST_LOG_TRIVIAL(debug) << "ostf_key: " << ostf_key ;
     BOOST_LOG_TRIVIAL(debug) << "device_id: " << device_id;
     BOOST_LOG_TRIVIAL(debug) << "logname: " << logname;
     BOOST_LOG_TRIVIAL(debug) << "n_samples: " << n_samples;
     BOOST_LOG_TRIVIAL(debug) << "n_channel: " << n_channel;
     BOOST_LOG_TRIVIAL(debug) << "n_elements: " << n_elements;
     BOOST_LOG_TRIVIAL(debug) << "n_pol: " << n_pol;
     BOOST_LOG_TRIVIAL(debug) << "n_beam: " << n_beam;
     BOOST_LOG_TRIVIAL(debug) << "integration: " << integration;
   }
};


template<class HandlerType, class ComputeType>
class Pipeline{
// Internal type defintions
private:
  typedef PipelineBuffer<char2> UnpackedInputType;    // Type for received raw input data (voltage)
  typedef PipelineBuffer<char2> InputType;    // Type for unpacked raw input data (voltage)
  typedef PipelineBuffer<int2> VoltageBeam;// Type for beamfored output data
  typedef PipelineBuffer<float> StokesIBeam;// Type for beamfored output data
  typedef PipelineBuffer<float4> StokesFBeam;// Type for beamfored output data
  typedef SharedPipelineBuffer<char2> WeightType;  // Type for beam weights
public:


	/**
	* @brief	Constructs an object of Pipeline
	*
  * @param	PipelineConfig conf  Pipeline configuration containing all necessary parameters (declaration can be found in Types.cuh)
  * @param	MultiLog log         Logging instance
	* @param	HandlerType	handler  Object for handling output data
	*
	* @detail	Initializes the pipeline enviroment including device memory and processor objects.
	*/
	Pipeline(PipelineConfig& conf,
    psrdada_cpp::MultiLog &log,
    HandlerType &handler_vol,
    HandlerType &handler_sti,
    HandlerType &handler_stf);


	/**
	* @brief	Deconstructs an object of Pipeline
  *
	* @detail	Destroys all objects and allocated memory
	*/
	~Pipeline();


  /**
   * @brief      Initialise the pipeline with a DADA header block
   *
   * @param      header  A RawBytes object wrapping the DADA header block
   */
	void init(psrdada_cpp::RawBytes &header_block);


  /**
   * @brief      Process the data in a DADA data buffer
   *
   * @param      data  A RawBytes object wrapping the DADA data block
   */
	bool operator()(psrdada_cpp::RawBytes &dada_block);
// Internal attributes
private:

  HandlerType &_handler_vol;
  HandlerType &_handler_sti;
	HandlerType &_handler_stf;
	psrdada_cpp::MultiLog &_log;
	PipelineConfig& _conf;
  // Processors
  Unpacker<char2>* _unpacker = nullptr; // Object to unpack and transpose received input data on GPU to an expected format
  Beamformer<ComputeType>* _beamformer = nullptr; // Object to perform beamforming on GPU
  Packer<VoltageBeam::type>* _packer_voltage = nullptr; // Object to perform beamforming on GPU
  Packer<StokesIBeam::type>* _packer_stokesi = nullptr; // Object to perform beamforming on GPU
	Packer<StokesFBeam::type>* _packer_stokesf = nullptr; // Object to perform beamforming on GPU
  // Buffers
	UnpackedInputType *_raw_input_buffer = nullptr; // Received input buffer
	InputType *_input_buffer = nullptr;  // Unpacked and transposed input buffer
	WeightType *_weight_buffer = nullptr; // Beam weights, updated through shared memory
  VoltageBeam *_ovoltage_buffer = nullptr; // Output buffer containing processed beams
  StokesIBeam *_ostokesi_buffer = nullptr; // Output buffer containing processed beams
  StokesFBeam *_ostokesf_buffer = nullptr; // Output buffer containing processed beams

  std::size_t _call_cnt = 0; // Internal dada block counter

	cudaStream_t _h2d_stream;  // Host to device cuda stream (used for async copys)
	cudaStream_t _prc_stream;  // Processing stream
	cudaStream_t _d2h_stream;  // Device to host cuda stream (used for async copys)
#ifdef DEBUG
  // Time measurement variables (debugging only)
	cudaEvent_t start, stop;
	float ms;
#endif
};


} // namespace beamforming
} // namespace cryopaf

#include "src/Pipeline.cu"

#endif /* POWERBEAMFORMER_CUH_ */
