#ifndef UNPACKER_CUH
#define UNPACKER_CUH

#include <cuda.h>
#include <cuda_fp16.h>
#include <thrust/device_vector.h>

#include <psrdada_cpp/common.hpp>
#include <psrdada_cpp/multilog.hpp>


#define NPOL_SAMP             2
#define NSAMP_PER_HEAP        4096
#define NTHREADS              1024

namespace cryopaf {
namespace beamforming {

__global__
void unpack_spead_tfpet_to_fpet(__restrict__ const char2* idata, __restrict__ char2* odata);


template<typename T>
class Unpacker
{
public:

    Unpacker(cudaStream_t& stream,
      std::size_t nsamples,
      std::size_t nchannels,
      std::size_t nelements,
      std::string protocol);
    ~Unpacker();
    Unpacker(Unpacker const&) = delete;

    void unpack(const char2* input, char2* output);

    void print_layout();

    int sample_size(){return _sample_size;}
private:
    cudaStream_t& _stream;
    int _sample_size = 8; // TODO: dynamic for spead and fixed for codif
    std::string _protocol;
    dim3 grid;
    dim3 block;
};

} //namespace beamforming
} //namespace cryopaf

#include "beamforming/details/UnpackerKernels.cu"
#include "beamforming/src/Unpacker.cu"

#endif // UNPACKER_CUH
