/*
* BufferTypes.cuh
* Author: Niclas Esser <nesser@mpifr-bonn.mpg.de>
* Description:
*   This file contains classes for different kinds of buffer representation.
*   All implemented classes inherit from DoubleBuffer<thrust::device_vector<T>>.
*/

#ifndef BUFFERTYPES_HPP_
#define BUFFERTYPES_HPP_

// boost::interprocess used to upload weights via POSIX shared memory
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/thread.hpp>

#include <psrdada_cpp/cuda_utils.hpp>
#include <psrdada_cpp/double_device_buffer.cuh>
#include <psrdada_cpp/double_host_buffer.cuh>

#include "beamforming/QueueHeader.hpp"

namespace cryopaf{
namespace beamforming {


/**
* @brief  Class providing buffers for beam data (is always the Output of the Pipeline)
* @detail An object of PipelineBuffer also contains an instance of DoublePinnedHostBuffer<T>
*         to allow an asynchronous copy to the host memory.
*/
template<class T>
class PipelineBuffer : public psrdada_cpp::DoubleBuffer<thrust::device_vector<T>>
{

public:
    typedef T type;
    psrdada_cpp::DoublePinnedHostBuffer<T> host;
public:
    /**
    * @brief	Instantiates an object of PipelineBuffer
    *
    * @param	std::size_t  Number of items in buffer
    *
    * @detail Allocates twice the size in device memory and in host memory as double buffers
    */
    PipelineBuffer(std::size_t size, bool host_mirror=false)
      : psrdada_cpp::DoubleBuffer<thrust::device_vector<T>>(),
      _mirror(host_mirror)
    {
      this->resize(size);
      if(_mirror)
      {
        host.resize(size);
      }
      _bytes = size * sizeof(T);
    }
    /**
    * @brief	Destroys an object of PipelineBuffer
    */
    ~PipelineBuffer(){}

    /**
    * @brief	Asynchronous copy to host memory
    *
    * @param	cudaStream_t& stream  Device to host stream
    */
    void async_copy(cudaStream_t& stream, cudaMemcpyKind kind = cudaMemcpyDeviceToHost)
    {
      if(!_mirror){
        BOOST_LOG_TRIVIAL(warning) << "No mirrored buffer resides on host side, doing nothing"; return;
      }else{
        CUDA_ERROR_CHECK(cudaMemcpyAsync(host.a_ptr(), this->a_ptr(), _bytes, kind, stream));
      }
    }
    /**
    * @brief	Returns the number of bytes used for a single buffer
    *
    * @detail The occupied memory is twice
    */
    std::size_t total_bytes(){return _bytes;}
private:
    std::size_t _bytes;
    bool _mirror;
};

// Define namespace for convinient access to boost::interprocess functionalitys, just used for weights
namespace bip = boost::interprocess;

/**
* @brief  Class providing buffers for beam weights
* @detail An object of SharedPipelineBuffer has the ability to read out a POSIX shared memory namespaces.
* @note   The current state is not final and will change in future. The idea for future is
*         to provide an update method which is called by a shared memory instance.
*/
template<class T>
class SharedPipelineBuffer : public psrdada_cpp::DoubleBuffer<thrust::device_vector<T>>
{
public:
    typedef T type;
public:

    /**
    * @brief	Instantiates an object of PipelineBuffer
    *
    * @param	std::size_t  Number of items in buffer
    * @param	std::string  Name of the POSIX shared memory
    *
    * @detail Allocates twice the size in device memory as double device buffer.
    *         It also launches a boost::thread to create, read and write from shared
    *         memory.
    */
    SharedPipelineBuffer(std::size_t size, std::string smem_name="SharedMemoryWeights")
      : psrdada_cpp::DoubleBuffer<thrust::device_vector<T>>()
      , _smem_name(smem_name)
    {

      this->resize(size);
      _bytes = size * sizeof(T);
      t = new boost::thread(boost::bind(&SharedPipelineBuffer::run, this));
    }
    /**
    * @brief	Destroys an object of PipelineBuffer
    */
    ~SharedPipelineBuffer(){}

    /**
    * @brief	Creates, read, write and removes a POSIX shared memory space
    *
    * @detail This function is a temporary solution to update beam weights on-the-fly
    *         while the pipeline is operating. In the future a clean interface will be created
    *         that provides addtional monitoring informations (e.g. power level) besides the
    *         beam weight updating mechanism.
    */
    void run()
    {

      bip::shared_memory_object::remove("SharedMemoryWeights");
      bip::shared_memory_object smem(bip::create_only, "SharedMemoryWeights", bip::read_write);

      // Set size of shared memory including QueueHeader + payload
      BOOST_LOG_TRIVIAL(info) << "Size of shared memory for weight uploading (IPC) " << sizeof(QueueHeader) + (this->size()) * sizeof(T);
      smem.truncate(sizeof(QueueHeader) + (this->size()) * sizeof(T));

      // Map shared memory to a addressable region
      bip::mapped_region region(smem, bip::read_write);

      void* smem_addr = region.get_address(); // get it's address

      QueueHeader* qheader = static_cast<QueueHeader*>(smem_addr);  // Interpret first bytes as QueueHeader
      T *ptr = &(static_cast<T*>(smem_addr)[sizeof(QueueHeader)]); // Pointer to address of payload (behind QueueHeader)
      qheader->stop = true;
      while(qheader->stop){usleep(1000);}
      while(!qheader->stop)
      {
        bip::scoped_lock<bip::interprocess_mutex> lock(qheader->mutex);
        if(!qheader->data_in)
        {
          BOOST_LOG_TRIVIAL(debug) << "Waiting for writing weights to shared memory";
          qheader->ready_to_read.wait(lock); // Wait for read out
        }

        BOOST_LOG_TRIVIAL(debug) << "Reading new weights from shared memory";
        CUDA_ERROR_CHECK(cudaMemcpy((void*)this->b_ptr(), (void*)ptr,
            _bytes, cudaMemcpyHostToDevice));
        // Swap double buffer, so next batch is calculated with new weights
        this->swap();
        //Notify the other process that the buffer is empty
        qheader->data_in = false;
        qheader->ready_to_write.notify_all();

      }
      bip::shared_memory_object::remove("SharedMemoryWeights");
      BOOST_LOG_TRIVIAL(info) << "Closed shared memory for weights uploading";
    }

    std::size_t total_bytes(){return _bytes;}

private:
    std::size_t _bytes;
    std::string _smem_name;
    boost::thread *t;
};

} // namespace beamforming
} // namespace cryopaf



#endif /* BUFFERTYPES_HPP_ */
