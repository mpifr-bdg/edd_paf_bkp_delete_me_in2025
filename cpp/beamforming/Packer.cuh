#ifndef PACKER_CUH
#define PACKER_CUH

#include <cuda.h>
#include <cuda_fp16.h>
#include <thrust/device_vector.h>

#include <psrdada_cpp/common.hpp>
#include <psrdada_cpp/multilog.hpp>


#define NPOL_SAMP             2
#define NSAMP_HEAP_VOLTAGE    1024
#define NSAMP_HEAP_STOKESI    1024
#define NSAMP_HEAP_STOKESF    1024
#define NTHREADS              1024

namespace cryopaf {
namespace beamforming {

template<typename T>
class Packer
{
public:

    Packer(cudaStream_t& stream,
      std::size_t nsamples,
      std::size_t nchannels,
      std::size_t nbeams,
      std::string type);
    ~Packer();
    Packer(Packer const&) = delete;
    void pack(const T* input, T* output);
private:
    void pack_voltage_beams(const T* input, T* output);
    void pack_stokesi_beams(const T* input, T* output);
    void pack_stokesf_beams(const T* input, T* output);
private:
    cudaStream_t& _stream;
    std::string _type;
    std::size_t _nsamples;
    std::size_t _nchannels;
    std::size_t _nbeams;
    std::size_t _heap_group_size;
    std::size_t _batch_size;
    void (Packer::*_func)(const T*, T*) = NULL;
};

} //namespace beamforming
} //namespace cryopaf

#include "beamforming/src/Packer.cu"

#endif // PACKER_CUH
