data_formats = {
    "CRYO_PAF_CHANNELIZER:1" : {
        "ip":"",
        "port": 17100,
        "buffer_size":128000000,
        "packet_size": 8400,
        "nheaps":512,
        "heap_size" : 8192,
        "heap_id_start" : 0,
        "heap_id_step" : 4,
        "heap_id_offset" : 0,
        "sample_clock" : 2e8,
        "sample_clock_start" : 0,
        "udp" : True,
        "nbit" : 2*8,
        "npol":2,
        "dim":2,
        "spead_desc": {
            "item1": {
                "id":0x1600,
                "name":"timestamp",
                "description":"timestamp of the oldest sample in this packet",
                "shape":[6],
                "dtype":">u1",
                "order":"C",
                "step":4096,
                "index": 1
            },
            "item2": {
                "id":0x1601,
                "name":"clipping_cnt",
                "description":"Counts channelwise clipped data points",
                "shape":[6],
                "dtype":">u1",
                "value":10,
                "order":"C"
            },
            "item3": {
                "id":0x1602,
                "name":"error_vector",
                "description":"Contains an error code",
                "shape":[6],
                "dtype":">u1",
                "value":3,
                "order":"C"
            },
            "item4": {
                "id":0x1603,
                "name":"scale_factor",
                "description":"Contains the scaling factor",
                "shape":[6],
                "dtype":">u1",
                "value":1,
                "order":"C"
            },
            "item5": {
                "id":0x1604,
                "name":"order_vector",
                "description":"channel ID",
                "shape":[6],
                "dtype":">u1",
                "order":"C",
                "value": 0,
                # "list":[0,262144,65536], # [start, step, exclude last, number of bytes]
                "list":[0x00000000,0x00100000,0x00010000],
                "mask":0x0000ffff0000,
                "index":2
            },
            "item6": {
                "id":0x1604,
                "name":"order_vector",
                "description":"element ID",
                "shape":[6],
                "dtype":">u1",
                "order":"C",
                "value": 0,
                "list":[0,0x00016,0x0001], # [start, step, exclude last, number of bytes]
                "mask":0x00000000ffff,
                "index":3
            },
            "item7": {
                "id":0x1605,
                "name":"reserved",
                "description":"Reserved item",
                "shape":[6],
                "dtype":">u1",
                "value":0,
                "order":"C"
            },
            "item8": {
                "id":0x1606,
                "name":"payload",
                "description":"channelized voltages of one PAF element",
                "shape":[4096,2],
                "dtype":"int8",
                "order":"C"
            }
        }
    },
    "CRYO_PAF_CBF_VOLTAGE_BEAM:1": {
        "ip":"",
        "port": "",
        "packet_size": 8400,
        "heap_size" : 8192,
        "heap_id_step": 8*16*2, # Number of streams * number of channels per stream
        "heap_offset": 1,
        "heap_group" : 8*16*2,
        "nsci":0,
        "nbit" : 32*2,
        "nhops": 16,
        "udp": True,
        "port":17001,
        # "heap_size": 8192 + 12 * 8,
        "data_rate" : 2e9,
        "spead_desc": {
            "item1": {
                "tag":1,
                "id":0x1600,
                "name":"timestamp",
                "description":"timestamp of the oldest sample in this packet",
                "shape":[6],
                "dtype":">u1",
                "order":"C",
                "step":1024,
                "index": 1
            },
            "item2": {
                "tag":2,
                "id":0x1602,
                "name":"channel_id",
                "description":"Contains element ID, channel ID and hardware ID",
                "shape":[6],
                "dtype":">u1",
                "order":"C",
                "value": 0,
                "list":[0,2,1],
                "index": 2
            },
            "item3": {
                "tag":3,
                "id":0x1603,
                "name":"polarisation",
                "description":"Contains element ID, channel ID and hardware ID",
                "shape":[6],
                "dtype":">u1",
                "order":"C",
                "value": 0,
                "list":[0,16,1],
                "index": 3
            },
            "item4": {
                "tag":4,
                "id":0x1604,
                "name":"beam_id",
                "description":"Contains element ID, channel ID and hardware ID",
                "shape":[6],
                "dtype":">u1",
                "order":"C",
                "value": 0,
                "list":[0,16,1],
                "index": 4
            },
            "item5": {
                "tag":5,
                "id":0x1605,
                "name":"payload",
                "description":"channelized voltage beam",
                "shape":[1024,2],
                "dtype":"int32",
                "order":"C"
            }
        }
    },
    "CBF_STOKES_I_BEAM:1": {
        "ip":"",
        "port": "",
        "packet_size": 8400,
        "heap_size" : 8192,
        "heap_id_step": 8*16*2, # Number of streams * number of channels per stream
        "heap_offset": 1,
        "heap_group" : 8*16*2,
        "nsci":0,
        "nbit" : 32,
        "udp": True,
        "port":17002,

        # "heap_size": 8192 + 12 * 8,
        "data_rate" : 2e9,
        "spead_desc": {
            "item1": {
                "tag":1,
                "id":0x1600,
                "name":"timestamp",
                "description":"timestamp of the oldest sample in this packet",
                "shape":[6],
                "dtype":">u1",
                "order":"C",
                "step":4096,
                "index": 1
            },
            "item2": {
                "tag":2,
                "id":0x1602,
                "name":"order_vector",
                "description":"Contains element ID, channel ID and hardware ID",
                "shape":[6],
                "dtype":">u1",
                "order":"C",
                "value": 0,
                "list":[0,2,1],
                "index": 2
            },
            "item3": {
                "tag":3,
                "id":0x1605,
                "name":"payload",
                "description":"channelized voltage beam",
                "shape":[512,4],
                "dtype":"float",
                "order":"C"
            }
        }
    },
    "CBF_FULL_STOKES_BEAM:1": {
        "ip":"",
        "port": "",
        "packet_size": 8400,
        "heap_id_step": 8*16*2, # Number of streams * number of channels per stream
        "heap_offset": 1,
        "heap_group" : 8*16*2,
        "nsci":0,
        "heap_size" : 6912,
        "nbit" : 32*4,
        "udp": True,
        "port":17003,
        # "heap_size": 8192 + 12 * 8,
        "data_rate" : 2e9,
        "spead_desc": {
            "item1": {
                "tag":1,
                "id":0x1600,
                "name":"timestamp",
                "description":"timestamp of the oldest sample in this packet",
                "shape":[6],
                "dtype":">u1",
                "order":"C",
                "step":4096,
                "index": 1
            },
            "item2": {
                "tag":2,
                "id":0x1602,
                "name":"order_vector",
                "description":"Contains element ID, channel ID and hardware ID",
                "shape":[6],
                "dtype":">u1",
                "order":"C",
                "value": 0,
                "list":[0,2,1],
                "index": 2
            },
            "item3": {
                "tag":3,
                "id":0x1603,
                "name":"payload",
                "description":"channelized voltage beam",
                "shape":[2048],
                "dtype":"int32",
                "order":"C"
            }
        }
    },
    "COMPLEX_GAIN_SOLUTIONS:1": {
        "ip":"",
        "port": "",
        "packet_size": 8400,
        "heap_id_step": 8*16*2, # Number of streams * number of channels per stream
        "heap_offset": 1,
        "heap_group" : 8*16*2,
        "nsci":0,
        "heap_size" : 6912,
        "nbit" : 32*4,
        "udp": True,
        "port":17003,
        # "heap_size": 8192 + 12 * 8,
        "data_rate" : 2e9,
        "spead_desc": {
            "item1": {
                "tag":1,
                "id":0x1600,
                "name":"timestamp",
                "description":"timestamp of the oldest sample in this packet",
                "shape":[6],
                "dtype":">u1",
                "order":"C",
                "step":4096,
                "index": 1
            }
        }
    },
    "RFI_SOLUTIONS:1": {
        "ip":"",
        "port": "",
        "packet_size": 8400,
        "heap_id_step": 8*16*2, # Number of streams * number of channels per stream
        "heap_offset": 1,
        "heap_group" : 8*16*2,
        "nsci":0,
        "heap_size" : 6912,
        "nbit" : 32*4,
        "udp": True,
        "port":17003,
        # "heap_size": 8192 + 12 * 8,
        "data_rate" : 2e9,
        "spead_desc": {
            "item1": {
                "tag":1,
                "id":0x1600,
                "name":"timestamp",
                "description":"timestamp of the oldest sample in this packet",
                "shape":[6],
                "dtype":">u1",
                "order":"C",
                "step":4096,
                "index": 1
            }
        }
    }

}
