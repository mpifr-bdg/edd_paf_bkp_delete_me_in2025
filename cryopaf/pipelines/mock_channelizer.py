from mpikat.utils.process_tools import ManagedProcess, command_watcher
from mpikat.utils.process_monitor import SubprocessMonitor
from mpikat.utils.sensor_watchdog import SensorWatchdog
from mpikat.utils.db_monitor import DbMonitor
from mpikat.utils.ip_utils import split_ipstring
from mpikat.utils.mkrecv_stdout_parser import MkrecvSensors
from mpikat.core.edd_pipeline import EDDPipeline, launchPipelineServer, updateConfig, state_change
from mpikat.core.datastore import EDDDataStore
import mpikat.utils.numa as numa
from mpikat.utils.core_manager import CoreManager
from mpikat.utils.ip_utils import ipstring_to_list

from cryopaf.utils.mkheader import MkHeader
from cryopaf.utils.key_gen import DadaKeyGen
from cryopaf.conf.spead_conf import data_formats
from cryopaf.conf.constants import *

import re
import os
import logging, time
import numpy as np
import json
import tornado
from copy import deepcopy
from tempfile import NamedTemporaryFile
from tornado.gen import coroutine
from katcp import Sensor, AsyncReply, FailReply
from collections import OrderedDict

log = logging.getLogger("mpikat.cryopaf.pipelines.MockChannelizer")

# CONSTANTS
DBDISK_CLI="/homes/nesser/SoftwareDev/Projects/EDD/psrdada_cpp/build/psrdada_cpp/diskdb"
DBSPLIT_CLI="/homes/nesser/SoftwareDev/Projects/EDD/psrdada_cpp/build/psrdada_cpp/dbsplit"
DADA_SLOTS = 16
TESTDATADIR = "/tmp/dada_test/"

DEFAULT_CONFIG = {
    "id": "MockChannelizer",
    "type": "MockChannelizer",
    "host" : "pacifix3",
    "port" : 7148,
    "dataset_dir" :"",
    "repeats":1024,
    "subband_id":0,
    "n_elements":N_PAF_ELEMENTS,
    "channels_per_mcgroup":CHANNELS_PER_SUBBAND,
    "heaps_per_buffer":128,
    "samples_per_heap":4096,
    "dataset_dir": "/tmp/dada_test/",
    "source" : "dataset",
    "mode":"unset",
    "output_data_streams": [],
    "data_store" :
    {
        "host" : "pacifix6",
        "port" : 6379
    }
}

class MockChannelizer(EDDPipeline):

    def __init__(self, ip, port):
        EDDPipeline.__init__(self, ip, port, DEFAULT_CONFIG)
        self._output_handler = []
        self.__eddDataStore = 0
        self._connect_sync = 0
        self._generator = 0
        self._dada_keys = []
        self._dada_buffers = []
        self._dada_headers = []
        self._obuffer_sizes = []
        self._output_data_rates = []


    @coroutine
    def setup_sensors(self):
        """
        @brief Setup monitoring sensors
        """
        EDDPipeline.setup_sensors(self)

    @state_change(target="streaming", allowed=["idle", "configured"], intermediate="configuring")
    @coroutine
    def configure(self, config_json={}):
        self._set_numa_node()
        
        self._subprocessMonitor = SubprocessMonitor()
        self.__coreManager = CoreManager(self.numa_node)
        # Setup output data streams
        self._reset_output_streams()
        self._populate_output_streams()
        # Setup dada keys
        keygen = DadaKeyGen()
        self._source_key = keygen.getFreeDadaKey()
        self._dada_keys = keygen.getFreeDadaKeys(len(self._config["output_data_streams"]), mode="ascending")
        log.info("DADA keys: " + str(self._dada_keys) + self._source_key)
        # Create directory for datasets
        if self._config["source"] == "dataset":
            if not os.path.exists(self._config["dataset_dir"]):
                os.mkdir(self._config["dataset_dir"])

        ## Create mksend and dada ringbuffer instances for handling output data stream
        for i, stream in enumerate(self._config["output_data_streams"]):
            # Set "ip" according to subband
            stream["ip"] = "226.0.0." + str(self._config["subband_id"])
            self._spead_description = deepcopy(data_formats[stream["format"]])
            # Create ringbuffers
            self._output_data_rates.append(data_formats[stream["format"]]["sample_clock"] / N_PFB_CHANNELS * CHANNELS_PER_SUBBAND)
            self._obuffer_sizes.append(self._config["heaps_per_buffer"] * data_formats[stream["format"]]["heap_size"] * CHANNELS_PER_SUBBAND)
            yield self._create_ring_buffer(self._obuffer_sizes[i], self._dada_keys[i], self.numa_node)
            # Create mksend instances
            self.__coreManager.add_task("mksend_{}".format(i), 1)
            cmd = self._get_output_handle_cmd(stream["format"], stream["ip"], self._dada_keys[i], i)
            log.info("Output handle command: {}".format(cmd))
            self._output_handler.append(ManagedProcess(cmd))
            self._subprocessMonitor.add(self._output_handler[i], self._subprocess_error)
            self._subprocesses.append(self._output_handler[i])
        # For synchronisation we need a further process and buffer that connects the mksend-ringbuffers
        # with a parent ringbuffer. The parent ringbuffer gets filled by generators.
        # Create buffer
        buffer_size = np.asarray(self._obuffer_sizes).sum()
        yield self._create_ring_buffer(buffer_size, self._source_key, self.numa_node)
        # Make process for splitting the parent ringbuffer into mksend-ringbuffers
        self.__coreManager.add_task("splitter", 1)
        cmd = self._get_connect_and_sync_cmd()
        log.info("Connecting output streams with source, using command: {}".format(cmd))
        self._connect_sync = ManagedProcess(cmd)
        self._subprocessMonitor.add(self._connect_sync, self._subprocess_error)
        self._subprocesses.append(self._connect_sync)

        self.__coreManager.add_task("generator", 1)
        cmd = self._get_generate_cmd()
        log.info("Generator command: {}".format(cmd))
        self._generator = ManagedProcess(cmd)
        self._subprocessMonitor.add(self._generator, self._subprocess_error)
        self._subprocesses.append(self._generator)

        log.info("Processes, buffers and generators ready, starting to stream..")


    @state_change(target="streaming", allowed=["streaming"], intermediate="capture_starting")
    @coroutine
    def capture_start(self):
        pass

    @state_change(target="streaming", allowed=["streaming"], intermediate="measurement_starting")
    @coroutine
    def measurement_start(self):
        pass

    @state_change(target="streaming", allowed=["streaming"], intermediate="measurement_stopping")
    @coroutine
    def measurement_stop(self):
        pass

    @state_change(target="streaming", allowed=["streaming"], intermediate="capture_stopping")
    @coroutine
    def capture_stop(self):
        pass

    @state_change(target="idle", intermediate="deconfiguring", error='panic')
    @coroutine
    def deconfigure(self):
        log.info("Deconfiguring EDD backend")
        # Kill processes in reverse order -> writer then reader
        for proc in self._subprocesses[::-1]:
            yield proc.terminate()

        for buffer in self._dada_buffers:
            self._destroy_ring_buffer(buffer)
        self._subprocesses = []
        self._output_handler = []


        if os.path.exists(self._config["dataset_dir"]):
            log.debug("Removing dataset dir {}".format(self._config["dataset_dir"]))
            import shutil
            shutil.rmtree(self._config["dataset_dir"])

    @coroutine
    def _create_ring_buffer(self, buffer_size, key, numa_node, n_reader=1):
         """
         @brief Create a ring buffer of given size with given key on specified numa node.
                Adds and register an appropriate sensor to thw list
         """
         # always clear buffer first. Allow fail here
         yield command_watcher("dada_db -d -k {key}".format(key=key), allow_fail=True)

         cmd = "numactl --cpubind={numa_node} --membind={numa_node} dada_db -k {key} -n {blocks} -b {buffer_size} -p -l -r {n_reader}" \
            .format(key=key, blocks=DADA_SLOTS, buffer_size=buffer_size, numa_node=numa_node, n_reader=n_reader)

         log.info("Creating ringbuffer: {}".format(cmd))
         yield command_watcher(cmd, allow_fail=True)

         M = DbMonitor(key, self._db_monitor_handle)
         M.start()
         self._dada_buffers.append({'key': key, 'monitor': M})

    @coroutine
    def _destroy_ring_buffer(self, buffer):
         """
         @brief Create a ring buffer of given size with given key on specified numa node.
                Adds and register an appropriate sensor to thw list
         """
         # always clear buffer first. Allow fail here
         buffer['monitor'].stop()
         cmd = "dada_db -d -k {0}".format(buffer['key'])
         log.debug("Running command: {0}".format(cmd))
         yield command_watcher(cmd)

    def _get_generate_cmd(self, duration=60):
        cmd = ""

        if self._config["source"] == "dummy":
            log.warning("Generating dummy input!")
            cmd = "dada_junkdb -c 1"
            cmd += " -R " +  str(self._spead_description["sample_clock"])
            cmd += " -t " + str(duration)
            cmd += " -k " + str(self._source_key)
            cmd += " " + str(self._dada_headers[0])

        elif self._config["source"] == "dataset":
            self._create_test_datasets()
            cmd = DBDISK_CLI
            # cmd = "diskdb "
            cmd += " -f " + str(self._dada_headers[0])
            cmd += " -d " + str(self._config["dataset_file"])
            cmd += " -r " + str(self._config["repeats"])
            cmd += " -k " + str(self._source_key)
            cmd += " -s " + str(np.asarray(self._output_data_rates).sum())

        else:
            log.error("Stream source {} not implemented".format(source))
            raise FailReply("Stream source {} not implemented".format(source))
        return cmd

    def _get_output_handle_cmd(self, format, ip, key, id):
        ## Set dynamic values for mksend header
        mksend_format = deepcopy(data_formats[format])
        mksend_format["dada_key"] = key
        mksend_format["nslots"] = DADA_SLOTS
        mksend_format["heap_id_start"] = id
        mksend_format["heap_id_step"] = len(self._config["output_data_streams"])
        mksend_format["nhops"] = 4
        mksend_format["file_size"] = self._obuffer_sizes[id]
        mksend_format["sample_clock"] = self._output_data_rates[id]
        if "udp" in mksend_format:
            mksend_format['udp_if'] = self.nic_params['ip']
        else:
            mksend_format['ibv_if'] = self.nic_params['ip']
        mksend_format["spead_desc"]["item5"]["list"][0] += id
        mksend_format["spead_desc"]["item5"]["list"][1] += id

        ## Make mksend header
        hdr = MkHeader(NamedTemporaryFile(delete=False).name, 'mksend', **mksend_format)
        hdr.create()
        hdr.add_items(mksend_format["spead_desc"], ignore="item6")
        self._dada_headers.append(hdr.name())
        ## Make mksend command
        if self._config["mode"] == "test":
            try:
                os.mkdir(self._config["dataset_dir"] + str(id))
            except:
                log.warning("Directory {} already exists".format(self._config["dataset_dir"] + str(id)))
            cmd = "dada_dbdisk -k " + str(key) + " -D " + self._config["dataset_dir"] + str(id)
        else:
            cmd = "taskset -c " + str(self.__coreManager.get_coresstr("mksend_" + str(id)))
            cmd += " mksend "
            cmd += " --dada-key " +str(key)
            cmd += " --header " +str(hdr.name())
            cmd += " " + str(ip)
        return cmd

    def _get_connect_and_sync_cmd(self, heap_size=8192):
        cmd = "taskset -c " + str(self.__coreManager.get_coresstr("splitter"))
        cmd += " " + DBSPLIT_CLI
        # cmd += " dbsplit "
        cmd += " --in_key " + str(self._source_key)
        cmd += " --out_keys " + re.sub("[\['\] ]","",str(self._dada_keys))
        cmd += " --chunk " + str(heap_size)
        return cmd

    def _db_monitor_handle(self, key):
        pass

    def _set_numa_node(self):
        # Get NUMA node and its charachteristics
        self.__numa_node_pool = []
        for node in numa.getInfo():
            fastest_nic, nic_params = numa.getFastestNic(node,False)
            # remove numa nodes with missing capabilities
            if nic_params['speed'] < 40000:
                log.debug("NIC speed to low of numa node {} - removing from pool.".format(node))
                continue
            else:
                self.__numa_node_pool.append(node)

        log.debug("NUMA nodes {} remain in pool after constraints.".format(len(self.__numa_node_pool)))

        if len(self.__numa_node_pool) == 0:
            raise FailReply("No viable nodes found to process data!")
        self.numa_node = self.__numa_node_pool[0]

        log.debug("Associating with numa node {}".format(self.numa_node))

        self.fastest_nic, self.nic_params = numa.getFastestNic(self.numa_node,True)
        log.info("Will use NIC {} [ {} ] @ {} Mbit/s" \
            .format( \
                self.fastest_nic, \
                self.nic_params["ip"], \
                self.nic_params['speed']))

    def _create_test_datasets(self):
        n_samples = self._config["heaps_per_buffer"] \
            * self._config["channels_per_mcgroup"] \
            * len(self._config["output_data_streams"]) \
            * self._config["samples_per_heap"] * 2
        input_data = ((np.random.rand(n_samples) - 0.5)*64) \
            .reshape(self._config["heaps_per_buffer"],
                self._config["channels_per_mcgroup"],
                len(self._config["output_data_streams"]),
                self._config["samples_per_heap"], 2) \
            .astype("int8")
        self._config["dataset_file"] = self._config["dataset_dir"] + "stream_{}.dat".format(self._config["subband_id"])
        input_data.tofile(self._config["dataset_file"])
        log.info("Created data set {}".format(self._config["dataset_file"]))


    def _populate_output_streams(self):
        for __ in range(self._config["n_elements"]):
            self._config["output_data_streams"].append({
            "format" : "CRYO_PAF_CHANNELIZER:1",
            "ip": "226.0.0.0",
            "port" : 17100,
            "bit_depth": data_formats["CRYO_PAF_CHANNELIZER:1"]["nbit"]
        })

    def _reset_output_streams(self):
        self._config["output_data_streams"] = []

    def register(self, host=None, port=None):
        """
        Registers the pipeline in the data store.

        Args:
            host, port: Ip and port of the data store.

        If no host and port ar eprovided, values from the internal config are used.
        """
        EDDPipeline.register(self, host, port)
        self._config["data_store"]["ip"] = host
        self._config["data_store"]["port"] = port
        try:
            self.__eddDataStore = EDDDataStore.EDDDataStore(host, port)
        except:
            log.error("Failed to connect to EDDDataStore")

if __name__ == "__main__":
    launchPipelineServer(MockChannelizer)
