from mpikat.utils.process_tools import ManagedProcess, command_watcher
from mpikat.utils.process_monitor import SubprocessMonitor
from mpikat.utils.sensor_watchdog import SensorWatchdog
from mpikat.core.edd_pipeline import EDDPipeline, launchPipelineServer, updateConfig, state_change
from mpikat.core.datastore import EDDDataStore
import mpikat.utils.numa as numa
from mpikat.utils.core_manager import CoreManager
from mpikat.utils.spead_capture import SpeadCapture

from cryopaf.conf.spead_conf import data_formats
from cryopaf.utils.mkheader import MkHeader
from cryopaf.utils.key_gen import DadaKeyGen
from cryopaf.utils.beam_weights import BeamWeightFile
from cryopaf.utils.spead_handlers import RFISpeadHandler, ComplexGainSpeadHandler

import os
import logging
import redis
from tornado.gen import coroutine
from katcp import Sensor, FailReply

log = logging.getLogger("mpikat.cryopaf.pipelines.WeightGeneratorPipeline")


DEFAULT_CONFIG = {
    "id": "WeightGeneratorPipeline",
    "type": "WeightGeneratorPipeline",
    "host" : "pacifix3",
    "port" : 7148,
    "dataset_dir" :"",
    "channels_total":1024,
    "channels_per_mcgroup":16,
    "testvector_id":"weng",
    "mode" : "bypass",
    "n_beams":16,
    "bf_configs":[],
    "output_data_streams": [],
    "input_data_streams":
    [],
    #     "cgs_solutions":
    #     {
    #         "format":"CRYO_PAF_COMPLEX_GAIN_SOLUTIONS:1",
    #         "ip":"227.0.0.0",
    #         "port":17100,
    #         "bit_depth":data_formats["CRYO_PAF_COMPLEX_GAIN_SOLUTIONS:1"]
    #     },
    #     "CRYO_PAF_RFI_SOLUTIONS":
    #     {
    #         "format":"CRYO_PAF_RFI_SOLUTIONS:1",
    #         "ip":"227.0.0.0",
    #         "port":17100,
    #         "bit_depth":data_formats["CRYO_PAF_RFI_SOLUTIONS:1"]
    #     }
    # ],
    "data_store" :
    {
        "host" : "pacifix6",
        "port" : 6379
    }
}

class WeightGeneratorPipeline(EDDPipeline):

    def __init__(self, ip, port):
        EDDPipeline.__init__(self, ip, port, DEFAULT_CONFIG)
        self.__eddDataStore = 0
        self._beamformer_configs = []
        self._package_cnt = 0

    @coroutine
    def setup_sensors(self):
        """
        @brief Setup monitoring sensors
        """
        EDDPipeline.setup_sensors(self)

    @state_change(target="configured", allowed=["idle", "configured"], intermediate="configuring")
    @coroutine
    def configure(self, config_json={}):
        log.info("Configuring WeightGeneratorPipeline")
        log.debug("Configruation string: {}".format(config_json))

        log.debug("Collecting ID of beamformer pipelines")
        # try:
        #     self._pipeline_sensors = redis.StrictRedis(5, self._config["data_store"]["ip"], self._config["data_store"]["port"])
        # except Exception as e:
        #     log.error("Could not connect to Redis, failed with {}".format(e))
        #     raise FailReply("Could not connect to Redis, failed with {}".format(e))
        #
        # for product in self.__eddDataStore.products:
        #     if not "beamform" in product["id"]:
        #         continue
        #     try:
        #         self._config["bf_configs"].append(self._pipeline_sensors[product["id"]]["current-config"])
        #     except Exception as e:
        #         log.error("Could not retrieve pipeline config of {}, failed with {}".format(product["id"], e))
        #         raise FailReply("Could not retrieve pipeline config of {}, failed with {}".format(product["id"], e))


    @state_change(target="ready", allowed=["configured"], intermediate="capture_starting")
    @coroutine
    def capture_start(self):
        pass
        # self.capture_cgs_solutions = SpeadCapture(self._config["input_data_streams"]["cgs_solutions"], ComplexGainSpeadHandler(),
        #     self._cgs_package_writer)
        # self.capture_rfi_solutions = SpeadCapture(self._config["input_data_streams"]["CRYO_PAF_RFI_SOLUTIONS"], RFISpeadHandler(),
        #     self._rfi_package_writer)
        # self.capture_cgs_solutions.start()
        # self.capture_rfi_solutions.start()


    @state_change(target="set", allowed=["ready"], intermediate="measurement_preparing")
    @coroutine
    def measurement_prepare(self, config_json={}):
        pass

    @state_change(target="streaming", allowed=["set", "ready"], intermediate="measurement_starting")
    @coroutine
    def measurement_start(self):
        pass

    @state_change(target="ready", allowed=["streaming"], intermediate="measurement_stopping")
    @coroutine
    def measurement_stop(self):
        pass

    @state_change(target="configured", allowed=["ready", "set"], intermediate="capture_stopping")
    @coroutine
    def capture_stop(self):
        self._cgs_package_cnt = 0
        self._rfi_package_cnt = 0
        # self.capture_cgs_solutions.stop()
        # self.capture_rfi_solutions.stop()

    @state_change(target="idle", intermediate="deconfiguring", error='panic')
    @coroutine
    def deconfigure(self):
        log.info("Deconfiguring EDD backend")

    def _cgs_package_writer(self, package):
        self._cgs_package_cnt += 1

    def _rfi_package_writer(self, package):
        self._rfi_package_cnt += 1

    def _get_telescope_meta(self):
        """
        @brief Get the current telescope meta data from Redis
        """
        if self.__eddDataStore != 0:
            self.source_name = self.__eddDataStore.getTelescopeDataItem("source-name").replace("_", "-")
            self.ra = (self.__eddDataStore.getTelescopeDataItem("ra")).replace("_", "-")
            self.dec = (self.__eddDataStore.getTelescopeDataItem("dec")).replace("_", "-")
            self.scannum = self.__eddDataStore.getTelescopeDataItem("scannum").replace("_", "-")
            self.receiver = self.__eddDataStore.getTelescopeDataItem("receiver").replace("_", "-")
            log.debug("Retrieved data from telescope:\n\
                Source name:        {}\n\
                Right Ascension:    {}\n\
                Declination:        {}\n\
                Scan ID:            {}"\
                .format(self.source_name, self.ra, self.dec, self.scannum))
        else:
            log.error("Not connected to Datastore (Redis), can not retrieve meta data")
            raise FailReply("Not connected to Datastore (Redis), can not retrieve meta data")

    def _get_beamweights(self):
        fname = ""
        if self._config["mode"] == "bypass":
            self.weight_file = BeamWeightFile(mode="w", sbid=self.scannum, n_beams=self._config["n_beams"])
            self.weight_file.create()
            self.weight_file.bypass()
        elif self._config["mode"] == "null":
            self.weight_file = BeamWeightFile(mode="w", sbid=self.scannum, n_beams=self._config["n_beams"])
            self.weight_file.create()
        elif self._config["mode"] == "tdm":
            self.weight_file = BeamWeightFile(mode="w", sbid=self.scannum, n_beams=self._config["n_beams"])
            self.weight_file.create()
            self.get_weights_from_tdm()
        elif self._config["mode"] == "interpolate":
            weight_files = self.get_weight_files()
            self.weight_file = BeamWeightFile()
        else:
            log.error("Mode {} not implemented, can not retrieve beam weights".format(self._config["mode"]))
            raise FailReply("Mode {} not implemented, can not retrieve beam weights".format(self._config["mode"]))

    def get_beam_weights_from_tdm(self):
        pass

    def register(self, host=None, port=None):
        """
        Registers the pipeline in the data store.

        Args:
            host, port: Ip and port of the data store.

        If no host and port ar eprovided, values from the internal config are used.
        """
        EDDPipeline.register(self, host, port)
        self._config["data_store"]["ip"] = host
        self._config["data_store"]["port"] = port
        try:
            self.__eddDataStore = EDDDataStore.EDDDataStore(host, port)
        except:
            log.error("Failed to connect to EDDDataStore")

if __name__ == "__main__":
    launchPipelineServer(WeightGeneratorPipeline)
