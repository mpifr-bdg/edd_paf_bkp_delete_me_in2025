from mpikat.utils.process_tools import ManagedProcess, command_watcher
from mpikat.utils.process_monitor import SubprocessMonitor
from mpikat.utils.sensor_watchdog import SensorWatchdog
from mpikat.utils.db_monitor import DbMonitor
from mpikat.utils.ip_utils import split_ipstring
from mpikat.utils.mkrecv_stdout_parser import MkrecvSensors
from mpikat.core.edd_pipeline import EDDPipeline, launchPipelineServer, updateConfig, state_change
from mpikat.core.datastore import EDDDataStore
import mpikat.utils.numa as numa
from mpikat.utils.core_manager import CoreManager
from mpikat.utils.ip_utils import ipstring_to_list

from cryopaf.utils.mkheader import MkHeader
from cryopaf.conf.spead_conf import data_formats
from cryopaf.utils.key_gen import DadaKeyGen

import re
import os
import logging, time
import numpy as np
import json
import tornado
from copy import deepcopy
from tempfile import NamedTemporaryFile
from tornado.gen import coroutine
from katcp import Sensor, AsyncReply, FailReply
from collections import OrderedDict

log = logging.getLogger("mpikat.cryopaf.pipelines.ComplexGainSolverPipeline")


DEFAULT_CONFIG = {
    "id": "ComplexGainSolverPipeline",
    "type": "ComplexGainSolverPipeline",
    "host" : "pacifix3",
    "port" : 7148,
    "dataset_dir" :"",
    "subband_id":0,
    "channels_total":1024,
    "channels_per_mcgroup":16,
    "input_data_streams": [],
    "data_store" :
    {
        "host" : "pacifix6",
        "port" : 6379
    }
}


class ComplexGainSolverPipeline(EDDPipeline):

    def __init__(self, ip, port):
        EDDPipeline.__init__(self, ip, port, DEFAULT_CONFIG)
        self.__eddDataStore = 0

    @coroutine
    def setup_sensors(self):
        """
        @brief Setup monitoring sensors
        """
        EDDPipeline.setup_sensors(self)

    @state_change(target="configured", allowed=["idle", "configured"], intermediate="configuring")
    @coroutine
    def configure(self, config_json={}):
        pass


    @state_change(target="ready", allowed=["configured"], intermediate="capture_starting")
    @coroutine
    def capture_start(self):
        pass

    @state_change(target="set", allowed=["ready"], intermediate="measurement_preparing")
    @coroutine
    def measurement_prepare(self, config_json={}):
        pass

    @state_change(target="streaming", allowed=["set", "ready"], intermediate="measurement_starting")
    @coroutine
    def measurement_start(self):
        pass

    @state_change(target="ready", allowed=["streaming"], intermediate="measurement_stopping")
    @coroutine
    def measurement_stop(self):
        pass

    @state_change(target="configured", allowed=["ready"], intermediate="capture_stopping")
    @coroutine
    def capture_stop(self):
        pass

    @state_change(target="idle", intermediate="deconfiguring", error='panic')
    @coroutine
    def deconfigure(self):
        log.info("Deconfiguring EDD backend")


    def register(self, host=None, port=None):
        """
        Registers the pipeline in the data store.

        Args:
            host, port: Ip and port of the data store.

        If no host and port ar eprovided, values from the internal config are used.
        """
        EDDPipeline.register(self, host, port)
        self._config["data_store"]["ip"] = host
        self._config["data_store"]["port"] = port
        try:
            self.__eddDataStore = EDDDataStore.EDDDataStore(host, port)
        except:
            log.error("Failed to connect to EDDDataStore")

if __name__ == "__main__":
    launchPipelineServer(ComplexGainSolverPipeline)
