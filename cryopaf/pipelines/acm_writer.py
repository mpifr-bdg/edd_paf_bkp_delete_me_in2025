import mpikat.utils.numa as numa
from mpikat.utils.process_monitor import SubprocessMonitor
from mpikat.utils.sensor_watchdog import SensorWatchdog
from mpikat.utils.spead_capture import SpeadCapture
from mpikat.core.edd_pipeline import EDDPipeline, launchPipelineServer, updateConfig, state_change
from mpikat.core.datastore import EDDDataStore

from cryopaf.utils.acm_file import ACMFile
from cryopaf.utils.spead_handlers import XEngineSpeadHandler
from cryopaf.conf.constants import *


import os
import logging, time
import numpy as np
import json
import tornado
from datetime import datetime
from copy import deepcopy
from tornado.gen import coroutine
from katcp import Sensor, AsyncReply, FailReply

log = logging.getLogger("mpikat.cryopaf.pipelines.ACMWriterPipeline")


DEFAULT_CONFIG = {
    "id": "ACMWriterPipeline",
    "type": "ACMWriterPipeline",
    "host" : "pacifix3",
    "port" : 7148,
    "dataset_dir" :"",
    "subband_id":0,
    "channels_total":1024,
    "channels_per_mcgroup":16,
    "input_data_streams": [],
    "acm_storage":"/tmp/acms",
    "data_store" :
    {
        "host" : "pacifix6",
        "port" : 6379
    }
}
def check_dir(dir):
    if not os.path.exists(dir):
        log.warning("ACM storage path '{}' does not exists, creating it".format(dir))
        os.mkdir(dir)
    if dir[-1] != "/":
        dir += "/"
    return dir



class PackageParser(object):

    def __init__(self, ts_stepwidth=4096, n_heap_groups=4):
        self.n_heap_groups = n_heap_groups
        self.heap_groups = np.zeros((n_heap_groups, N_PFB_CHANNELS, N_PAF_ELEMENTS, N_PAF_ELEMENTS, 2), dtype='float32')
        self.n_heaps_per_group = N_PFB_CHANNELS // CHANNELS_PER_SUBBAND * N_PAF_ELEMENTS
        self.current_timestamp = None
        self.write_counters = np.zeros(n_heap_groups)
        self.total_cnt = 0
        self.stepwidth = ts_stepwidth
        self.trash_counter = 0

    def parse(self, packet):
        chan_start = (packet.order_vector & 0xffff0000) * CHANNELS_PER_SUBBAND # Subband ID
        chan_stop = chan_start + CHANNELS_PER_SUBBAND # Subband ID
        elem_id = packet.order_vector & 0x0000ffff # Element ID
        # Set the first timestamp value
        if self.current_timestamp == None:
            log.info("Received first heap, setting timestamp to {}".format(packet.timestamp))
            self.current_timestamp = packet.timestamp

        # Checking Timestamps and determine heap_group_id
        if self.current_timestamp < packet.timestamp:
            log.debug("Heap of future heap group received with timestamp {}".format(packet.timestamp))
            heap_group_id = ((packet.timestamp - self.current_timestamp) // self.stepwidth) - 1
            if heap_group_id >= self.n_heap_groups:
                log.warning("Heap with timestamp {} to far in future can not add to buffer".format(packet.timestamp))
                self.trash_counter += 1
                return None
        elif self.current_timestamp > packet.timestamp:
            log.warning("Timestamp {} to old, discarding heap".format(packet.timestamp))
            self.trash_counter += 1
            return None
        # Counter increment and inserting payload data
        self.write_counters[heap_group_id] += 1
        self.heap_groups[heap_group_id, chan_start:chan_stop, elem_id] = packet.data

        if self.write_counters[0] == self.n_heaps_per_group:
            log.debug("Heap group for timestamp {} full, releasing it".format(self.current_timestamp))
            return self.heap_groups[0]
        elif self.write_counters.sum() >= self.n_heaps_per_group * (self.n_heap_groups // 2):
            log.warning("Heap group for timestamp {} incomplete ({}/{}), but need to move to next group."\
                .format(self.current_timestamp, self.write_counters[0], self.n_heaps_per_group))
            return self.heap_groups[0]
        return None

    def reset(self):
        self.heap_groups[0] = np.zeros((N_PFB_CHANNELS, N_PAF_ELEMENTS, N_PAF_ELEMENTS, 2), dtype='float32')
        self.heap_groups = np.roll(self.heap_groups, -1, axis=0)
        self.write_counters[0] = 0
        self.write_counters = np.roll(self.write_counters, -1)
        self.current_timestamp += self.stepwidth


class ACMWriterPipeline(EDDPipeline):

    def __init__(self, ip, port):
        EDDPipeline.__init__(self, ip, port, DEFAULT_CONFIG)
        self.__eddDataStore = 0
        self._acm_files = []

    @coroutine
    def setup_sensors(self):
        """
        @brief Setup monitoring sensors
        """
        EDDPipeline.setup_sensors(self)

    @state_change(target="configured", allowed=["idle", "configured"], intermediate="configuring")
    @coroutine
    def configure(self, config_json={}):
        log.info("Configuring ACMWriterPipeline")
        log.debug("Configruation string: {}".format(config_json))
        self._config["acm_storage"] = check_dir(self._config["acm_storage"])


    @state_change(target="ready", allowed=["configured"], intermediate="capture_starting")
    @coroutine
    def capture_start(self):
        pass
        self.capture_x_engine = SpeadCapture(self._config["input_data_streams"], XEngineSpeadHandler(), self._package_handler)

    @state_change(target="set", allowed=["ready"], intermediate="measurement_starting")
    @coroutine
    def measurement_prepare(self):
        self._package_parser = PackageParser()

    @state_change(target="streaming", allowed=["set", "ready"], intermediate="measurement_starting")
    @coroutine
    def measurement_start(self):
        self.capture_x_engine.start()

    @state_change(target="ready", allowed=["streaming"], intermediate="measurement_stopping")
    @coroutine
    def measurement_stop(self):
        pass

    @state_change(target="configured", allowed=["ready"], intermediate="capture_stopping")
    @coroutine
    def capture_stop(self):
        self.capture_x_engine.stop()

    @state_change(target="idle", intermediate="deconfiguring", error='panic')
    @coroutine
    def deconfigure(self):
        log.info("Deconfiguring EDD backend")

    def _package_handler(self, packet):
        ret = self._package_parser.parse(packet)
        fname = self._config["acm_storage"] + datetime.now().strftime("%Y_%m_%d_%H_%M_%S") + ".acm.hdf5"
        log.debug("Creating ACM file {}".format(fname))
        acm_file = ACMFile(fname)
        acm_file.create()
        acm_file["ACMdata"] = data
        self.acm_files.append(acm_file)

    def register(self, host=None, port=None):
        """
        Registers the pipeline in the data store.

        Args:
            host, port: Ip and port of the data store.

        If no host and port ar eprovided, values from the internal config are used.
        """
        EDDPipeline.register(self, host, port)
        self._config["data_store"]["ip"] = host
        self._config["data_store"]["port"] = port
        try:
            self.__eddDataStore = EDDDataStore.EDDDataStore(host, port)
        except:
            log.error("Failed to connect to EDDDataStore")

if __name__ == "__main__":
    launchPipelineServer(ACMWriterPipeline)
