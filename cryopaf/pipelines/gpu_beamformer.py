"""
Copyright (c) 2021 Niclas Esser <nesser@mpifr-bonn.mpg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
# MPIKAT modules
from __future__ import print_function, division, unicode_literals

from mpikat.utils.process_tools import ManagedProcess, command_watcher
from mpikat.utils.process_monitor import SubprocessMonitor
from mpikat.utils.sensor_watchdog import SensorWatchdog # ToDo: add watchdog to receiving sensor
from mpikat.utils.db_monitor import DbMonitor
from mpikat.utils.mkrecv_stdout_parser import MkrecvSensors
from mpikat.core.edd_pipeline import EDDPipeline, launchPipelineServer, updateConfig, state_change
from mpikat.core.datastore import EDDDataStore
import mpikat.utils.numa as numa
from mpikat.utils.core_manager import CoreManager

from tornado.gen import coroutine, sleep
from katcp import Sensor, AsyncReply, FailReply, Message

from cryopaf.utils.mkheader import MkHeader
from cryopaf.utils.key_gen import DadaKeyGen
from cryopaf.conf.spead_conf import data_formats
from cryopaf.conf.constants import *


from datetime import datetime
from astropy.time import Time
from copy import deepcopy

try:
    import mpikat.utils.pynvml as pynvml
    pynvml.nvmlInit()
    __haspynvml = True
except Exception as E:
    log.warning("pynvml not available. Cannot use GPUs (if any)")
    __haspynvml = False

# Common used modules
import os
import logging
import json
import signal
import time
import coloredlogs
from tempfile import NamedTemporaryFile

log = logging.getLogger("mpikat.cryopaf.pipelines.BeamformPipeline")

# CONSTANTS
CUDA_COMPUTE_CAPABILTY = 7
DADA_SLOTS = 16
MAX_RAW_BEAMS = 16
N_DADA_BUFFERS = 4
N_POL = 2

# The dictionary below is the default configuration of this EDD pipeline
DEFAULT_CONFIG = {
    # Pipeline identifiers
    "id": "pafbeamform_0",                # Id of pipeline (automatically set by ansible)
    "port" : 7148,                      # KATCP port (automatically set by ansible)
    "ip" : "pacifix1",                  # KATCP host (automatically set by ansible)
    "telescope" : "Effelsberg",         # Used telescope
    "utc_start" : "unset",                   # Must be empty or isot compatible (e.g. 2018-01-01T00:00:00)
    "total_bandwidth" : 2e9,                # Center frequency of the entire band
    "channel_bandwidth" : 2e9/(1024),                # Center frequency of the entire band
    "n_heap_groups":32,
    # General parameters used for calculation and configuration
    "n_beams" : 16,                       # Number of beams that should be processed
    "nfft" : 32,                        # Size of 2nd FFT
    "numerator" : 32,                   # Oversample numerator
    "denumerator" : 27,                  # Oversample denumerator
    "subband_ids" : [0],                     # Number of channels received
    # Paramters for beamform process
    "integration": 64,                   # Accumulation / integration used for Stokes I beamformer to integrate ( < 32 )
    "weight_scale":1,
    "output_type" : "network",          # Possible options: "network", "disk", "null"
    "output_directory" : "/mnt",        # If output_type equals "disk", then this specify the output root directory
    "dummy_input" : False,              # If set to True, it generates dummy input data with dada_dbjunk
    # Data store dictionary (Redis DB) used for communication with MC and PAFcontroller
    "data_store":
    {
        "ip":"pacifix6",                # Redis ip
        "port":6379                     # Redis port
    },
    # Input stream configuration
    "input_data_streams":
    [
        {
            "format" : "CRYO_PAF_CHANNELIZER:1",
            "ip": "226.0.0.0",          # IP address of NIC
            "port": 17100,              # Capture ports
            "bit_depth" : data_formats["CRYO_PAF_CHANNELIZER:1"]["nbit"]          #  uses 16 bit integer for real + imaginary
        }
    ],
    # Output stream configuration
    "output_data_streams":
    {
        "voltage_beam_00":
        {
            "format" : "CRYO_PAF_CBF_VOLTAGE_BEAM:1",
            "ip": "226.0.1.0",
            "port" : 17102,
            "bit_depth": data_formats["CBF_STOKES_I_BEAM:1"]["nbit"]            # Will be set automatically
        },
        "stokes_i_beams_000":
        {
            "format" : "CBF_STOKES_I_BEAM:1",
            "ip": "226.0.3.0",
            "port" : 17102,
            "bit_depth": data_formats["CBF_STOKES_I_BEAM:1"]["nbit"]            # Will be set automatically
        },
        "full_stokes_beams_000":
        {
            "format" : "CBF_FULL_STOKES_BEAM:1",
            "ip": "226.0.2.0",
            "port" : 17103,
            "bit_depth": data_formats["CBF_FULL_STOKES_BEAM:1"]["nbit"]           # Will be set automatically
        }
    }
}

class BeamformPipeline(EDDPipeline):
    """
    @brief An EDDPipeline to process beams from raw voltages of the current PAF in Effelsberg
    """
    VERSION_INFO = ("mpikat-edd-api", 0, 1)
    BUILD_INFO = ("mpikat-edd-implementation", 0, 1, "rc1")

    def __init__(self, ip, port):
        """
        @brief initialize the pipeline.
        @param ip: KATCP ip address
        @param port: KATCP port
        """
        EDDPipeline.__init__(self, ip, port, DEFAULT_CONFIG)
        self._dada_buffers = []
        self._dada_keys = DadaKeyGen().getFreeDadaKeys(N_DADA_BUFFERS, mode="ascending")
        self.__eddDataStore = 0
        self._set_numa_node()

    def setup_sensors(self):
        """
        @brief Setup monitoring sensors
        """
        EDDPipeline.setup_sensors(self)

        self._mkrecv_sensor = MkrecvSensors(self._config["id"] + "_input_stream")
        for sensor in self._mkrecv_sensor.sensors.values():
            self.add_sensor(sensor)

        self._input_fill_level_sensor = Sensor.float(
                "input-buffer-fill-level",
                description="Fill level of the input buffer")
        self.add_sensor(self._input_fill_level_sensor)

        self._input_total_write_sensor = Sensor.float(
                "input-buffer-total-write",
                description="Total write into input buffer ")
        self.add_sensor(self._input_total_write_sensor)

        self._output_voltage_beam_fill_level_sensor = Sensor.float(
                "output-buffer-fill-level",
                description="Fill level of the output buffer")
        self.add_sensor(self._output_voltage_beam_fill_level_sensor)

        self._output_voltage_beam_total_read_sensor = Sensor.float(
                "output-buffer-total-read",
                description="Total read from output buffer")
        self.add_sensor(self._output_voltage_beam_total_read_sensor)

        self._output_stokes_i_beams_fill_level_sensor = Sensor.float(
                "output-stokes_i_beams-buffer-fill-level",
                description="Fill level of the output buffer")
        self.add_sensor(self._output_stokes_i_beams_fill_level_sensor)

        self._output_stokes_i_beams_total_read_sensor = Sensor.float(
                "output-stokes_i_beams-buffer-total-read",
                description="Total read from output buffer")
        self.add_sensor(self._output_stokes_i_beams_total_read_sensor)

        self._output_full_stokes_beams_fill_level_sensor = Sensor.float(
                "output-full_stokes_beams-buffer-fill-level",
                description="Fill level of the output buffer")
        self.add_sensor(self._output_full_stokes_beams_fill_level_sensor)

        self._output_full_stokes_beams_total_read_sensor = Sensor.float(
                "output-full_stokes_beams-buffer-total-read",
                description="Total read from output buffer")
        self.add_sensor(self._output_full_stokes_beams_total_read_sensor)


    @state_change(target="configured", allowed=["idle", "configured"], intermediate="configuring")
    @coroutine
    def configure(self, config_json={}):
        """
        @brief   Configure beamform pipeline

        @param   config_json    A JSON dictionary object containing configuration information
        """
        log.info("Configuring EDD backend for processing")
        if self._config["n_beams"]%16:
            raise FailReply("key 'n_beams' must be a multiple of 16")
        #---------------------------------------------#
        # Calculations based on latest configurations #
        #---------------------------------------------#
        # General calculations
        self._config["channel_bandwidth"] = data_formats["CRYO_PAF_CHANNELIZER:1"]["sample_clock"] * self._config["numerator"] / (N_PFB_CHANNELS * self._config["denumerator"])
        self.n_samples = self._config["n_heap_groups"] * data_formats["CRYO_PAF_CHANNELIZER:1"]["heap_size"] // data_formats["CRYO_PAF_CHANNELIZER:1"]["nbit"] * 8
        self.n_subbands = len(self._config["subband_ids"])
        self.n_channel = self.n_subbands * CHANNELS_PER_SUBBAND
        # Calculate params for input raw voltage
        self.input_heap_sz = data_formats["CRYO_PAF_CHANNELIZER:1"]["heap_size"]
        self.n_samples_per_heap_input = self.input_heap_sz // data_formats["CRYO_PAF_CHANNELIZER:1"]["nbit"] * 8
        self.input_num_heaps = N_PAF_ELEMENTS * self.n_samples * self.n_channel / self.n_samples_per_heap_input
        self.input_buffer_sz = int(self.input_heap_sz * self.input_num_heaps)
        self.input_data_rate = self.n_channel \
            * N_PAF_ELEMENTS \
            * float(self._config["channel_bandwidth"]) \
            * data_formats["CRYO_PAF_CHANNELIZER:1"]["nbit"] / 8
        # Calculate params for voltage output
        self.output_voltage_heap_size = data_formats["CRYO_PAF_CBF_VOLTAGE_BEAM:1"]["heap_size"]
        self.n_samples_per_heap_voltage = self.output_voltage_heap_size // (data_formats["CRYO_PAF_CBF_VOLTAGE_BEAM:1"]["nbit"]*N_POL) * 8
        self.output_voltage_num_heaps = MAX_RAW_BEAMS * self.n_samples * self.n_channel / self.n_samples_per_heap_voltage
        self.output_voltage_buffer_sz = int(self.output_voltage_heap_size * self.output_voltage_num_heaps)
        self.output_voltage_data_rate = self.n_channel \
            * MAX_RAW_BEAMS * N_POL \
            * float(self._config["channel_bandwidth"]) \
            * data_formats["CRYO_PAF_CBF_VOLTAGE_BEAM:1"]["nbit"] / 8
        # Calculate params for stokes I output
        self.output_stokesi_heap_size = data_formats["CBF_STOKES_I_BEAM:1"]["heap_size"]
        self.n_samples_per_heap_stokesi = self.output_stokesi_heap_size // data_formats["CBF_STOKES_I_BEAM:1"]["nbit"] * 8
        self.output_stokesi_num_heaps = self._config["n_beams"] * self.n_samples * self.n_channel / (self.n_samples_per_heap_stokesi * self._config["nfft"])
        self.output_stokesi_buffer_sz = int(self.output_stokesi_heap_size * self.output_stokesi_num_heaps)
        self.output_stokesi_data_rate = self.n_channel \
            * self._config["n_beams"] \
            * float(self._config["channel_bandwidth"]) \
            * data_formats["CBF_STOKES_I_BEAM:1"]["nbit"] \
            * self._config["denumerator"] \
            / (8*self._config["numerator"]*self._config["nfft"])
        # Calculate params for full stokes output
        self.output_stokesf_heap_size = data_formats["CBF_FULL_STOKES_BEAM:1"]["heap_size"]
        self.n_samples_per_heap_stokesf = self.output_stokesf_heap_size // data_formats["CBF_FULL_STOKES_BEAM:1"]["nbit"] * 8# * (1 - 1/self._config["numerator"] * (self._config["numerator"] - self._config["denumerator"]))
        self.output_stokesf_num_heaps = self._config["n_beams"] * self.n_samples * self.n_channel / (self.n_samples_per_heap_stokesi * self._config["integration"])
        self.output_stokesf_buffer_sz = int(self.n_samples \
             * self._config["n_beams"] \
             * self.n_channel \
             * (1 - 1/self._config["numerator"] * (self._config["numerator"] - self._config["denumerator"])) \
             * data_formats["CBF_FULL_STOKES_BEAM:1"]["nbit"] \
             / (self._config["integration"] * 8))

        self.output_stokesf_data_rate = self.n_channel \
            * self._config["n_beams"] \
            * data_formats["CBF_FULL_STOKES_BEAM:1"]["nbit"] \
            * float(self._config["channel_bandwidth"]) \
            * self._config["denumerator"] \
            / (8*self._config["integration"]*self._config["numerator"])

        log.info('\nParameters for input stream:\n\
            DADA key:              {} \n\
            Heap size:             {} byte\n\
            Samples per heap:      {} \n\
            Heaps per block:       {}\n\
            Total buffer size:     {} byte\n\
            Datarate:              {:.2f} Bytes/s'
                .format(self._dada_keys[0],
                    self.input_heap_sz,
                    self.n_samples_per_heap_input,
                    self.input_num_heaps,
                    self.input_buffer_sz,
                    self.input_data_rate))
        log.info('\nParameters for output voltage stream:\n\
            DADA key:              {} \n\
            Heap size:             {} byte\n\
            Samples per heap:      {} \n\
            Heaps per block:       {} \n\
            Total buffer size:     {} byte\n\
            Datarate:              {:.2f} Bytes/s'
                .format(self._dada_keys[1],
                    self.output_voltage_heap_size,
                    self.n_samples_per_heap_voltage,
                    self.output_voltage_num_heaps,
                    self.output_voltage_buffer_sz,
                    self.output_voltage_data_rate))
        log.info('\nParameters for output stokes I stream (high temporal res.):\n\
            DADA key:              {} \n\
            Heap size:             {} byte\n\
            Samples per heap:      {} \n\
            Heaps per block:       {}\n\
            Total buffer size:     {} byte\n\
            Datarate:              {:.2f} Bytes/s'
                .format(self._dada_keys[2],
                    self.output_stokesi_heap_size,
                    self.n_samples_per_heap_stokesi,
                    self.output_stokesi_num_heaps,
                    self.output_stokesi_buffer_sz,
                    self.output_stokesi_data_rate))
        log.info('\nParameters for output full stokes stream (high frequency res.):\n\
            DADA key:              {} \n\
            Heap size:             {} byte\n\
            Samples per heap:      {} \n\
            Heaps per block:       {}\n\
            Total buffer size:     {} byte\n\
            Datarate:              {:.2f} Bytes/s'
                .format(self._dada_keys[3],
                    self.output_stokesf_heap_size,
                    self.n_samples_per_heap_stokesf,
                    self.output_stokesf_num_heaps,
                    self.output_stokesf_buffer_sz,
                    self.output_stokesf_data_rate))

        # Update data stream
        if "subband_ids" in config_json:
            self._update_input_stream()
        self._update_output_voltage_streams()
        log.debug("Configuration string: '{}'".format(config_json))
        log.debug("Final config: '{}'".format(json.dumps(self._config, indent=4)))
        # Add input streams to Redis
        if self.__eddDataStore != 0:
            self.__eddDataStore.addDataStream(self._config["id"] + "_input_stream", self._config["input_data_streams"][0])

        # Monitor all subprocesses
        self._subprocessMonitor = SubprocessMonitor()

        log.debug("Creating ring buffers")
        # create dada buffers
        yield self._create_ring_buffer(self.input_buffer_sz, DADA_SLOTS, \
            self._dada_keys[0], self.numa_node)
        yield self._create_ring_buffer(self.output_voltage_buffer_sz, DADA_SLOTS, \
            self._dada_keys[1], self.numa_node)
        yield self._create_ring_buffer(self.output_stokesi_buffer_sz, DADA_SLOTS, \
            self._dada_keys[2], self.numa_node)
        yield self._create_ring_buffer(self.output_stokesf_buffer_sz, DADA_SLOTS, \
            self._dada_keys[3], self.numa_node)
        log.debug("Ringbuffers created")
        # # specify all subprocesses
        self.__coreManager = CoreManager(self.numa_node)
        self.__coreManager.add_task("mkrecv", self.n_subbands+1)
        self.__coreManager.add_task("processing", 2)
        self.__coreManager.add_task("weightupdater", 1)
        self.__coreManager.add_task("mksend_voltage", 1)
        self.__coreManager.add_task("mksend_full_stokes", 1)
        self.__coreManager.add_task("mksend_stokes_i", 1)

        # Instance to handle all subprocesses
        self._subprocessMonitor = SubprocessMonitor()

        # Launch processing process
        processing_cmd = self._get_processing_cmd()
        log.info("Processing command: {}".format(processing_cmd))
        self.processing_cli = ManagedProcess(processing_cmd, env={"CUDA_VISIBLE_DEVICES": self.__cuda_viable_devices[0]})
        self._subprocessMonitor.add(self.processing_cli, self._subprocess_error)
        self._subprocesses.append(self.processing_cli)
        # Launch weight updater process
        weightupdater_cmd = self._get_weight_interface_cmd()
        log.info("Weightupdate command: {}".format(weightupdater_cmd))
        self.weightupdater_cli = ManagedProcess(weightupdater_cmd)
        self._subprocessMonitor.add(self.weightupdater_cli, self._subprocess_error)
        self._subprocesses.append(self.weightupdater_cli)
        # Lauch output handle command
        output_handle_voltage_cmd = self._get_output_handle_cmd("CRYO_PAF_CBF_VOLTAGE_BEAM:1", self._dada_keys[1], self._config["output_type"], "mksend_voltage")
        log.info("Output handle command for voltage beams: {}".format(output_handle_voltage_cmd))
        self.output_handle_voltage_cli = ManagedProcess(output_handle_voltage_cmd)
        self._subprocessMonitor.add(self.output_handle_voltage_cli, self._subprocess_error)
        self._subprocesses.append(self.output_handle_voltage_cli)
        # Lauch output handle command
        output_handle_stokesi_cmd = self._get_output_handle_cmd("CBF_STOKES_I_BEAM:1", self._dada_keys[2], self._config["output_type"], "mksend_full_stokes")
        log.info("Output handle command for Stokes I beams: {}".format(output_handle_stokesi_cmd))
        self.output_handle_stokesi_cli = ManagedProcess(output_handle_stokesi_cmd)
        self._subprocessMonitor.add(self.output_handle_stokesi_cli, self._subprocess_error)
        self._subprocesses.append(self.output_handle_stokesi_cli)
        # Lauch output handle command
        output_handle_fullstokes_cmd = self._get_output_handle_cmd("CBF_FULL_STOKES_BEAM:1", self._dada_keys[3], self._config["output_type"], "mksend_stokes_i")
        log.info("Output handle command for full Stokes beams: {}".format(output_handle_fullstokes_cmd))
        self.output_handle_fullstokes_cli = ManagedProcess(output_handle_fullstokes_cmd)
        self._subprocessMonitor.add(self.output_handle_fullstokes_cli, self._subprocess_error)
        self._subprocesses.append(self.output_handle_fullstokes_cli)

        log.info("Successfully configured EDD")

    @state_change(target="ready", allowed=["configured"], intermediate="capture_starting")
    @coroutine
    def capture_start(self):
        self._get_telescope_meta()
        capture_cmd = self._get_capture_cmd()

        # Launch capture process
        log.info("Capture command: {}".format(capture_cmd))
        self.capture_cli = ManagedProcess(capture_cmd)
        self._subprocessMonitor.add(self.capture_cli, self._subprocess_error)
        self._subprocesses.append(self.capture_cli)

    @state_change(target="set", allowed=["ready"], intermediate="measurement_preparing")
    @coroutine
    def measurement_prepare(self, config_json={}):
        pass


    @state_change(target="streaming", allowed=["set", "ready"], intermediate="measurement_starting")
    @coroutine
    def measurement_start(self):
        """
        @brief start streaming beamformed output
        """
        log.info("Starting measuring")

    @state_change(target="ready", allowed=["streaming"], intermediate="measurement_stopping")
    @coroutine
    def measurement_stop(self):
        pass

    @state_change(target="configured", allowed=["ready"], intermediate="capture_stopping")
    @coroutine
    def capture_stop(self):
        """
        @brief Stop streaming of data
        """
        log.info("Stoping capturing")
        if self.capture_cli.is_alive():
            yield self.capture_cli.terminate()
        else:
            log.warning("Capture process already terminated")

    @state_change(target="idle", intermediate="deconfiguring", error='panic')
    @coroutine
    def deconfigure(self):
        """
        @brief deconfigure the beamform pipeline.
        """
        log.info("Deconfiguring EDD backend")

        if self.previous_state == 'streaming':
            yield self.capture_stop()

        if self._subprocessMonitor is not None:
            yield self._subprocessMonitor.stop()
        for proc in self._subprocesses:
            if proc is not None:
                yield proc.terminate()

        log.debug("Destroying dada buffers")
        for buffer in self._dada_buffers:
            self._destroy_ring_buffer(buffer)

        self._dada_buffers = []

    def _get_processing_cmd(self):
        """@brief Parses the given input configuration to the processing command"""
        cmd = "taskset -c {physcpu} {process_cli} \
                --in_key {in_key} \
                --out_vol_key {out_vol_key} \
                --out_sti_key {out_sti_key} \
                --out_stf_key {out_stf_key} \
                --samples {samples} \
                --channels {channels} \
                --elements {elements} \
                --beams {beams} \
                --integration {integration}".replace("\n","") \
            .format(physcpu=self.__coreManager.get_coresstr('processing'),
                process_cli="beamforming_cli ",
                in_key=self._dada_keys[0],
                out_vol_key=self._dada_keys[1],
                out_sti_key=self._dada_keys[2],
                out_stf_key=self._dada_keys[3],
                samples=self.n_samples,
                channels=self.n_channel,
                elements=N_PAF_ELEMENTS // 2,
                beams=self._config["n_beams"],
                integration=self._config["integration"])
        return cmd

    def _get_weight_interface_cmd(self):
        """@brief Parses the given input configuration to the weight interface"""
        cmd = "taskset -c {physcpu} {cli} \
                --redis_channel {redis_channel} \
                --channels {channels} \
                --elements {elements} \
                --beams {beams} \
                --scale {scale} \
                --host {host} \
                --port {port}" \
            .format(physcpu=self.__coreManager.get_coresstr('weightupdater'),
                cli="weightupdater_cli",
                redis_channel=self._config["id"],
                channels=self.n_channel,
                elements=N_PAF_ELEMENTS // 2,
                beams=self._config["n_beams"],
                scale=self._config["weight_scale"],
                host=self._config["data_store"]["ip"],
                port=self._config["data_store"]["port"])
        return cmd

    def _get_capture_cmd(self):

        # Prepare dictionary 'mkrecv_format' for header generator
        mkrecv_format = data_formats["CRYO_PAF_CHANNELIZER:1"]
        mkrecv_format['freq'] = self.n_channel
        mkrecv_format['utc_start'] = self._config['utc_start']
        mkrecv_format['dada_key'] = self._dada_keys[0]
        mkrecv_format['port'] = self._config["input_data_streams"][0]["port"]
        mkrecv_format['bytes_per_second'] = int(self.input_data_rate)
        mkrecv_format['resolution'] = 1
        mkrecv_format['dada_nslots'] = DADA_SLOTS
        mkrecv_format["slot_nbytes"] = int(self.input_buffer_sz)
        mkrecv_format["heap_nbytes"] = mkrecv_format["heap_size"]
        mkrecv_format["sync_time"] = str(int(datetime.now().timestamp()))
        mkrecv_format["file_size"] = int(self.input_buffer_sz)
        if "udp" in mkrecv_format:
            mkrecv_format['udp_if'] = self.nic_params['ip']
        else:
            mkrecv_format['ibv_if'] = self.nic_params['ip']
        ## Make mkrecv header
        hdr = MkHeader(NamedTemporaryFile(delete=False).name, 'mkrecv', **mkrecv_format)
        hdr.create()
        hdr.add_items(mkrecv_format["spead_desc"], ignore=["item8"])
        # Use dada_junkdb to generate dummy data
        if self._config["dummy_input"]:
            log.warning("Creating dummy input instead of network capture!")
            cmd = "dada_junkdb -c 1 -R {fs} -t 60 -k {key} {header}" \
                .format(fs=1000, key=self._dada_keys[0], header=hdr.name())
        # Use network to capture data
        else:
            log.debug("Network capturing.. ")
            cmd = "taskset -c {physcpu} mkrecv_v4 --quiet --lst \
                    --header {header} \
                    --heap-size {heap_size} \
                    --nthreads {nthreads} \
                    --dada-key {dada_key} \
                    --sync-epoch {sync_time} \
                    --udp-if {udp_if} \
                    --port {port} {mc_ip}" \
                .format(physcpu=self.__coreManager.get_coresstr('mkrecv'),
                    header=hdr.name(),
                    nthreads = len(self.__coreManager.get_cores('mkrecv')) - 1,
                    mc_ip=self._config["input_data_streams"][0]["ip"],
                    **data_formats["CRYO_PAF_CHANNELIZER:1"])
        return cmd

    def _get_output_handle_cmd(self, format, key, output_type, process_name, dir=""):

        log.info("Creating out handle command for format {}".format(format))
        stream_configs = []
        # Parse multicast streams
        try:
            for stream in self._config["output_data_streams"].values():
                if stream["format"] == format:
                    stream_configs.append(deepcopy(stream))
        except Exception as error:
            log.error("No output_data_streams with format {} specified in config".format(format))
            raise FailReply("No output_data_streams with format {} specified in config".format(format))

        stream_configs.sort(key=lambda x: x['ip'])
        # Network transmission via mksend / spead
        if output_type == "network":
            mksend_format = data_formats[format]
            mksend_format["dada_key"] = key
            if "udp" in mksend_format:
                mksend_format['udp_if'] = self.nic_params['ip']
            else:
                mksend_format['ibv_if'] = self.nic_params['ip']
            ## Make mksend header
            hdr = MkHeader(NamedTemporaryFile(delete=False).name, 'mksend', **mksend_format)
            hdr.create()
            hdr.add_items(mksend_format["spead_desc"])

            cmd = "taskset -c {physcpu} mksend --header {header}" \
                .format(physcpu=self.__coreManager.get_coresstr(process_name), header=hdr.name())
            for stream in stream_configs:
                cmd += " " + str(stream["ip"])
        # Data to disk
        elif output_type == "disk":
            cmd = "dada_dbdisk -k {key} -D {dir} -W".format(key=key, dir=self._config["output_directory"])
        # No recording or transmission
        elif output_type == "null":
            cmd = "dada_dbnull -k {key} -q".format(key=key)
        else:
            log.error("output_type not known which is {}".format(output_type))
            return ""
        return cmd

    def _get_telescope_meta(self):
        """
        @brief Get the current telescope meta data from Redis
        """
        pass
        # if self.__eddDataStore != 0:
        #     self.source_name = self.__eddDataStore.getTelescopeDataItem("source-name").replace("_", "-")
        #     self.ra = (self.__eddDataStore.getTelescopeDataItem("ra")).replace("_", "-")
        #     self.dec = (self.__eddDataStore.getTelescopeDataItem("dec")).replace("_", "-")
        #     self.scannum = self.__eddDataStore.getTelescopeDataItem("scannum").replace("_", "-")
        #     self.receiver = self.__eddDataStore.getTelescopeDataItem("receiver").replace("_", "-")
        #     log.debug("Retrieved data from telescope:\n   Source name: {}\n   RA = {},  dec = {} scan = {}".format(self.source_name, self.ra, self.dec, self.scannum))


    @coroutine
    def _create_ring_buffer(self, buffer_size, blocks, key, numa_node):
         """
         @brief Create a ring buffer of given size with given key on specified numa node.
                Adds and register an appropriate sensor to thw list
         """
         # always clear buffer first. Allow fail here
         yield command_watcher("dada_db -d -k {key}".format(key=key), allow_fail=True)

         cmd = "numactl --cpubind={numa_node} --membind={numa_node} dada_db -k {key} -n {blocks} -b {buffer_size} -p -l" \
            .format(key=key, blocks=blocks, buffer_size=int(buffer_size), numa_node=numa_node)

         log.debug("Running command: {0}".format(cmd))
         yield command_watcher(cmd, allow_fail=True)

         M = DbMonitor(key, self._buffer_status_handle)
         M.start()
         self._dada_buffers.append({'key': key, 'monitor': M})

    @coroutine
    def _destroy_ring_buffer(self, buffer):
         """
         @brief Create a ring buffer of given size with given key on specified numa node.
                Adds and register an appropriate sensor to thw list
         """
         # always clear buffer first. Allow fail here
         buffer['monitor'].stop()
         cmd = "dada_db -d -k {0}".format(buffer['key'])
         log.debug("Running command: {0}".format(cmd))
         yield command_watcher(cmd)

    def _set_numa_node(self):
        # Get NUMA node and its charachteristics
        self.__numa_node_pool = []
        self.__cuda_viable_devices = []
        for node in numa.getInfo():
            cuda_devices = numa.getInfo()[node]['gpus']
            for ii in cuda_devices:
                handle = pynvml.nvmlDeviceGetHandleByIndex(int(ii))
                major, minor = pynvml.nvmlDeviceGetCudaComputeCapability(handle)
                if major >= CUDA_COMPUTE_CAPABILTY:
                    self.__cuda_viable_devices.append(ii)
            # remove numa nodes with missing capabilities
            if len(cuda_devices) < 1:
                log.debug("Not enough gpus on numa node {} - removing from pool.".format(node))
                continue
            elif len(numa.getInfo()[node]['net_devices']) < 1:
                log.debug("Not enough nics on numa node {} - removing from pool.".format(node))
                continue
            elif len(self.__cuda_viable_devices) < 1:
                log.debug("No viable CUDA device found on numa node {} - removing from pool.".format(node))
                continue
            else:
                self.__numa_node_pool.append(node)

        log.debug("NUMA nodes {} remain in pool after constraints.".format(len(self.__numa_node_pool)))

        if len(self.__numa_node_pool) == 0:
            raise FailReply("Not enough numa nodes to process data!")
        self.numa_node = self.__numa_node_pool[0]

        log.debug("Associating with numa node {}".format(self.numa_node))

        self.fastest_nic, self.nic_params = numa.getFastestNic(self.numa_node)
        log.info("Will receive data on NIC {} [ {} ] @ {} Mbit/s" \
            .format( \
                self.fastest_nic, \
                self.nic_params["ip"], \
                self.nic_params['speed']))



    def _buffer_status_handle(self, status):
        """
        @brief Process a change in the buffer status
        """
        if status['key'] == self._dada_keys[0]:
            self._input_fill_level_sensor.set_value(status['fraction-full'])
            self._input_total_write_sensor.set_value(status['written'])
        elif status['key'] == self._dada_keys[1]:
            self._output_voltage_beam_fill_level_sensor.set_value(status['fraction-full'])
            self._output_voltage_beam_total_read_sensor.set_value(status['read'])
        elif status['key'] == self._dada_keys[2]:
            self._output_stokes_i_beams_fill_level_sensor.set_value(status['fraction-full'])
            self._output_stokes_i_beams_total_read_sensor.set_value(status['read'])
        elif status['key'] == self._dada_keys[3]:
            self._output_full_stokes_beams_fill_level_sensor.set_value(status['fraction-full'])
            self._output_full_stokes_beams_total_read_sensor.set_value(status['read'])

    def _update_input_stream(self):
        self._config["input_data_streams"]["ip"] = ""
        for id in self._config["subband_ids"]:
            self._config["input_data_streams"][0]["ip"] += "226.0.0.{:03d}".format(id)

    def _update_output_voltage_streams(self):
        for id in range(MAX_RAW_BEAMS):
            stream_name = "voltage_beam_{:02d}".format(id)
            if stream_name in self._config["output_data_streams"]:
                continue
            self._config["output_data_streams"][stream_name] = {
                "format" : "CRYO_PAF_CBF_VOLTAGE_BEAM:1",
                "ip": "226.0.1.0{:02d}".format(id),
                "port" : 17101,
                "bit_depth": data_formats["CRYO_PAF_CBF_VOLTAGE_BEAM:1"]["nbit"]
            }

    @coroutine
    def register(self, host=None, port=None):
        """
        Registers the pipeline in the data store.

        Args:
            host, port: Ip and port of the data store.

        If no host and port ar eprovided, values from the internal config are used.
        """
        EDDPipeline.register(self, host, port)
        self._config["data_store"]["ip"] = host
        self._config["data_store"]["port"] = port
        try:
            self.__eddDataStore = EDDDataStore.EDDDataStore(host, port)
        except:
            log.error("Failed to connect to EDDDataStore")

if __name__ == "__main__":
    launchPipelineServer(BeamformPipeline)
