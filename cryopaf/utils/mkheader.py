import os
import logging
import six
from collections import OrderedDict

log = logging.getLogger("mpikat.effelsberg.edd.header_create")

class OrderedClassMeta(type):
    """
    Subclasses derived from OrderedClassMeta returns an OrderedDict for `self.__dict__`.
    The order of self.__dict__ is the order of attribute definition in the __init_()-method.
    """
    @classmethod
    def __prepare__(cls, name, bases, **kwds):
        return OrderedDict()

class OrderedClass(six.with_metaclass(OrderedClassMeta)):
    pass

class MkHeader(OrderedClass):

    def __init__(self, fname, type, **kwargs):

        self._type = type
        self._fname = fname
        self._items = []
        ## DADA PARAMS ##
        self.header=kwargs.get('header', 'DADA')
        self.hdr_version=kwargs.get('hdr_version', 1.0)
        self.hdr_size=kwargs.get('hdr_size', 4096)
        self.dada_version=kwargs.get('dada_version', 1.0)
        self.file_size=kwargs.get('file_size', 67108864)
        self.utc_start=kwargs.get('utc_start', 'unset')
        self.dim=kwargs.get('dim', 'unset')
        self.npol=kwargs.get('npol', 2)
        self.nbit=kwargs.get('nbit', 'unset')
        self.freq=kwargs.get('freq', 'unset')
        self.telescope=kwargs.get('telescope', 'unset')
        self.receiver=kwargs.get('receiver', 'unset')
        self.instrument=kwargs.get('instrument', 'unset')
        self.obs_offset=kwargs.get('obs_offset', 'unset')
        self.resolution=kwargs.get('resolution', 'unset')
        # MKRECV / MKSEND PARAMS

        # COMMON PARAMS
        self.packet_size=kwargs.get('packet_size',1472)     # Maximum packet size to use for UDP
        self.buffer_size=kwargs.get('buffer_size',8388608)   # Socket buffer size
        self.ngroups_data=kwargs.get('heap_group',64)     # Number of groups (heaps with the same timestamp) going into the data space.
        self.sample_clock=kwargs.get('sample_clock', 1750000000)# virtual sample clock used for calculations
        self.dada_mode=kwargs.get('dada_mode',1)            # dada mode (0 = artificial data, 1 = data from dada ringbuffer)
        self.bytes_per_second=kwargs.get('bytes_per_second',1750000000)            # dada mode (0 = artificial data, 1 = data from dada ringbuffer)
        # MKSEND PARAMS
        if type == 'mksend':
            self.nthreads=kwargs.get('nthreads',1)              # Number of #worker threads
            self.nheaps=kwargs.get('nheaps',4)                  # Maximum number #of active heaps
            self.memcpy_nt=kwargs.get('memcpy_nt',"unset")           # Use non_temporal memcpy
            self.dada_key=kwargs.get('dada_key','dada')         # PSRDADA ring buffer key
            self.ibv_if=kwargs.get('ibv_if','unset')                 # Interface address for ibverbs
            self.ibv_vector=kwargs.get('ibv_vector',0)          # Interrupt vector (_1 for polled)
            self.ibv_max_poll=kwargs.get('ibv_max_poll',10)     # Maximum number of times to poll in a row
            self.udp_if=kwargs.get('udp_if','unset')            # UDP interface
            self.port=kwargs.get('port','unset')                # Port number
            self.sync_time=kwargs.get('sync_time','unset')    # the ADC sync epoch
            self.sample_clock_start=kwargs.get('sample_clock_start',0)# virtual sample clock used for calculations

            self.nslots=kwargs.get('nslots',0)                  # Number of slots send via network during simulation, 0  means infinite.
            self.network_mode=kwargs.get('network_mode',1)      # network mode (0 = no network, 1 = full network support)
            self.nhops=kwargs.get('nhops',1)                    # Maximum number #of hops
            self.rate=kwargs.get('rate',0.0)                    # Network use #rate
            self.burst_size=kwargs.get('burst_size',65536)      # Size of a network burst [Bytes]
            self.burst_ratio=kwargs.get('burst_ratio',1.05)     # I do not know what this means.
            self.heap_size=kwargs.get('heap_size',0)            # The heap size used for checking incomming heaps.
            self.heap_id_start=kwargs.get('heap_id_start',1)    # First used heap id
            self.heap_id_offset=kwargs.get('heap_id_offset',1)  # offset of heap id (different streams)
            self.heap_id_step=kwargs.get('heap_id_step',1)      # difference between two heap ids (same stream)
            self.heap_group=kwargs.get('heap_group',1)          # number of consecutive heaps going into the same stream
            self.nsci=kwargs.get('nsci',0)                      # Number of #item pointers in the side_channel
            self.nitems=kwargs.get('nitems',0)                  # Number of #item pointers in a SPEAD heap
        # MKRECV PARAMS
        if type == 'mkrecv':
            self.dada_nslots=kwargs.get('dada_nslots',0)                  # Number of slots send via network during simulation, 0  means infinite.
            self.slots_skip=kwargs.get('slots_skip',0)                  # Number of slots send via network during simulation, 0  means infinite.
            self.nindices=kwargs.get('nindices', 0)
            self.heap_nbytes=kwargs.get('heap_size',0)            # The heap size used for checking incomming heaps.
            self.slot_nbytes=kwargs.get('slot_nbytes',0)            # The heap size used for checking incomming heaps.
            self.nheaps=kwargs.get('nheaps',4)
            # self.nheaps=kwargs.get('sample_clock_start',0)
            self.level_data=kwargs.get('level_data', 'unset')
            self.sci_list=kwargs.get('sci_list', 'unset')

    def create(self):
        with open(self._fname, "w") as file:
            file.write("# DADA parameters")
            for key, val in self.__dict__.items():
                if key[0] == '_':
                    continue
                if key == "packet_size":
                    file.write("\n# {} parameters".format(self._type.upper()))
                file.write("\n{} {}".format(key.upper(), str(val)))
            file.write("\n")

    def remove(self):
        os.remove(self._fname)

    def update_attr(self, attr, val):
        try:
            search_text = str(attr).upper() + " " + str(getattr(self, attr))
            setattr(self, attr, val)
            replace_text = str(attr).upper() + " " + str(getattr(self, attr))
        except Exception as e:
            log.error("Could not assign value {} to attribute {}. Error {}".format(attr, val, e))
            return
        with open(self._fname, 'r') as file:
            data = file.read().replace(search_text, replace_text)
        with open(self._fname, 'w') as file:
            file.write(data)

    def name(self):
        return self._fname

    def add_items(self, dictionary, ignore=[]):
        if not os.path.exists(self._fname):
            log.error("File does not exist, doing nothing")
            return
        with open(self._fname, "a") as file:
            # Write items for mksend
            cnt = 0
            if self._type == "mksend":
                for item_name, item_conf in dictionary.items():
                    if item_name in ignore:
                        continue
                    cnt += 1
                    self._items.append(item_conf)
                    file.write("\nITEM{}_ID {}".format(cnt, item_conf["id"]))
                    if "step" in item_conf:
                        file.write("\nITEM{}_STEP {}".format(cnt, item_conf["step"]))
                    elif "list" in item_conf:
                        file.write("\nITEM{}_LIST {}:{}:{}".format(cnt, item_conf["list"][0], item_conf["list"][1], item_conf["list"][2]))
                    elif "value" in item_conf:
                        file.write("\nITEM{}_LIST {}".format(cnt, item_conf["value"]))
                    if "index" in item_conf:
                        file.write("\nITEM{}_INDEX {}".format(cnt, item_conf["index"]))
                self.update_attr('nitems', len(self._items))
            # Write items for mkrecv
            elif self._type == "mkrecv":
                nindices = 0
                for item_name, item_conf in dictionary.items():
                    if item_name in ignore:
                        continue
                    cnt += 1
                    self._items.append(item_conf)
                    file.write("\nIDX{}_ITEM {}".format(cnt, item_conf["id"] - 0x1600))
                    if "step" in item_conf:
                        file.write("\nIDX{}_STEP {}".format(cnt, item_conf["step"]))
                    elif "list" in item_conf:
                        file.write("\nIDX{}_LIST {}:{}:{}".format(cnt, item_conf["list"][0], item_conf["list"][1], item_conf["list"][2]))
                    elif "value" in item_conf:
                        file.write("\nIDX{}_LIST {}".format(cnt, item_conf["value"]))
                    if "index" in item_conf:
                        file.write("\nIDX{}_INDEX {}".format(cnt, item_conf["index"]))
                    if "mask" in item_conf:
                        file.write("\nIDX{}_MASK {}".format(cnt, str(hex(item_conf["mask"]))))
                self.update_attr('nindices', cnt)
            file.write("\n")
