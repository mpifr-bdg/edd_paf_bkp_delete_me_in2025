import h5py
import numpy as np

class ACMFile(Exception):
    pass

class ACMFile(h5py.File):
    def __init__(self, fname, mode="w"):
        super(ACMFile, self).__init__(fname, mode)
        self.prefix = "ACM"

    def create(self):
        self.attrs['n_samples'] = 0
        self.attrs['azimuth_start'] = 0
        self.attrs['azimuth_stop'] = 0
        self.attrs['elevation_start'] = 0
        self.attrs['elevation_stop'] = 0
        self.attrs['dec_start'] = 0
        self.attrs['dec_stop'] = 0
        self.attrs['ra_start'] = 0
        self.attrs['ra_stop'] = 0
        self.attrs['on_source'] = False
        self.create_dataset('ACMdata', (N_PFB_CHANNELS, N_ELEMENTS, N_ELEMENTS), dtype=np.complex64)
        self.create_dataset('ACMstatus', (N_PFB_CHANNELS, N_ELEMENTS), dtype='int32')
        self.create_dataset('skyFrequency', (N_PFB_CHANNELS,), dtype='double')

    def write_dataset(name, array):
        pass
    def write_attribtue(name, value):
        pass
