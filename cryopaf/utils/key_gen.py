import time
import os
from random import randint


class DadaKeyGen():
    def __init__(self, exclude=[]):
        self.occupied = []
        self.update(exclude)

    def update(self, exclude=[]):
        self.occupied = [] + exclude
        out = os.popen("ipcs -m ").read().split("\n")
        for line in out:
            if line[0:2] != "0x":
                continue
            s = line.split(" ")
            self.occupied.append(s[0])

    def getFreeDadaKey(self):
        while True:
            key = str(hex(randint(int('0x1000', 16), int('0xefff', 16))))
            if key not in self.occupied:
                self.occupied.append(key)
                return key.replace("0x","")
        return None

    def getFreeDadaKeys(self, n_keys, mode="ascending", step=2):
        keys = []
        if mode == "random":
            for key in range(n_keys):
                keys.append(self.getFreeDadaKey())
        elif mode == "ascending":
            keys.append(self.getFreeDadaKey())
            for key in range(n_keys-1):
                k = str(hex(int(keys[-1],16) + step))
                for k in self.occupied:
                    k = str(hex(int(keys[-1],16) + step))
                self.occupied.append(k)
                keys.append(k.replace("0x", ""))
        return keys
