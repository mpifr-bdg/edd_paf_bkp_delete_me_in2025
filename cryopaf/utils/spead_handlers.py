import spead2
import spead2.recv
from mpikat.utils.spead_capture import SpeadPacket

from cryopaf.conf.constants import *

class RFISpeadHandler(object):
    def __init__(self):
        self.ig = spead2.ItemGroup()
        self.ig.add_item(5632, "timestamp", "", (6,), dtype=">u1")
        self.ig.add_item(5633, "channel_id", "", (6,), dtype=">u1")
        self.ig.add_item(5634, "element_id", "", (6,), dtype=">u1")
        self.ig.add_item(5635, "integration_interval", "", (6,), dtype=">u1")

        self.timestamp_handler = None

    def __call__(self, heap):
        log.debug("Unpacking heap")
        items = self.ig.update(heap)
        return SpeadPacket(items)

class ComplexGainSpeadHandler(object):
    def __init__(self):
        self.ig = spead2.ItemGroup()
        self.ig.add_item(5632, "timestamp", "", (6,), dtype=">u1")
        self.ig.add_item(5633, "channel_id", "", (6,), dtype=">u1")
        self.ig.add_item(5634, "element_id", "", (6,), dtype=">u1")
        self.ig.add_item(5635, "integration_interval", "", (6,), dtype=">u1")

        self.timestamp_handler = None

    def __call__(self, heap):
        log.debug("Unpacking heap")
        items = self.ig.update(heap)
        return SpeadPacket(items)

class XEngineSpeadHandler(object):
    def __init__(self):
        self.ig = spead2.ItemGroup()
        self.ig.add_item(5632, "timestamp", "", (6,), dtype=">u1")
        self.ig.add_item(5633, "order_vector", "", (6,), dtype=">u1")
        self.ig.add_item(5634, "flagged", "", (6,), dtype=">u1")
        self.ig.add_item(5635, "data", "", (CHANNELS_PER_SUBBAND,N_PAF_ELEMENTS,2), dtype=">u1")

    def __call__(self, heap):
        log.debug("Unpacking heap")
        items = self.ig.update(heap)
        return SpeadPacket(items)
