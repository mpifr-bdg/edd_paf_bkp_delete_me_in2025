import os
import h5py
import numpy as np
import random
import redis
import logging
from datetime import datetime as dt
# import matplotlib
# matplotlib.use("Agg")
import matplotlib.pyplot as mplt

from cryopaf.conf.constants import *

logging.basicConfig()
log = logging.getLogger("mpikat.cryopaf.weightfile")
log.setLevel(logging.DEBUG)

N_PAF_PORTS = N_PAF_ELEMENTS*2

class BeamWeightFileError(Exception):
    pass

class BeamWeightFile(h5py.File):

    MAXINT_14BIT_SIGNED = 8191
    WEIGHT_DTYPE = np.dtype([('r', np.int16), ('i', np.int16)])
    ODC_REPSONSE_DTYPE = np.dtype([('r', np.float32), ('i', np.float32)])
    VERSION = 2
    SITE = 'pk'
    ANTENNA = 1
    FILE_EXTENSION = '.pk01.wt.hdf5'
    PAF_VALID=False
    PORTS_VALID=False
    MAX_WEIGHT=8191
    SCALE_X=480
    SCALE_Y=480

    """
    Class for reading/writing '.wt.hdf5' weights files for the ASKAP beamformer.  It should not be possible to create a
    file that will not upload to a properly configured ASKAP digital backend (with up to 8 beamformer chassis).
    """
    def __init__(self, name="", mode='w', sbid=4831, n_beams=N_BEAMS_MAX, n_freq=N_PFB_CHANNELS, center_freq=1340):
        """
        """
        if 'w' in mode:
            if not name:
                date = dt.now().strftime('%Y%m%d%H%M')
                name += 'SB' + str(sbid).zfill(5) + "_" + date + "_" + str(center_freq) + "M"
            if self.FILE_EXTENSION not in name:
                name += self.FILE_EXTENSION
            super(BeamWeightFile, self).__init__(name, mode)
            self.n_beam = n_beams
            self.n_freq = n_freq
            self.sbid = sbid
            self.beams = np.arange(1, self.n_beam+1)
            self.freq = np.arange(center_freq-n_freq/2, center_freq+n_freq/2)
        if 'r' in mode:
            if not os.path.isfile(name):
                log.error("File {} does not exists".format(name))
                raise WeightsFileError("File {} does not exists".format(name))
            super(BeamWeightFile, self).__init__(name, mode)
            self.n_beam = np.asarray(self['pts_x']).shape[0]
            self.n_freq = np.asarray(self['pts_x']).shape[1]
        self.get_max_ports()
        self.n_ports_max_x = int(N_PAF_ELEMENTS/2)
        self.n_ports_max_y = int(N_PAF_ELEMENTS/2)

    def create(self):
        """
        Initialise emtpy `wt.hdf5` file according to version 1 of the schema (fixed precision weights).
        """
        # Create HDF5 attribtues
        self.attrs['version'] = self.VERSION
        self.attrs['paf_valid'] = 1
        self.attrs['site'] = self.SITE
        self.attrs['antenna'] = self.ANTENNA
        self.attrs['schedulingblock'] = self.sbid
        self.attrs['paf_valid'] = self.PAF_VALID
        self.attrs['ports_valid'] = self.PORTS_VALID

        self.create_dataset('beams', data=self.beams, maxshape=(N_BEAMS_MAX,), dtype=np.int64)
        self.create_dataset('freq', data=self.freq, maxshape=(N_PFB_CHANNELS,), dtype=np.int64)
        self.create_dataset('eig_x', data=np.zeros((self.n_beam, self.n_freq, self.n_ports_max_x), np.float64),
            maxshape=(N_BEAMS_MAX, N_PFB_CHANNELS, N_DBE_PORTS), dtype=np.float64,
            chunks=(self.n_beam, self.n_freq, self.n_ports_max_x))
        self.create_dataset('eig_y', data=np.zeros((self.n_beam, self.n_freq, self.n_ports_max_y), np.float64),
            maxshape=(N_BEAMS_MAX, N_PFB_CHANNELS, N_DBE_PORTS), dtype=np.float64,
            chunks=(self.n_beam, self.n_freq, self.n_ports_max_y))
        self.create_dataset('wgt_x', data=np.zeros((self.n_beam, self.n_freq, self.max_ports), self.WEIGHT_DTYPE),
            maxshape=(N_BEAMS_MAX, N_PFB_CHANNELS, N_DBE_PORTS), dtype=self.WEIGHT_DTYPE,
            chunks=(self.n_beam, self.n_freq, self.max_ports))
        self.create_dataset('wgt_y', data=np.zeros((self.n_beam, self.n_freq, self.max_ports), self.WEIGHT_DTYPE),
            maxshape=(N_BEAMS_MAX, N_PFB_CHANNELS, N_DBE_PORTS), dtype=self.WEIGHT_DTYPE,
            chunks=(self.n_beam, self.n_freq, self.max_ports))
        self.create_dataset('pts_x', data=np.zeros((self.n_beam, self.n_freq, self.max_ports), np.int64),
            maxshape=(N_BEAMS_MAX, N_PFB_CHANNELS, N_DBE_PORTS), dtype=np.int64,
            chunks=(self.n_beam, self.n_freq, self.max_ports))
        self.create_dataset('pts_y', data=np.zeros((self.n_beam, self.n_freq, self.max_ports), np.int64),
            maxshape=(N_BEAMS_MAX, N_PFB_CHANNELS, N_DBE_PORTS), dtype=np.int64,
            chunks=(self.n_beam, self.n_freq, self.max_ports))
        self.create_dataset('scale_x', data=np.full((self.n_beam, self.n_freq), self.SCALE_X, np.int64),
            maxshape=(N_BEAMS_MAX, N_PFB_CHANNELS), dtype=np.int64,
            chunks=(self.n_beam, self.n_freq))
        self.create_dataset('scale_y', data=np.full((self.n_beam, self.n_freq), self.SCALE_Y, np.int64),
            maxshape=(N_BEAMS_MAX, N_PFB_CHANNELS), dtype=np.int64,
            chunks=(self.n_beam, self.n_freq))
        self.create_dataset('y_factor_x', data=np.zeros((self.n_beam, self.n_freq), np.float64),
            dtype=np.float64, chunks=(self.n_beam, self.n_freq))
        self.create_dataset('y_factor_y', data=np.zeros((self.n_beam, self.n_freq), np.float64),
            dtype=np.float64, chunks=(self.n_beam, self.n_freq))
        self.create_dataset('ports_valid', data=np.zeros(N_PAF_ELEMENTS, np.bool),
            dtype=np.bool, maxshape=(N_PAF_ELEMENTS,))
        self.create_dataset('paf_valid', data=self.PAF_VALID, dtype=np.bool)
        self.create_dataset('odc_response', data=np.zeros((self.n_beam+1, self.n_freq, N_DBE_PORTS), self.WEIGHT_DTYPE),
            maxshape=(N_BEAMS_MAX+1, N_PFB_CHANNELS, N_DBE_PORTS), dtype=self.ODC_REPSONSE_DTYPE)

    def bypass(self, element_list=[]):
        element_list = np.asarray(element_list)

        weights_x = np.zeros((self.n_beam, self.n_freq, self.max_ports), self.WEIGHT_DTYPE)
        weights_y = np.zeros((self.n_beam, self.n_freq, self.max_ports), self.WEIGHT_DTYPE)
        weights_x[:,:,0] = (self.MAX_WEIGHT, 0)
        weights_y[:,:,0] = (self.MAX_WEIGHT, 0)

        self['wgt_x'][:,:,:] = weights_x
        self['wgt_y'][:,:,:] = weights_y
        port_x = []
        port_y = []
        port_x.extend(range(min(ELEMENTS_X),max(ELEMENTS_X) + 1))
        port_y.extend(range(min(ELEMENTS_Y),max(ELEMENTS_Y) + 1))
        if element_list.shape[1] == 2:
            for idx, element in enumerate(element_list):
                x = random.sample(list(set(port_x) - set([element_list[idx][0]])), self.max_ports)
                y = random.sample(list(set(port_y) - set([element_list[idx][1]])), self.max_ports)
                self['pts_x'][idx] = np.repeat(x, N_PFB_CHANNELS).reshape((self.max_ports, N_PFB_CHANNELS)).transpose()
                self['pts_y'][idx] = np.repeat(y, N_PFB_CHANNELS).reshape((self.max_ports, N_PFB_CHANNELS)).transpose()
                self['pts_x'][idx,:,0] = element_list[idx][0]
                self['pts_y'][idx,:,0] = element_list[idx][1]
        else:
            for e in range(self.n_beam):
                if e == 0:
                    self['pts_x'][e,:,:] = element_list[0,0,:,0:self.max_ports]
                    self['pts_y'][e,:,:] = element_list[1,0,:,0:self.max_ports]
                else:
                    element_list[0,0,:,0:self.max_ports] = np.roll(element_list[0,0,:,0:self.max_ports], -1, axis=1)
                    element_list[1,0,:,0:self.max_ports] = np.roll(element_list[1,0,:,0:self.max_ports], -1, axis=1)
                    self['pts_x'][e,:,:] = element_list[0,0,:,0:self.max_ports]
                    self['pts_y'][e,:,:] = element_list[1,0,:,0:self.max_ports]


    def get_element_ports(self):
        pts_x = np.asarray(self["pts_x"])
        pts_y = np.asarray(self["pts_y"])
        return np.asarray([pts_x, pts_y])

    def select_strongest(self, nelements):
        weights_x = self["wgt_x"][:,:,0:nelements]
        weights_y = self["wgt_y"][:,:,0:nelements]
        return np.asarray([weights_x, weights_y])


    def get_weights(self, freq, beams=np.arange(N_BEAMS_MAX), elements=np.asarray(zip(np.arange(0,N_PAF_ELEMENTS), np.arange(N_PAF_ELEMENTS, N_PAF_ELEMENTS*2)))):
        elements = np.asarray(elements)
        pts_x = np.asarray(self['pts_x'][:, freq, :])
        pts_y = np.asarray(self['pts_y'][:, freq, :])
        wgt_x = np.asarray(self["wgt_x"])
        wgt_y = np.asarray(self["wgt_y"])
        pos_x = np.asarray(np.nonzero(np.isin(pts_x, elements[:,0])))
        pos_y = np.asarray(np.nonzero(np.isin(pts_y, elements[:,1])))
        weight_x = (wgt_x[pos_x[0, :],pos_x[1, :], pos_x[2, :]]).reshape(1, self.n_beam, len(freq), len(elements))
        weight_y = (wgt_y[pos_y[0, :],pos_y[1, :], pos_y[2, :]]).reshape(1, self.n_beam ,len(freq), len(elements))
        if weight_x.shape != weight_y.shape:
            raise WeightsFileError("Shape mismatch of X- and Y-pol {} {}".format(weight_x.shape, weight_y.shape))
        return np.concatenate([weight_x, weight_y])

    def plot_weights(self, outdir="", elements=[], freq=[], show=False):
        if elements != []:
            self.n_ports_max_x = len(elements)
            self.n_ports_max_y = len(elements)
        weights_x = np.zeros((self.n_beam, self.n_freq, self.n_ports_max_x), dtype=np.complex64)
        weights_y = np.zeros((self.n_beam, self.n_freq, self.n_ports_max_y), dtype=np.complex64)
        pts_x = np.asarray(self['pts_x'])
        pts_y = np.asarray(self['pts_y'])
        weights_from_file_x = np.asarray(self["wgt_x"]).view(np.int16).astype(np.float32).view(np.complex64)
        weights_from_file_y = np.asarray(self["wgt_y"]).view(np.int16).astype(np.float32).view(np.complex64)

        for beam in range(self.n_beam):
            freq_list = np.arange(0,self.n_freq,int(self.n_freq*0.2)).tolist()
            if elements == [] or freq == []:
                for f in range(self.n_freq):
                        for e in range(0,self.max_ports):
                            weights_x[beam,f,pts_x[beam,f,e]-1] = weights_from_file_x[beam,f,e]/self.MAXINT_14BIT_SIGNED
                            weights_y[beam,f,pts_y[beam,f,e]-self.n_ports_max_x-1] = weights_from_file_y[beam,f,e]/self.MAXINT_14BIT_SIGNED
            else:
                weights_x = self.get_weights_by_freq_and_elementid(freq, elements)[0].view(np.int16).astype(np.float32).view(np.complex64)
                weights_y = self.get_weights_by_freq_and_elementid(freq, elements)[1].view(np.int16).astype(np.float32).view(np.complex64)
                freq_list = freq.tolist()

            sub = [0,0,0,0]

            f = mplt.figure(num=beam, figsize=(12,12))
            sub[0] = mplt.subplot(2,2,1)
            sub[0].set_title("X-pol weights")
            sub[0].set_ylabel("Elements")
            sub[0].set_xlabel("Frequency [MHz]")
            sub[0].set_xticks(freq_list)
            sub[0].set_xticklabels(self["freq"][freq_list])

            sub[1] = mplt.subplot(2,2,2)
            sub[1].set_title("Y-pol weights")
            sub[1].set_ylabel("Elements")
            sub[1].set_xlabel("Frequency [MHz]")
            sub[1].set_xticks(freq_list)
            sub[1].set_xticklabels(self["freq"][freq_list])

            sub[2] = mplt.subplot(2,2,3)
            sub[2].set_ylim(.5, 5)
            sub[2].set_xlim(np.min(self["freq"])-0.5, np.max(self["freq"])+0.5)
            sub[2].plot(self["freq"], self["y_factor_x"][beam], lw=1, color="black")
            sub[2].set_ylabel('y-factor', fontsize=10)
            sub[2].set_xlabel('Frequency [MHz]', fontsize=10)

            sub[3] = mplt.subplot(2,2,4)
            sub[3].set_ylim(.5, 5)
            sub[3].set_xlim(np.min(self["freq"])-0.5, np.max(self["freq"])+0.5)
            sub[3].plot(self["freq"], self["y_factor_y"][beam], lw=1, color="black")
            sub[3].set_ylabel('y-factor', fontsize=10)
            sub[3].set_xlabel('Frequency [MHz]', fontsize=10)

            sub[0].imshow(np.abs(weights_x[beam]).transpose(),interpolation='nearest', aspect='auto')
            sub[1].imshow(np.abs(weights_y[beam]).transpose(),interpolation='nearest', aspect='auto')
            mplt.tight_layout()
            if outdir:
                mplt.savefig(outdir + "weights_beam{}.png".format(beam))
            if show:
                mplt.show()
            else:
                mplt.close(f)
