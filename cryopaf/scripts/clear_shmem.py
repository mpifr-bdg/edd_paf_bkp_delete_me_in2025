import os

fname = "/tmp/ipcs_file.txt"
os.system("ipcs -m > " + str(fname))

with open(fname, "r") as file:
    data = file.read()
data = data.split("\n")
for line in data:
    if line[0:2] != "0x":
        continue
    s = line.split(" ")
    while("" in s):
        s.remove("")
    mem_addr = s[0]
    if s[5] == "0":
        os.system("ipcrm -M " + str(mem_addr))
        print("ipcrm -M " + str(mem_addr))
os.system("rm " + fname)
