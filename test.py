import logging
import unittest


if __name__ == "__main__":
    logging.basicConfig(filename='debug.log',
        format=("[ %(levelname)s - %(asctime)s - %(name)s "
             "- %(filename)s:%(lineno)s] %(message)s"),
            level=logging.DEBUG)

    loader = unittest.TestLoader()
    # tests = loader.discover(pattern="test_*.py", start_dir="paf_edd/")
    tests = loader.discover(pattern="test_*.py", start_dir="test/")
    runner = unittest.runner.TextTestRunner()
    res = runner.run(tests)
    exit(len(res.failures))
