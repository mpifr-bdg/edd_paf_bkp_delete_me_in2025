from cryopaf.pipelines.gpu_beamformer import BeamformPipeline
from katcp import FailReply
import unittest
import tornado.testing
import tornado.gen
import logging


class TestBeamformPipeline(tornado.testing.AsyncTestCase):

    @tornado.gen.coroutine
    def __test_sequence(self, pipeline):
        self.assertEqual(pipeline.state, 'idle')
        result = pipeline.configure()
        self.assertEqual(pipeline.state, 'configuring')
        yield result
        self.assertEqual(pipeline.state, 'configured')

        yield pipeline.capture_start()
        self.assertEqual(pipeline.state, 'ready')

        yield pipeline.measurement_prepare()
        self.assertEqual(pipeline.state, 'set')

        yield pipeline.measurement_start()
        self.assertEqual(pipeline.state, 'streaming')

        yield pipeline.measurement_stop()
        self.assertEqual(pipeline.state, 'ready')

        yield pipeline.capture_stop()
        self.assertEqual(pipeline.state, 'configured')

        yield pipeline.deconfigure()
        self.assertEqual(pipeline.state, 'idle')


    @tornado.testing.gen_test(timeout=120)
    def test_BeamformPipeline_sequence(self):
        pass
        # pipeline = BeamformPipeline("localhost", 1234)
        # yield self.__test_sequence(pipeline)


if __name__ == '__main__':
    logging.basicConfig(filename='debug.log',
        format=("[ %(levelname)s - %(asctime)s - %(name)s "
             "- %(filename)s:%(lineno)s] %(message)s"),
            level=logging.DEBUG)
    unittest.main()
