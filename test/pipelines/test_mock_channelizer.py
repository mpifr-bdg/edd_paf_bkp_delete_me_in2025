from cryopaf.pipelines.mock_channelizer import MockChannelizer
from katcp import FailReply
import unittest
import tornado.testing
import tornado.gen
import logging
import numpy as np
import time
import os
import shutil

HEADER_BYTES = 4096

class TestMockChannelizer(tornado.testing.AsyncTestCase):

    @tornado.gen.coroutine
    def __test_sequence(self, pipeline):

        self.assertEqual(pipeline.state, 'idle')
        # pipeline.set('{"mode":"test","repeats":1, "n_elements":4}')
        # result = pipeline.configure()
        # self.assertEqual(pipeline.state, 'configuring')
        # yield result
        # self.assertEqual(pipeline.state, 'streaming')
        #
        # while pipeline._generator.is_alive():
        #     time.sleep(2)
        #
        # conf = pipeline._config
        # self.proof_dbsplit(conf)
        #
        # yield pipeline.capture_start()
        # self.assertEqual(pipeline.state, 'streaming')
        #
        #
        # yield pipeline.measurement_start()
        # self.assertEqual(pipeline.state, 'streaming')
        #
        # yield pipeline.measurement_stop()
        # self.assertEqual(pipeline.state, 'streaming')
        #
        # yield pipeline.measurement_prepare()
        # self.assertEqual(pipeline.state, 'streaming')
        #
        # yield pipeline.capture_stop()
        # self.assertEqual(pipeline.state, 'streaming')

        # This test needs to be available,as otherwise on successfull test
        # pycoverage wont exit
        yield pipeline.deconfigure()
        self.assertEqual(pipeline.state, 'idle')


    @tornado.testing.gen_test(timeout=120)
    def test_MockChannelizer_sequence(self):
        pipeline = MockChannelizer("localhost", 1234)
        yield self.__test_sequence(pipeline)

    def proof_dbsplit(self, conf):
        # Load test data set generated by MockChannelizer
        origin = np.fromfile(conf["dataset_file"], dtype="int8") \
            .reshape(conf["heaps_per_buffer"],
                conf["channels_per_mcgroup"],
                len(conf["output_data_streams"]),
                conf["samples_per_heap"], 2)
        # Initialize result array
        result = np.zeros((
            conf["heaps_per_buffer"],
            conf["channels_per_mcgroup"],
            len(conf["output_data_streams"]),
            conf["samples_per_heap"], 2,),
            dtype="int8")
        # Parse subdirectories of dataset_dir
        subdir = [os.path.join(conf["dataset_dir"], dname) for dname in os.listdir(conf["dataset_dir"]) if os.path.isdir(os.path.join(conf["dataset_dir"], dname))]
        subdir.sort(key=lambda x: int("0x" + (x.split("/")[-1]), 16))
        # load resulting data into array
        for i, dir in enumerate(subdir):
            files = sorted(os.listdir(dir))
            start = 0
            for ii, file in enumerate(files[:-1]):
                fname = dir + "/" + file
                nheaps = (os.path.getsize(fname) - HEADER_BYTES) // (conf["channels_per_mcgroup"] * conf["samples_per_heap"] * 2)
                result[start:(ii+1)*nheaps,:,i] = np.fromfile(fname, dtype="int8", offset=HEADER_BYTES) \
                    .reshape(nheaps,conf["channels_per_mcgroup"],conf["samples_per_heap"],2) # offset 4096 because of dada header
                start += (ii+1)*nheaps
        # Compare both arrays
        self.assertEqual(np.all(result == origin), True)


if __name__ == '__main__':
    logging.basicConfig(filename='debug.log',
        format=("[ %(levelname)s - %(asctime)s - %(name)s "
             "- %(filename)s:%(lineno)s] %(message)s"),
            level=logging.DEBUG)
    unittest.main()
