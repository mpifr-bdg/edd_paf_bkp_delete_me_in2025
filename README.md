# Introduction
This repository contains processing pipelines used for the CryoPAF project in Effelsberg.


## Requirements

The python-based processing pipelines are implementations of the EDDPipeline-class which is implemented in our [mpikat-repository](https://gitlab.mpcdf.mpg.de/mpifr-bdg/mpikat). All further dependencies to external python-libraries are given by installing the mpikat library.

The C++-code has depends on [psrdada_cpp](https://gitlab.mpcdf.mpg.de/mpifr-bdg/mpikat), [redis-c++](), [boost](), [CUDA]()

## Installation

# Contact

Author: Niclas Esser - <nesser@mpifr-bonn.mpg.de>

# ToDo's
- There are many
- Remove thirdparty from cpp/
